﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BPM.Data
{
    public interface IGenericRepository<T> where T : class
    {
       
            List<T> GetAll();
            Task<List<T>> GetModel();
            T GetModelById(int modelId);
            Task<List<T>> GetModelWithEagrLoading(string entity);
            Task<List<T>> GetMultipleModelWithEagrLoading(Expression<Func<T, bool>> filter, string[] children);
            Task<List<T>> GetModelByBPMId(int bpmId);
            Task<List<T>> GetModelByYear(string year);
            Task<bool> InsertModel(T model);
            Task<bool> UpdateModel(T model);
            Task<bool> DeleteModel(int modelId);
            void Save();
    }
}
