﻿using BPM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BPM.Data
{
    public class BPMRepository<T> : IGenericRepository<T> where T : class
        {
            private bpmdbdevContext _context = new bpmdbdevContext();
        private DbSet<T> dbEntity;

        public BPMRepository(bpmdbdevContext context)
            {
                this._context = context;
                dbEntity = _context.Set<T>();
             }

            public List<T> GetAll()
            {
                try
                {
                    return dbEntity.ToList();
                }
                catch (Exception e)
                {
                    throw new NotImplementedException();
                }
            }

            public async Task<List<T>> GetModel()
            {
                try
                {
                    return dbEntity.ToList();
                }
                catch (Exception e)
                {

                    throw e;
                }
            }

            public async Task<List<T>> GetMultipleModelWithEagrLoading(Expression<Func<T, bool>> filter, string[] children)
            {
                try
                {
                    IQueryable<T> query = dbEntity;
                    foreach (string entity in children)
                    {
                        query = query.Include(entity);

                    }
                    return await query.Where(filter).ToListAsync();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }

            public async Task<List<T>> GetModelWithEagrLoading(string entity)
            {
                try
                {
                    return await dbEntity.Include(entity).ToListAsync();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            public T GetModelById(int modelId)
            {
                try
                {
                    T model = dbEntity.Find(modelId);
                    return model;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            public async Task<List<T>> GetModelByBPMId(int bpmId)
            {
                try
                {
                    return await dbEntity.ToListAsync();
                }
                catch (Exception)
                {
                    throw new NotImplementedException();
                }
            }

            public async Task<List<T>> GetModelByYear(string year)
            {
                try
                {
                    return await dbEntity.ToListAsync();
                }
                catch (Exception)
                {
                    throw new NotImplementedException();
                }
            }

            public async Task<bool> InsertModel(T model)
            {
                try
                {
                    dbEntity.Add(model);
                    int x = await _context.SaveChangesAsync();
                    return x == 0 ? false : true;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }


            public async Task<bool> UpdateModel(T model)
            {
                try
                {
                    _context.Entry(model).State = (Microsoft.EntityFrameworkCore.EntityState)EntityState.Modified;
                    int x = await _context.SaveChangesAsync();
                    return x == 0 ? false : true;
                }
                catch (Exception e)
                {
                    throw new NotImplementedException();
                }
            }

            public async Task<bool> DeleteModel(int modelId)
            {
                try
                {
                    T model = dbEntity.Find(modelId);
                    dbEntity.Remove(model);
                    int x = await _context.SaveChangesAsync();
                    return x == 0 ? false : true;
                }
                catch
                {
                    throw new NotImplementedException();
                }
            }



            public void Save()
            {
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception)
                {
                    throw new NotImplementedException();
                }

            }

        }
    
}
