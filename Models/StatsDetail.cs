﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class StatsDetail
    {
        public int StatsDetailsId { get; set; }
        public int BatchId { get; set; }
        public string Dbname { get; set; }
        public string TableName { get; set; }
        public string SchemaName { get; set; }
        public string StatName { get; set; }
        public DateTime? LastUpdate { get; set; }
        public long ModificationCounter { get; set; }
        public DateTime? StatsDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsProcessed { get; set; }
        public DateTime? ProcessedDate { get; set; }
    }
}
