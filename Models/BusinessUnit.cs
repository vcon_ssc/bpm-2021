﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public  class BusinessUnit
    {
        public BusinessUnit()
        {
            Bpmreferences = new HashSet<Bpmreference>();
        }

        public int Buid { get; set; }
        public string Buname { get; set; }
        public bool IsDeleted { get; set; }
        public int? RegionId { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<Bpmreference> Bpmreferences { get; set; }
    }
}
