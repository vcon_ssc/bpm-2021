﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class PredefinedSupportItf
    {
        public int SupportItfId { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public int? SerialNo { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
