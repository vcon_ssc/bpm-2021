﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class Region
    {
        public Region()
        {
            Bpmreferences = new HashSet<Bpmreference>();
        }

        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ResponsiblePerson { get; set; }
        public string GroupName { get; set; }
        public string Mcsponser { get; set; }
        public string Year { get; set; }
        public int? SerialNo { get; set; }

        public virtual ICollection<Bpmreference> Bpmreferences { get; set; }
    }
}
