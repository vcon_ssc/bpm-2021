﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace BPM.Models
{
    public partial class bpmdbdevContext : DbContext
    {
        public bpmdbdevContext()
        {
        }

        public bpmdbdevContext(DbContextOptions<bpmdbdevContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bpmreference> Bpmreferences { get; set; }
        public virtual DbSet<BusinessUnit> BusinessUnits { get; set; }
        public virtual DbSet<CapitalExpense> CapitalExpenses { get; set; }
        //public virtual DbSet<CoreMarket> CoreMarkets { get; set; }
        public virtual DbSet<CoreMarket> CoreMarkets1 { get; set; }
        public virtual DbSet<CorporateServiceGroup> CorporateServiceGroups { get; set; }
        public virtual DbSet<CurrentChallenge> CurrentChallenges { get; set; }
        //public virtual DbSet<CurrentChallenge1> CurrentChallenges1 { get; set; }
        public virtual DbSet<CurrentOpportunity> CurrentOpportunities { get; set; }
        //public virtual DbSet<CurrentOpportunity1> CurrentOpportunities1 { get; set; }
        public virtual DbSet<CurrentSuccess> CurrentSuccesses { get; set; }
        //public virtual DbSet<CurrentSuccess1> CurrentSuccesses1 { get; set; }
        public virtual DbSet<FocusArea> FocusAreas { get; set; }
        public virtual DbSet<FocusAreaRequireGroup> FocusAreaRequireGroups { get; set; }
        public virtual DbSet<FocusAreaSupportItf> FocusAreaSupportItfs { get; set; }
        public virtual DbSet<IndexStat> IndexStats { get; set; }
        public virtual DbSet<Individual> Individuals { get; set; }
        //public virtual DbSet<Individual1> Individuals1 { get; set; }
        public virtual DbSet<IntegratedTopFive> IntegratedTopFives { get; set; }
        public virtual DbSet<LastDatabaseAccess> LastDatabaseAccesses { get; set; }
        public virtual DbSet<LastLogon> LastLogons { get; set; }
        public virtual DbSet<LeaseModification> LeaseModifications { get; set; }
        public virtual DbSet<LoginAccessTracking> LoginAccessTrackings { get; set; }
        public virtual DbSet<MaintException> MaintExceptions { get; set; }
        public virtual DbSet<MissedOpportunity> MissedOpportunities { get; set; }
        //public virtual DbSet<MissedOpportunity1> MissedOpportunities1 { get; set; }
        public virtual DbSet<Narrative> Narratives { get; set; }
        public virtual DbSet<NationalService> NationalServices { get; set; }
        //public virtual DbSet<NationalService1> NationalServices1 { get; set; }
        public virtual DbSet<NextBigIdea> NextBigIdeas { get; set; }
        //public virtual DbSet<NextBigIdea1> NextBigIdeas1 { get; set; }
        public virtual DbSet<NextChallenge> NextChallenges { get; set; }
        //public virtual DbSet<NextChallenge1> NextChallenges1 { get; set; }
        public virtual DbSet<NextOpportunity> NextOpportunities { get; set; }
        //public virtual DbSet<NextOpportunity1> NextOpportunities1 { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<PredefinedRequireGroup> PredefinedRequireGroups { get; set; }
        public virtual DbSet<PredefinedSupportItf> PredefinedSupportItfs { get; set; }
        public virtual DbSet<RecessionPlanning> RecessionPlannings { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        //public virtual DbSet<Region1> Regions1 { get; set; }
        public virtual DbSet<Ssneed> SsNeeds { get; set; }
       // public virtual DbSet<Ssneed1> Ssneeds1 { get; set; }
        public virtual DbSet<StatsDetail> StatsDetails { get; set; }
        public virtual DbSet<StrategicPartner> StrategicPartners { get; set; }
       // public virtual DbSet<StrategicPartner1> StrategicPartners1 { get; set; }
        public virtual DbSet<UtilizedOpportunity> UtilizedOpportunities { get; set; }
       // public virtual DbSet<UtilizedOpportunity1> UtilizedOpportunities1 { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=azr-dev-sqldb.database.windows.net;Database=bpmdbdev;persist security info=True;user id=UsrBPM;password=Gk$912FDsdf02;MultipleActiveResultSets=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Bpmreference>(entity =>
            {
                entity.HasKey(e => e.Brid);

                entity.ToTable("BPMReference");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.Buid).HasColumnName("BUID");

                entity.Property(e => e.Cmid).HasColumnName("CMID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Nsid).HasColumnName("NSID");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.Spid).HasColumnName("SPID");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Bu)
                    .WithMany(p => p.Bpmreferences)
                    .HasForeignKey(d => d.Buid)
                    .HasConstraintName("FK_BPMReference_BusinessUnit");

                entity.HasOne(d => d.Cm)
                    .WithMany(p => p.Bpmreferences)
                    .HasForeignKey(d => d.Cmid)
                    .HasConstraintName("FK_BPMReference_CoreMarket");

                entity.HasOne(d => d.Ns)
                    .WithMany(p => p.Bpmreferences)
                    .HasForeignKey(d => d.Nsid)
                    .HasConstraintName("FK_BPMReference_NationalService");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Bpmreferences)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK_BPMReference_Region");

                entity.HasOne(d => d.Sp)
                    .WithMany(p => p.Bpmreferences)
                    .HasForeignKey(d => d.Spid)
                    .HasConstraintName("FK_BPMReference_StrategicPartners");
            });

            //modelBuilder.Entity<BusinessUnit>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Business_Unit");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.Buname)
            //        .HasMaxLength(50)
            //        .HasColumnName("BUName");
            //});

            modelBuilder.Entity<BusinessUnit>(entity =>
            {
                entity.HasKey(e => e.Buid);

                entity.ToTable("BusinessUnit");

                entity.Property(e => e.Buid).HasColumnName("BUID");

                entity.Property(e => e.Buname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("BUName");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");
            });

            modelBuilder.Entity<CapitalExpense>(entity =>
            {
                entity.HasKey(e => e.Ceid)
                    .HasName("PK_CapitalExpenses");

                entity.ToTable("CapitalExpense");

                entity.Property(e => e.Ceid).HasColumnName("CEID");

                entity.Property(e => e.Amount).HasMaxLength(50);

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.CapitalExpenses)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_CapitalExpenses_BPMReference");
            });

            //modelBuilder.Entity<CoreMarket>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Core_Market");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.Cmname)
            //        .HasMaxLength(50)
            //        .HasColumnName("CMName");
            //});

            modelBuilder.Entity<CoreMarket>(entity =>
            {
                entity.HasKey(e => e.Cmid);

                entity.ToTable("CoreMarket");

                entity.Property(e => e.Cmid).HasColumnName("CMID");

                entity.Property(e => e.Cmname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("CMName");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<CorporateServiceGroup>(entity =>
            {
                entity.HasKey(e => e.Csgid);

                entity.Property(e => e.Csgid).HasColumnName("CSGID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Csgname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("CSGName");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            
            modelBuilder.Entity<CurrentChallenge>(entity =>
            {
                entity.HasKey(e => e.Ccid);

                entity.ToTable("CurrentChallenges");

                entity.Property(e => e.Ccid).HasColumnName("CCID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.CurrentChallenge1s)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_CurrentChallenges_BPMReference");
            });

           

            modelBuilder.Entity<CurrentOpportunity>(entity =>
            {
                entity.HasKey(e => e.Coid);

                entity.ToTable("CurrentOpportunities");

                entity.Property(e => e.Coid).HasColumnName("COID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.CurrentOpportunity1s)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CurrentOpportunities_BPMReference");
            });

            //modelBuilder.Entity<CurrentSuccess>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Current_Success");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(50);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.CurrentSuccess).HasColumnName("CurrentSuccess");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            //    entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Year)
            //        .IsRequired()
            //        .HasMaxLength(50);
            //});

            modelBuilder.Entity<CurrentSuccess>(entity =>
            {
                entity.HasKey(e => e.Csid);

                entity.ToTable("CurrentSuccess");

                entity.Property(e => e.Csid).HasColumnName("CSID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.CurrentSuccess1s)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CurrentSuccess_BPMReference");
            });

            modelBuilder.Entity<FocusArea>(entity =>
            {
                entity.HasKey(e => e.Faid)
                    .HasName("PK_FocusAreas");

                entity.ToTable("FocusArea");

                entity.Property(e => e.Faid).HasColumnName("FAID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IncrementalCosts)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StrategicActions).IsUnicode(false);

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.FocusAreas)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_FocusArea_BPMReference");
            });

            modelBuilder.Entity<FocusAreaRequireGroup>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("date");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.Faid).HasColumnName("FAID");

                entity.Property(e => e.ModifiedAt).HasColumnType("date");

                entity.Property(e => e.ModifiedBy).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.HasOne(d => d.Fa)
                    .WithMany(p => p.FocusAreaRequireGroups)
                    .HasForeignKey(d => d.Faid)
                    .HasConstraintName("FK_FocusAreaRequireGroups_FocusAreaRequireGroups");
            });

            modelBuilder.Entity<FocusAreaSupportItf>(entity =>
            {
                entity.ToTable("FocusAreaSupportITF");

                entity.Property(e => e.CreatedAt).HasColumnType("date");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.Faid).HasColumnName("FAID");

                entity.Property(e => e.ModifiedAt).HasColumnType("date");

                entity.Property(e => e.ModifiedBy).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.HasOne(d => d.Fa)
                    .WithMany(p => p.FocusAreaSupportItfs)
                    .HasForeignKey(d => d.Faid)
                    .HasConstraintName("FK_FocusAreaSupportITF_FocusAreaSupportITF");
            });

            modelBuilder.Entity<IndexStat>(entity =>
            {
                entity.HasKey(e => e.IndexStatsId)
                    .HasName("PK_Table_1");

                entity.ToTable("IndexStats", "DBA");

                entity.Property(e => e.IndexStatsId).HasColumnName("IndexStatsID");

                entity.Property(e => e.BatchId).HasColumnName("BatchID");

                entity.Property(e => e.Dbname)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DBName");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IndexName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IndexType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProcessedDate).HasColumnType("datetime");

                entity.Property(e => e.SchemaName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatsDate).HasColumnType("datetime");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Individual>(entity =>
            {
                entity.HasKey(e => e.IndId);

                entity.ToTable("Individual");

                entity.Property(e => e.IndId).HasColumnName("IndID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Ops)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.People)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Sales)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.Individuals)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Individual_BPMReference");
            });

            //modelBuilder.Entity<Individual>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Individuals");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(50);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            //    entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Ops).HasMaxLength(50);

            //    entity.Property(e => e.People).HasMaxLength(50);

            //    entity.Property(e => e.Sales).HasMaxLength(50);
            //});

            modelBuilder.Entity<IntegratedTopFive>(entity =>
            {
                entity.HasKey(e => e.Itfid);

                entity.ToTable("IntegratedTopFive");

                entity.Property(e => e.Itfid).HasColumnName("ITFID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.IntegratedTopFives)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_IntegratedTopFive_BPMReference");
            });

            modelBuilder.Entity<LastDatabaseAccess>(entity =>
            {
                entity.ToTable("LastDatabaseAccess", "DBA");

                entity.Property(e => e.LastDatabaseAccessId).HasColumnName("LastDatabaseAccessID");

                entity.Property(e => e.DatabaseLastAccess).HasColumnType("datetime");

                entity.Property(e => e.DatabaseName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LastLogon>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LastLogon", "DBA");

                entity.Property(e => e.HostName)
                    .HasMaxLength(128)
                    .HasColumnName("host_name");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("login_name");

                entity.Property(e => e.LoginTime)
                    .HasColumnType("datetime")
                    .HasColumnName("login_time");

                entity.Property(e => e.ProgramName)
                    .HasMaxLength(128)
                    .HasColumnName("program_name");

                entity.Property(e => e.SampledDate)
                    .HasColumnType("datetime")
                    .HasColumnName("sampled_date");
            });

            modelBuilder.Entity<LeaseModification>(entity =>
            {
                entity.HasKey(e => e.Lmid);

                entity.Property(e => e.Lmid).HasColumnName("LMID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.LeaseModifications)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_LeaseModifications_BPMReference");
            });

            modelBuilder.Entity<LoginAccessTracking>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LoginAccessTracking", "DBA");

                entity.Property(e => e.DaysSinceLastLogon).HasColumnName("days_since_last_logon");

                entity.Property(e => e.DisabledDate)
                    .HasColumnType("datetime")
                    .HasColumnName("disabled_date");

                entity.Property(e => e.DropDate)
                    .HasColumnType("datetime")
                    .HasColumnName("drop_date");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("login_name");

                entity.Property(e => e.LoginSid)
                    .IsRequired()
                    .HasMaxLength(85)
                    .HasColumnName("login_sid");

                entity.Property(e => e.LoginTrackingId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("login_tracking_id");

                entity.Property(e => e.ReadyForDisabling).HasColumnName("ready_for_disabling");

                entity.Property(e => e.ResetDate)
                    .HasColumnType("datetime")
                    .HasColumnName("reset_date");
            });

            modelBuilder.Entity<MaintException>(entity =>
            {
                entity.ToTable("MaintException", "DBA");

                entity.Property(e => e.MaintExceptionId).HasColumnName("MaintExceptionID");

                entity.Property(e => e.ExceptionType)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ObjectName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ObjectType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SchemaName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            //modelBuilder.Entity<MissedOpportunity>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Missed_Opportunity");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(50);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.MissedOpportunity1).HasColumnName("MissedOpportunity");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            //    entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Year)
            //        .IsRequired()
            //        .HasMaxLength(50);
            //});

            modelBuilder.Entity<MissedOpportunity>(entity =>
            {
                entity.HasKey(e => e.Moid)
                    .HasName("PK__MissedOp__6DB360006D112FEA");

                entity.ToTable("MissedOpportunity");

                entity.Property(e => e.Moid).HasColumnName("MOID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MissedOpportunities).IsRequired();

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.MissedOpportunity1s)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MissedOpportunity_BPMReference");
            });

            modelBuilder.Entity<Narrative>(entity =>
            {
                entity.HasKey(e => e.Nid);

                entity.ToTable("Narrative");

                entity.Property(e => e.Nid).HasColumnName("NID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.Narratives)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_Narrative_BPMReference");
            });

            //modelBuilder.Entity<NationalService>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("National_Service");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.Nsname)
            //        .HasMaxLength(50)
            //        .HasColumnName("NSName");
            //});

            modelBuilder.Entity<NationalService>(entity =>
            {
                entity.HasKey(e => e.Nsid);

                entity.ToTable("NationalService");

                entity.Property(e => e.Nsid).HasColumnName("NSID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Nsname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("NSName");
            });

            //modelBuilder.Entity<NextBigIdea>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Next_BigIdeas");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(50);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            //    entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Year)
            //        .IsRequired()
            //        .HasMaxLength(50);
            //});

            modelBuilder.Entity<NextBigIdea>(entity =>
            {
                entity.HasKey(e => e.Nbiid);

                entity.ToTable("NextBigIdeas");

                entity.Property(e => e.Nbiid).HasColumnName("NBIID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NextBigIdeas).IsRequired();

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.NextBigIdea1s)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NextBigIdeas_BPMReference");
            });

            //modelBuilder.Entity<NextChallenge>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Next_Challenges");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(50);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            //    entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Year)
            //        .IsRequired()
            //        .HasMaxLength(50);
            //});

            modelBuilder.Entity<NextChallenge>(entity =>
            {
                entity.HasKey(e => e.Ncid)
                    .HasName("PK__NextChal__D945EEF781900A14");

                entity.ToTable("NextChallenges");

                entity.Property(e => e.Ncid).HasColumnName("NCID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NextChallenges).IsRequired();

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.NextChallenge1s)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NextChallenges_BPMReference");
            });

           

            modelBuilder.Entity<NextOpportunity>(entity =>
            {
                entity.HasKey(e => e.Noid);

                entity.ToTable("NextOpportunities");

                entity.Property(e => e.Noid).HasColumnName("NOID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NextOpportunities).IsRequired();

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.NextOpportunity1s)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NextOpportunities_BPMReference");
            });

            modelBuilder.Entity<Office>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Offices");

                entity.Property(e => e.BpmId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("BPM_ID");

                entity.Property(e => e.OfficeName).HasMaxLength(50);
            });

            modelBuilder.Entity<PredefinedRequireGroup>(entity =>
            {
                entity.HasKey(e => e.RequireGroupId);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Year).HasMaxLength(10);
            });

            modelBuilder.Entity<PredefinedSupportItf>(entity =>
            {
                entity.HasKey(e => e.SupportItfId);

                entity.ToTable("PredefinedSupportITF");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Year).HasMaxLength(10);
            });

            modelBuilder.Entity<RecessionPlanning>(entity =>
            {
                entity.HasKey(e => e.Csrpid)
                    .HasName("PK_CompanyStrategiesRecessionPlanning");

                entity.ToTable("RecessionPlanning");

                entity.Property(e => e.Csrpid).HasColumnName("CSRPID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.RecessionPlannings)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_CompanyStrategies_BPMReference");
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.ToTable("Region");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Mcsponser).HasColumnName("MCSponser");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Year)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("((2020))");
            });

            //modelBuilder.Entity<Region>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Regions");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.RegionName).HasMaxLength(50);
            //});

            //modelBuilder.Entity<SsNeed>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("SS_Needs");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(50);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            //    entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Year)
            //        .IsRequired()
            //        .HasMaxLength(50);
            //});

            modelBuilder.Entity<Ssneed>(entity =>
            {
                entity.HasKey(e => e.Ssnid);

                entity.ToTable("SSNeeds");

                entity.Property(e => e.Ssnid).HasColumnName("SSNID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Role).IsRequired();

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.Ssneed1s)
                    .HasForeignKey(d => d.Brid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SSNeeds_BPMReference");
            });

            modelBuilder.Entity<StatsDetail>(entity =>
            {
                entity.HasKey(e => e.StatsDetailsId)
                    .HasName("PK_StatsDetails_1");

                entity.ToTable("StatsDetails", "DBA");

                entity.Property(e => e.StatsDetailsId).HasColumnName("StatsDetailsID");

                entity.Property(e => e.BatchId).HasColumnName("BatchID");

                entity.Property(e => e.Dbname)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DBName");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("last_update");

                entity.Property(e => e.ProcessedDate).HasColumnType("datetime");

                entity.Property(e => e.SchemaName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StatsDate).HasColumnType("datetime");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            //modelBuilder.Entity<StrategicPartner>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Strategic_Partners");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.Spname)
            //        .HasMaxLength(50)
            //        .HasColumnName("SPName");
            //});

            modelBuilder.Entity<StrategicPartner>(entity =>
            {
                entity.HasKey(e => e.Spid);

                entity.ToTable("StrategicPartners");

                entity.Property(e => e.Spid).HasColumnName("SPID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Spname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("SPName");
            });

            //modelBuilder.Entity<UtilizedOpportunity>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("Utilized_Opportunity");

            //    entity.Property(e => e.BpmId)
            //        .ValueGeneratedOnAdd()
            //        .HasColumnName("BPM_ID");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(50);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            //    entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.UtilizedOpportunity1).HasColumnName("UtilizedOpportunity");

            //    entity.Property(e => e.Year)
            //        .IsRequired()
            //        .HasMaxLength(50);
            //});

            modelBuilder.Entity<UtilizedOpportunity>(entity =>
            {
                entity.HasKey(e => e.Uoid);

                entity.ToTable("UtilizedOpportunity");

                entity.Property(e => e.Uoid).HasColumnName("UOID");

                entity.Property(e => e.Brid).HasColumnName("BRID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.UtilizedOpportunities).IsRequired();

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Br)
                    .WithMany(p => p.UtilizedOpportunity1s)
                    .HasForeignKey(d => d.Brid)
                    .HasConstraintName("FK_UtilizedOpportunity_BPMReference");
            });

            modelBuilder.HasSequence("IndexStatsSeq", "DBA");

            modelBuilder.HasSequence("StatsDetailsSeq", "DBA");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
