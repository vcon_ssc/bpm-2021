﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class CapitalExpense
    {
        public int Ceid { get; set; }
        public int? Brid { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string Year { get; set; }
        public int? SortOrder { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Bpmreference Br { get; set; }
    }
}
