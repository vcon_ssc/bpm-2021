﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class LastLogon
    {
        public DateTime SampledDate { get; set; }
        public DateTime? LoginTime { get; set; }
        public string HostName { get; set; }
        public string ProgramName { get; set; }
        public string LoginName { get; set; }
    }
}
