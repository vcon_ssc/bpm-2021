﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class CoreMarket
    {
        public CoreMarket()
        {
            Bpmreferences = new HashSet<Bpmreference>();
        }

        public int Cmid { get; set; }
        public string Cmname { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<Bpmreference> Bpmreferences { get; set; }
    }
}
