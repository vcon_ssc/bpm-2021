﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class FocusArea
    {
        public FocusArea()
        {
            FocusAreaRequireGroups = new HashSet<FocusAreaRequireGroup>();
            FocusAreaSupportItfs = new HashSet<FocusAreaSupportItf>();
        }

        public int Faid { get; set; }
        public int? Brid { get; set; }
        public string StrategicActions { get; set; }
        public bool? IsImpact { get; set; }
        public bool? RequireHelp { get; set; }
        public bool? OngoingEffort { get; set; }
        public string IncrementalCosts { get; set; }
        public string Year { get; set; }
        public int? SortOrder { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Bpmreference Br { get; set; }
        public virtual ICollection<FocusAreaRequireGroup> FocusAreaRequireGroups { get; set; }
        public virtual ICollection<FocusAreaSupportItf> FocusAreaSupportItfs { get; set; }
    }
}
