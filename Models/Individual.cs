﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class Individual
    {
        public int IndId { get; set; }
        public int Brid { get; set; }
        public string IndividualName { get; set; }
        public string CurrentRole { get; set; }
        public string NextRole { get; set; }
        public bool IsAvailable { get; set; }
        public string Sales { get; set; }
        public string Ops { get; set; }
        public string People { get; set; }
        public int? SortOrder { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Bpmreference Br { get; set; }
    }
}
