﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class LoginAccessTracking
    {
        public int LoginTrackingId { get; set; }
        public string LoginName { get; set; }
        public byte[] LoginSid { get; set; }
        public int DaysSinceLastLogon { get; set; }
        public DateTime? ResetDate { get; set; }
        public bool ReadyForDisabling { get; set; }
        public DateTime? DisabledDate { get; set; }
        public DateTime? DropDate { get; set; }
    }
}
