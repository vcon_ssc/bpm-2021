﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class IndexStat
    {
        public int IndexStatsId { get; set; }
        public int BatchId { get; set; }
        public string Dbname { get; set; }
        public string TableName { get; set; }
        public string SchemaName { get; set; }
        public string IndexName { get; set; }
        public string IndexType { get; set; }
        public double FragPercentage { get; set; }
        public long IndexSize { get; set; }
        public int? IndexRowCount { get; set; }
        public DateTime? StatsDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsProcessed { get; set; }
        public DateTime? ProcessedDate { get; set; }
    }
}
