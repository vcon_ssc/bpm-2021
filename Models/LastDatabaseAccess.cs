﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class LastDatabaseAccess
    {
        public int LastDatabaseAccessId { get; set; }
        public string DatabaseName { get; set; }
        public DateTime DatabaseLastAccess { get; set; }
    }
}
