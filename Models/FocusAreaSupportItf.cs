﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class FocusAreaSupportItf
    {
        public int Id { get; set; }
        public int? Faid { get; set; }
        public int? SupportItfId { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual FocusArea Fa { get; set; }
    }
}
