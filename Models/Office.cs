﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class Office
    {
        public int BpmId { get; set; }
        public string OfficeName { get; set; }
    }
}
