﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class CorporateServiceGroup
    {
        public int Csgid { get; set; }
        public string Csgname { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
