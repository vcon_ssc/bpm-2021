﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class MaintException
    {
        public int MaintExceptionId { get; set; }
        public string TableName { get; set; }
        public string SchemaName { get; set; }
        public string ObjectName { get; set; }
        public string ObjectType { get; set; }
        public bool SkipThisObject { get; set; }
        public string ExceptionType { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
