﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class Bpmreference
    {
        public Bpmreference()
        {
            CapitalExpenses = new HashSet<CapitalExpense>();
            CurrentChallenge1s = new HashSet<CurrentChallenge>();
            CurrentOpportunity1s = new HashSet<CurrentOpportunity>();
            CurrentSuccess1s = new HashSet<CurrentSuccess>();
            FocusAreas = new HashSet<FocusArea>();
            Individuals = new HashSet<Individual>();
            IntegratedTopFives = new HashSet<IntegratedTopFive>();
            LeaseModifications = new HashSet<LeaseModification>();
            MissedOpportunity1s = new HashSet<MissedOpportunity>();
            Narratives = new HashSet<Narrative>();
            NextBigIdea1s = new HashSet<NextBigIdea>();
            NextChallenge1s = new HashSet<NextChallenge>();
            NextOpportunity1s = new HashSet<NextOpportunity>();
            RecessionPlannings = new HashSet<RecessionPlanning>();
            Ssneed1s = new HashSet<Ssneed>();
            UtilizedOpportunity1s = new HashSet<UtilizedOpportunity>();
        }

        public int Brid { get; set; }
        public string Year { get; set; }
        public int? RegionId { get; set; }
        public int? Buid { get; set; }
        public int? Spid { get; set; }
        public int? Nsid { get; set; }
        public int? Cmid { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual BusinessUnit Bu { get; set; }
        public virtual CoreMarket Cm { get; set; }
        public virtual NationalService Ns { get; set; }
        public virtual Region Region { get; set; }
        public virtual StrategicPartner Sp { get; set; }
        public virtual ICollection<CapitalExpense> CapitalExpenses { get; set; }
        public virtual ICollection<CurrentChallenge> CurrentChallenge1s { get; set; }
        public virtual ICollection<CurrentOpportunity> CurrentOpportunity1s { get; set; }
        public virtual ICollection<CurrentSuccess> CurrentSuccess1s { get; set; }
        public virtual ICollection<FocusArea> FocusAreas { get; set; }
        public virtual ICollection<Individual> Individuals { get; set; }
        public virtual ICollection<IntegratedTopFive> IntegratedTopFives { get; set; }
        public virtual ICollection<LeaseModification> LeaseModifications { get; set; }
        public virtual ICollection<MissedOpportunity> MissedOpportunity1s { get; set; }
        public virtual ICollection<Narrative> Narratives { get; set; }
        public virtual ICollection<NextBigIdea> NextBigIdea1s { get; set; }
        public virtual ICollection<NextChallenge> NextChallenge1s { get; set; }
        public virtual ICollection<NextOpportunity> NextOpportunity1s { get; set; }
        public virtual ICollection<RecessionPlanning> RecessionPlannings { get; set; }
        public virtual ICollection<Ssneed> Ssneed1s { get; set; }
        public virtual ICollection<UtilizedOpportunity> UtilizedOpportunity1s { get; set; }
    }
}
