﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BPM.Models
{
    public partial class MissedOpportunity
    {
        public int Moid { get; set; }
        public int Brid { get; set; }
        public string MissedOpportunities { get; set; }
        public string Year { get; set; }
        public bool IsDeleted { get; set; }
        public int? SortOrder { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Bpmreference Br { get; set; }
    }
}
