import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { BpmReference } from '../models/bpmreference.model';
import { BusinessUnit } from '../models/businessUnit.model';
import { CoreMarket } from '../models/CoreMarket.model';
import { NationalService } from '../models/nationalService.model';
import { Region } from '../models/region.model';
import { StrategicPartner } from '../models/strategicPartner.model';
import { BpmComponentService } from '../services/bpm-component.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-see-my-sheet-landing-page',
  templateUrl: './see-my-sheet-landing-page.component.html',
  styleUrls: ['./see-my-sheet-landing-page.component.css']
})
export class SeeMySheetLandingPageComponent implements OnInit {

  mergeData: any = [];
  oldItem: any;
  finalDataArray: any[] = [];
  bpmReference: any[];
  addNewRow = {
    Year: "",
    RegionName: "Select DPR Group",
    CMID: 0,
    BUID: 0,
    RegionID: 0,
    SPID: 0,
    NSID: 0
  };
  isEdit: boolean[];
  businessUnits: BusinessUnit[];
  regions: Region[];
  strategicPartners: StrategicPartner[];
  nationalServices: NationalService[];
  coreMarkets: CoreMarket[];
  successMessage: string;
  alertType: string;
  searchText: any;
  addRow = false;
  count = 1;
  selectedRow: BpmReference;
  years: string[] = ["2019", "2020", '2021'];
  selectedYear: string;
  landingPage: boolean = false;
  topFiveAccordian: boolean = false;
  nextyearFiveAccordian: boolean = false;

  name: string;
  constructor(private data: DataService, private spinner: NgxSpinnerService, private bpmComponentService: BpmComponentService, private route: ActivatedRoute, private router: Router) {
    this.data.changeTitle(this.landingPage);

  }

  ngOnInit() {
    this.spinner.show();
    this.selectedYear = localStorage.getItem('selectedYear');
    this.bpmReference = [];
    if (this.selectedYear == "2019") {
      this.topFiveAccordian = true;
    }
    else {
      this.nextyearFiveAccordian = true;
    }
    this.getRegions();
  }

  public DownloadFile() {
    this.spinner.show();
    var pdfDownload = document.createElement("a");
    document.body.appendChild(pdfDownload);

    const fileName = this.selectedYear + "Top5Sheet.pdf";
    this.bpmComponentService.exportMyTop5ToPDF(this.selectedYear).subscribe(result => {
      this.spinner.hide();
      console.log("fgh", result);
      var fileBlob = new Blob([result], { type: 'application/pdf' });
      if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
        window.navigator.msSaveBlob(fileBlob, fileName);
      }
      else {
        var url = window.URL.createObjectURL(fileBlob);
        pdfDownload.href = url;
        pdfDownload.download = fileName;
        pdfDownload.click();
      }

    }, (err: any) => {
      this.spinner.hide();
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });
  }

  hideErrorMessage() {
    setTimeout(() => {
      this.successMessage = null;
    }, 1000);
  }

  navigateToDashboard(item: any) {
    localStorage.setItem('RegionName', item.RegionName);
    localStorage.setItem('showActions', 'yes');
    this.router.navigate(['dashboard/' + item.BRID]);
  }

  navigateToViewDashboard(item: any) {
    localStorage.setItem('RegionName', item.RegionName);
    localStorage.setItem('showActions', 'yes');
    this.router.navigate(['dashboardView/' + item.BRID]);
  }

  getRegions() {
    this.bpmComponentService.RegionByResPer(this.selectedYear).subscribe((data: Region[]) => {
      console.log(data);
      console.log("Region Count:" + data.length);
      this.regions = data;
      this.getAllBpmReferences();

    }, (err: any) => {
      this.spinner.hide();
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });
  }

  getAllBpmReferences() {
    //getTable
    this.bpmComponentService.getBpmRefrencesByRespPer(this.selectedYear).subscribe((data: BpmReference[]) => {
      this.bpmReference = data;

      this.bpmReference.forEach(obj => {
        if (obj.GroupName == 'CM') {
          obj.GroupName = 'CORE MARKET GROUPS'
        }
        else if (obj.GroupName == 'CSG') {
          obj.GroupName = 'CORPORATE SERVICE GROUPS'
        }
        else if (obj.GroupName == 'SP') {
          obj.GroupName = 'STRATEGIC BUSINESS PARTNERS'
        }
        else if (obj.GroupName == 'WVE') {
          obj.GroupName = 'WND VENTURES ENTITIES'
        }
        else {
          obj.GroupName = 'REGIONS & BUSINESS UNITS'
        }
      });
      this.spinner.hide();
      this.filterAllData(data);
      console.log(data);
    }, (err: any) => {
      this.spinner.hide();
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });

  }
  filterAllData(data: BpmReference[]) {
    let regionsData: any = [];
    this.regions.forEach((value, index1, array) => {
      var found = data.some(function (el) {
        return el.RegionID == value.RegionID;
      });
      if (!found) {
        regionsData.push(value);
      }
    });

    this.regions = regionsData;

  }

  enableEdit(event: any, selectedRow: any, i: number) {
    this.isEdit[i] = false;
    this.selectedRow = selectedRow;
    this.oldItem = Object.assign({}, selectedRow);
    if (!this.selectedRow.RegionID) {
      this.selectedRow.RegionName = "Select DPR Group";
    }
  }
  Update(event: any, selectedRow: any, i: number) {
    this.isEdit[i] = true;
    this.selectedRow = selectedRow;

    this.bpmComponentService.editBpmRefrence(this.selectedRow).subscribe((data: BpmReference) => {
      console.log(data);

      setTimeout(() => {
        this.getAllBpmReferences();
        this.alertType = 'success';
        this.successMessage = 'Item Updated Successfully.';
        this.hideErrorMessage();
      }, 1000);

    }, (err: any) => {
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });

  }
  reset(event: any, item: any, i: number) {
    this.isEdit[i] = true;
    this.bpmReference[i] = this.oldItem;
  }
  cancel(event: any, item: any) {
    this.addRow = false;
    this.count--;
  }
  AddNewRow(event: any) {
    this.addRow = true;
    this.count++;
    this.addNewRow = {
      Year: "",
      RegionName: "Select DPR Group",
      CMID: null,
      BUID: null,
      RegionID: null,
      SPID: null,
      NSID: null
    };
  }

  addNewItem(event: any) {

    let addNewRow = {
      Year: this.selectedYear,
      RegionID: this.addNewRow.RegionID,
      BUID: this.addNewRow.BUID,
      SPID: this.addNewRow.SPID,
      NSID: this.addNewRow.NSID,
      CMID: this.addNewRow.CMID
    }

    this.bpmComponentService.addBpmRefrence(addNewRow as BpmReference).subscribe(
      (data: BpmReference) => {
        console.log(data);
        setTimeout(() => {
          //this.getAllBpmReferences();
          this.getRegions();
          this.alertType = 'success';
          this.successMessage = 'Item Added Successfully.';
          this.hideErrorMessage();
        }, 1000);
        this.addRow = false;
      }, (err: any) => {
        console.log(err);
        this.alertType = 'danger';
        this.successMessage = err.error.Message;
        this.hideErrorMessage();
      }
    );
  }

  ChangeRegion(menu: any, item: BpmReference) {
    item.RegionName = menu.RegionName;
    item.RegionID = menu.RegionID;
  }

}