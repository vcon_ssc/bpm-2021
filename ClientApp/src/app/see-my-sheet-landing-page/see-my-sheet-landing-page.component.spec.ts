import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeMySheetLandingPageComponent } from './see-my-sheet-landing-page.component';

describe('SeeMySheetLandingPageComponent', () => {
  let component: SeeMySheetLandingPageComponent;
  let fixture: ComponentFixture<SeeMySheetLandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeMySheetLandingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeMySheetLandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
