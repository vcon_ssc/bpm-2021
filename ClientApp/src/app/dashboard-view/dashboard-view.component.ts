import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['../dashboard/dashboard.component.css']
})
export class DashboardViewComponent implements OnInit {
  selectedYear: string;
  currentYear: string;
  landingPage = false;
  topFiveAccordian: boolean = false;
  nextyearFiveAccordian: boolean = false;
  constructor(private data: DataService) {
    this.data.changeTitle(this.landingPage);
  }

  ngOnInit() {
    this.data.currentMessage1.subscribe(message1 => this.landingPage = message1);
    this.data.currentMessage.subscribe(message => this.selectedYear = message);
    if (this.selectedYear) {
      this.currentYear = this.selectedYear;
      if (this.selectedYear == "2019") {
        this.topFiveAccordian = true;
      }
      else {
        this.nextyearFiveAccordian = true;
      }
    }

  }
}
