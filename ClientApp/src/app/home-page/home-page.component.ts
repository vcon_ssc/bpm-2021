import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BpmComponentService } from '../services/bpm-component.service';
import { DataService } from '../services/data.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  regions: any[]=[];
  successMessage: string;
  alertType: string;
  caseOneAccordian: boolean = false;
  caseTwoAccordian: boolean = false;
  caseThreeAccordian: boolean = false;
  selectedRegion = {
    regionId: 0,
    regionName: 'Select Group'
  };

  years: string[] = [];
  nextYear = (new Date().getFullYear() + 2).toString();
  currentYear = (new Date().getFullYear() + 1).toString();
  selectedYear: string = (new Date().getFullYear() + 1).toString();
  landingPage: boolean = true;
  groupName: any;
  showValidation: boolean = false;
  ChangeYear(menu: string) {
    this.selectedYear = menu;
    this.data.changeMessage(this.selectedYear);
    localStorage.setItem('selectedYear', this.selectedYear);
  }
  constructor(private data: DataService, private spinner: NgxSpinnerService, private bpmComponentService: BpmComponentService, private router: Router) {
    this.data.changeMessage(this.selectedYear);
    this.data.changeTitle(this.landingPage);
    console.log(this.selectedYear);
    console.log(this.currentYear);
  }
  ChangeRegion(region: any) {
    this.selectedRegion = region;
    localStorage.setItem('RegionName', this.selectedRegion.regionName);
    this.showValidation = false;
  }

  ngOnInit() {
    let year = '2019'
    while (year !== this.nextYear) {
      this.years.push(year)
      year = (parseInt(year) + 1).toString();
    }
    this.getRegions();

    // this.data.currentMessage.subscribe(message => { this.selectedYear = message;  });
    // this.data.currentMessage.subscribe(message => this.selectedYear = message);
    // this.data.currentMessage1.subscribe(message1 => this.landingPage = message1);
    //this.data.changeTitle(this.landingPage);
    localStorage.removeItem('RegionName');
    localStorage.removeItem('showActions');
    localStorage.setItem('selectedYear', this.selectedYear);

  }
  hideErrorMessage() {
    setTimeout(() => {
      this.successMessage = null;
    }, 1000);
  }

  getRegions() {
    this.spinner.show();
    this.bpmComponentService.getRegionNameByRespPer(this.selectedYear).subscribe((data: any[]) => {
      this.regions = data;
      this.spinner.hide();
    }, (err: any) => {
      this.spinner.hide();
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });

  }


  //getAllRegions() {
  //    this.bpmComponentService.getRegionsForYear(this.selectedYear).subscribe((data: Region[]) => {
  //        // console.log(data);
  //        // console.log("Region Count:" + data.length);
  //        this.regions = data;
  //        //this.getAllBpmReferences();
  //    }, (err: any) => {
  //        console.log(err);
  //        this.alertType = 'danger';
  //        this.successMessage = err.error.Message;
  //        this.hideErrorMessage();
  //    });

  //}

  //getMyRegions() {
  //    this.bpmComponentService.getRegionNameByRespPer().subscribe((data: Region[]) => {
  //        // console.log(data);
  //        // console.log("Region Count:" + data.length);
  //        this.regions = data;
  //        //this.getAllBpmReferences();
  //    }, (err: any) => {
  //        console.log(err);
  //        this.alertType = 'danger';
  //        this.successMessage = err.error.Message;
  //        this.hideErrorMessage();
  //    });
  //}

  navigateToDashboard() {
    if (!this.selectedRegion.regionId) {
      this.showValidation = true;
      return;
    }

    this.bpmComponentService.getBRIDByYearRegId(this.selectedYear, this.selectedRegion.regionId).subscribe((data: any) => {
      console.log("Count:" + data);
      this.router.navigate(['dashboard/', data]);
    }, (err: any) => {
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });


  }


}
