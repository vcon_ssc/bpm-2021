import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  selectedYear: string;
  // nextYear: string;
     currentYear: string;
     BRID: any;
   landingPage = false;
     topFiveAccordian: boolean = false;
     nextyearFiveAccordian: boolean = false;
     constructor(private data: DataService, private route: ActivatedRoute) {
       this.data.changeTitle(this.landingPage);
       this.data.currentMessage1.subscribe(message1  => this.landingPage = message1);
       this.data.currentMessage.subscribe(message => this.selectedYear = message);
       if (this.selectedYear) {
           //let year =this.selectedYear.split('-');
           //this.currentYear =year[0];
           //this.nextYear = year[1];
           this.currentYear = this.selectedYear;
           if (this.selectedYear === "2019") {
               this.topFiveAccordian = true;
           }
           else {
               this.nextyearFiveAccordian = true;
           }
       }
    }
 
     ngOnInit() {
         this.BRID = this.route.snapshot.paramMap.get('id');
   }
 }
 
