import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  getDetails  =  false;
  constructor() {
  }

  ngOnInit() {
  }
  public showDetails() {
      this.getDetails = true;
     
  }    
}

