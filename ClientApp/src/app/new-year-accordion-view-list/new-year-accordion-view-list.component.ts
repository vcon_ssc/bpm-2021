import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { NgbAccordionConfig, NgbPanelChangeEvent, NgbAccordion, NgbDropdown, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { CapitalExpenses } from '../models/capitalExpenses.model';
import { FocusAreas } from '../models/focusAreas.model';
import { IntegratedTopFive } from '../models/integratedTopFive';
import { LeaseModifications } from '../models/leaseModifications.model';
import { Narrative } from '../models/Narrative';
import { RecessionPlanning } from '../models/recessionPlanning.model';
import { DataService } from '../services/data.service';
import { BpmComponentService } from '../services/bpm-component.service';

@Component({
  selector: 'app-new-year-accordion-view-list',
  templateUrl: './new-year-accordion-view-list.component.html',
  styleUrls: ['../next-year-accordian-menu-list/next-year-accordian-menu-list.component.css']
})
export class NewYearAccordionViewListComponent implements OnInit {

  RegionName: string;
  bpmReferenceId: number;
  activeIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map(i => `ngb-panel-${i}`);
  prevYear: string;
  currentDate: string;
  userName = "";
  caseOneAccordian: boolean = false;
  caseThreeAccordian: boolean = false;
  caseTwoAccordian: boolean = false;
  caseFourAccordian: boolean = false;
  @Input() currentYear: string;
  angForm: FormGroup;
  dropdownSettings: IDropdownSettings = {
      singleSelection: false,
      idField: 'RequireGroupId',
      textField: 'Name',
      //selectAllText: 'Select All',
      //unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      //allowSearchFilter: true
  };

  dropdownSettings2: IDropdownSettings = {
      singleSelection: false,
      idField: 'SupportItfId',
      textField: 'Name',
      //selectAllText: 'Select All',
      //unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      //allowSearchFilter: true
  };


  groupName: any;
  RBFA: string;
  RBLM: string;
  RBCE: string;
  FA: string;
  LM: string;
  CE: string;
  dropdownList: any;
  ITFDropdown: any;
  constructor(config: NgbDropdownConfig, private spinner: NgxSpinnerService, private fb: FormBuilder,
    private activatedRoute: ActivatedRoute, private bpmComponentService: BpmComponentService, private data: DataService) {
      config.placement = 'top-left';
      //config.autoClose = false;
      this.createForm();
  }

  createForm() {
      this.angForm = this.fb.group({
          narrativename: ['', Validators.required],
          narrativenameadd: ['', Validators.required],
          succname: ['', Validators.required],
          succnameadd: ['', Validators.required],
          leasename: ['', Validators.required],
          leasenameadd: ['', Validators.required],
          datename: ['', Validators.required],
          datenameadd: ['', Validators.required],
          planname: ['', Validators.required],
          plannameadd: ['', Validators.required],
          descriptionname: ['', Validators.required],
          descriptionnameadd: ['', Validators.required],
          amountname: ['', Validators.required],
          amountnameadd: ['', Validators.required],
          planningname: ['', Validators.required],
          planningnameadd: ['', Validators.required],
          description: ['', Validators.required],
          descriptionadd: ['', Validators.required],
          expensename: ['', Validators.required],
          expensenameadd: ['', Validators.required],
          category: ['', Validators.required],
          categoryadd: ['', Validators.required],
          integrated: ['', Validators.required],
          integratedadd: ['', Validators.required]

      });
  }

  ngOnInit() {
      this.spinner.show();
      setTimeout(() => {
          /** spinner ends after 7 seconds */
          this.spinner.hide();
      }, 5000);
      this.activatedRoute.params.subscribe((params: Params) => {
          this.bpmReferenceId = params['id'];
      });
      console.log("bpmrefernce id ", this.bpmReferenceId);
      if (this.currentYear === "Select Year" || !this.currentYear) {
          this.currentYear = localStorage.getItem('selectedYear');
          this.data.changeMessage(this.currentYear);
      }
      this.bpmComponentService.getGroupByBRId(this.bpmReferenceId, this.currentYear).subscribe((data: any) => {
          this.groupName = data[0].GroupName;
          if (data[0].GroupName === "RBU") {
              this.caseOneAccordian = true;
              this.RBFA = "Please identify the Top Priorities that your Business Unit or Region will take in" + this.currentYear + "to improve its performance.";
              this.RBLM = "Is your work  group planning to lease any additional space or have a lease that is reaching expiration in" + this.currentYear + "?   If so, how much net additional space are you anticipating leasing or what is the lease expiration date and what are your plans for renewal?";
              this.RBCE = "Is your work group planning any major capital expenses in " + this.currentYear + " over $250,000 in a single transaction (leasehold improvements; OES equipment including formwork; etc..)  If so, please list those items and identify the anticipated quarter that the cash outlay would hit the books.";
          }
          else if (data[0].GroupName==='IP') {
              this.caseThreeAccordian = true;
          }
          else if (data[0].GroupName === "CSG" || data[0].GroupName === "CM") {
              this.caseTwoAccordian = true;
              this.FA = "Please identify the Top Priorities that your work group will take in " + this.currentYear + " to improve its performance.";
              this.LM = "Is your work  group planning to lease any additional space or have a lease that is reaching expiration in " + this.currentYear + "?   If so, how much net additional space are you anticipating leasing or what is the lease expiration date and what are your plans for renewal?";
              this.CE = "Is your work group planning any major capital expenses in " + this.currentYear + " over $250,000 in a single transaction (leasehold improvements; OES equipment including formwork; etc..)  If so, please list those items and identify the anticipated quarter that the cash outlay would hit the books.";
          }
          if (data[0].GroupName === "SP" || data[0].GroupName === "RBU" || data[0].GroupName === "CM" || data[0].GroupName === "WVE") {
              this.FA = "Please identify the Top Priorities that your work group will take in " + this.currentYear + " to improve its performance.";
              this.LM = "Is your work  group planning to lease any additional space or have a lease that is reaching expiration in " + this.currentYear + "?   If so, how much net additional space are you anticipating leasing or what is the lease expiration date and what are your plans for renewal?";
              this.CE = "Is your work group planning any major capital expenses in " + this.currentYear + " over $250,000 in a single transaction (leasehold improvements; OES equipment including formwork; etc..)  If so, please list those items and identify the anticipated quarter that the cash outlay would hit the books.";

              this.caseFourAccordian = true;
          }
          console.log("Count:" + data.length);

      }, (err: any) => {
          console.log(err);
      });

      this.bpmComponentService.RequireGroupsAll().subscribe((data: any) => {
          this.dropdownList = data;
      });
      this.bpmComponentService.SupportITFAll().subscribe((data: any) => {
          this.ITFDropdown = data;
      });
      this.prevYear = "" + (parseInt(this.currentYear) - 1);
      this.RegionName = localStorage.getItem('RegionName');
      this.tableList.narrative.initializeData(this.bpmComponentService, this.spinner, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
      this.tableList.focusAreas.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
      this.tableList.leaseModifications.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
      this.tableList.capitalExpenses.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
      this.tableList.recessionPlanning.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
      this.tableList.integratedTopFive.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
  }

  getCurrentDate() {
      let today = new Date();
      let dd = today.getDate();
      let mm = today.getMonth() + 1; //January is 0!
      let yyyy = today.getFullYear();
      let hh = today.getHours();
      let MM = today.getMinutes();
      return mm + '/' + dd + '/' + yyyy;
      //this.currentDate = (dd < 10) ? '0' + dd as string : dd + '/' + (mm < 10) ? '0' + mm as string : mm + '/' + yyyy;
      //document.write(today1);
  }

  tableList = {
      narrative: {
          titles: ['Description'],
          initializeData(bpmComponentService: BpmComponentService, spinner: NgxSpinnerService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
              this.currentDate = currentDate;
              this.bpmReferenceId = bpmReferenceId;
              this.bpmComponentService = bpmComponentService;
              this.userName = userName;
              this.currentYear = currentYear;
              this.spinner = spinner;
              this.addNewRow = {};
              this.list = [];
              this.updatedData = "";
              this.listAllData();
              
              this.isEditenabled = true;
          },
          listAllData() {
              //************************************************
              this.bpmComponentService.narrativeAll(this.bpmReferenceId, this.currentYear).subscribe((data: Narrative[]) => {
                  this.list = data;
                  console.log(this.list);
                  
                 
                  this.isEdit = [];
                  this.count = data.length;
                  if (this.list.length !== 0) {
                      this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                  }
                  this.isEditenabled = true;
                  for (let i = 0; i < data.length; i++) {
                      this.isEdit[i] = true;
                  }
              }, (err: any) => {
                  this.spinner.hide();
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          hideErrorMessage() {
              setTimeout(() => {
                  this.successMessage = null;
              }, 2000);
          },
          successMessage: '',
          alertType: '',
          addRow: false,
          selectedRow: {
          },
          Update(event: any, selectedRow: any, i: number) {
              this.spinner.show();
              this.isEdit[i] = true;
              this.isEditenabled = true;
              this.selectedRow = selectedRow;
              this.selectedRow.ModifiedDate = this.currentDate;
              //***********************************
              this.bpmComponentService.editNarrative(this.selectedRow as Narrative).subscribe(
                  (data: Narrative) => {
                      console.log(data);

                      setTimeout(() => {
                          this.listAllData();
                          this.spinner.hide();
                          this.alertType = 'success';
                          this.successMessage = 'Item Updated Successfully.';
                          this.hideErrorMessage();
                      }, 1000);

                  }, (err: any) => {
                      this.spinner.hide();
                      console.log(err);
                      this.alertType = 'danger';
                      this.successMessage = err.error.Message;
                      this.hideErrorMessage();
                  }
              );
          },
          enableEdit(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = false;
              this.isEditenabled = false;
              this.selectedRow = selectedRow;
              this.oldItem = Object.assign({}, selectedRow);
          },
          cancel(event: any) {
              this.addRow = false;
              this.count--;
          },
          reset(event: any, i: number) {
              this.list[i] = this.oldItem;
              this.isEdit[i] = true;
              this.isEditenabled = true;
          },
          AddNewRow(event: any) {
              this.addRow = true;
              this.count = (this.list) ? this.list.length + 1 : 1;
              this.addNewRow = {
                  //CSID: 1,
                  Description: "",
                  Year: this.currentYear, //this.currentYear,// need to do dynamic
                  SortOrder: 0,
                  CreatedBy: this.userName,//need to pass user login data
                  ModifiedBy: this.userName,
                  CreatedDate: this.currentDate,
                  ModifiedDate: this.currentDate,
                  BRID: this.bpmReferenceId
              }
          },
          // API for Add
          addNewItem(event: any) {
              this.spinner.show();
              //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
              //**************************
              this.bpmComponentService.addNarrative(this.addNewRow as Narrative).subscribe((data: Narrative) => {
                  console.log(data);
                  setTimeout(() => {
                      this.spinner.hide();
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Added Successfully.';
                      this.hideErrorMessage();
                  }, 1000);

                  this.addRow = false;
                  this.addNewRow.narrative = "";
              }, (err: any) => {
                  this.spinner.hide();
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          deleteItem(event: any, item: any) {
              this.spinner.show();
              this.bpmComponentService.deleteNarrative(item.NID).subscribe((data: any) => {
                  setTimeout(() => {
                      this.spinner.hide();
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Deleted Successfully.';
                      this.hideErrorMessage();
                  }, 500);
              }, (err: any) => {
                  this.spinner.hide();
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          }
      },
      focusAreas: {
          titles: ['#', 'Description'],
          initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
              this.currentDate = currentDate;
              this.bpmReferenceId = bpmReferenceId;
              this.bpmComponentService = bpmComponentService;
              this.userName = userName;
              this.currentYear = currentYear;
              this.addNewRow = {};
              this.list = [];
              this.updatedData = "";
              this.listAllData();
              this.isEditenabled = true;
          },
          listAllData() {
              this.bpmComponentService.FocusAreasAll(this.bpmReferenceId, this.currentYear).subscribe((data: FocusAreas[]) => {
                  this.list = data;
                  if (this.list.length !== 0) {
                      this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                  }
                  console.log(this.list);
                  this.isEdit = [];
                  this.count = data.length;
                  this.isEditenabled = true;
                  for (let i = 0; i < data.length; i++) {
                      this.isEdit[i] = true;
                  }
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          hideErrorMessage() {
              setTimeout(() => {
                  this.successMessage = null;
              }, 2000);
          },
          searchText: 'abc',
          successMessage: '',
          alertType: '',
          addRow: false,
          selectedRow: {
          },
          Update(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = true;
              this.isEditenabled = true;
              this.selectedRow = selectedRow;
              this.selectedRow.ModifiedDate = this.currentDate;
              this.bpmComponentService.editFocusAreasItem(this.selectedRow as FocusAreas).subscribe(
                  (data: FocusAreas) => {
                      console.log(data);

                      setTimeout(() => {
                          this.listAllData();
                          this.alertType = 'success';
                          this.successMessage = 'Item Updated Successfully.';
                          this.hideErrorMessage();
                      }, 1000);

                  }, (err: any) => {
                      console.log(err);
                      this.alertType = 'danger';
                      this.successMessage = err.error.Message;
                      this.hideErrorMessage();
                  }
              );
          },
          enableEdit(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = false;
              this.isEditenabled = false;
              this.selectedRow = selectedRow;
              this.oldItem = Object.assign({}, selectedRow);
          },
          cancel(event: any) {
              this.addRow = false;
              this.count--;
          },
          reset(event: any, i: number) {
              this.list[i] = this.oldItem;
              this.isEdit[i] = true;
              this.isEditenabled = true;
          },
          AddNewRow(event: any) {
              this.addRow = true;
              this.count = (this.list) ? this.list.length + 1 : 1;
              this.addNewRow = {
                  //CSID: 1,
                  FocusAreas: "",
                  Year: this.currentYear, //this.currentYear,// need to do dynamic
                  SortOrder: 0,
                  CreatedBy: this.userName,//need to pass user login data
                  ModifiedBy: this.userName,
                  CreatedDate: this.currentDate,
                  ModifiedDate: this.currentDate,
                  BRID: this.bpmReferenceId
              }
          },
          // API for Add
          addNewItem(event: any) {
              //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
              this.bpmComponentService.addFocusAreasItem(this.addNewRow as FocusAreas).subscribe((data: FocusAreas) => {
                  console.log(data);
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Added Successfully.';
                      this.hideErrorMessage();
                  }, 1000);

                  this.addRow = false;
                  this.addNewRow.FocusAreas = "";
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          deleteItem(event: any, item: any) {
              this.bpmComponentService.deleteFocusAreasItem(item.FAID).subscribe((data: any) => {
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Deleted Successfully.';
                      this.hideErrorMessage();
                  }, 500);
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          }
      },
      leaseModifications: {
          titles: ['#', 'Lease Description', 'Current Expiration Date', 'Plan Of Renewal'],
          initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
              this.currentDate = currentDate;
              this.bpmReferenceId = bpmReferenceId;
              this.bpmComponentService = bpmComponentService;
              this.userName = userName;
              this.currentYear = currentYear;
              this.addNewRow = {};
              this.list = [];
              this.updatedData = "";
              this.listAllData();
              this.isEditenabled = true;
          },
          listAllData() {
              this.bpmComponentService.LeaseModificationsAll(this.bpmReferenceId, this.currentYear).subscribe((data: LeaseModifications[]) => {
                  this.list = data;
                  if (this.list.length !== 0) {
                      this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                  }
                  console.log(this.list);
                  this.isEdit = [];
                  this.count = data.length;
                  this.isEditenabled = true;
                  for (let i = 0; i < data.length; i++) {
                      this.isEdit[i] = true;
                  }
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          hideErrorMessage() {
              setTimeout(() => {
                  this.successMessage = null;
              }, 2000);
          },
          searchText: 'abc',
          successMessage: '',
          alertType: '',
          addRow: false,
          selectedRow: {
          },
          Update(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = true;
              this.isEditenabled = true;
              this.selectedRow = selectedRow;
              this.selectedRow.ModifiedDate = this.currentDate;
              this.bpmComponentService.editLeaseModificationsItem(this.selectedRow as LeaseModifications).subscribe(
                  (data: LeaseModifications) => {
                      console.log(data);

                      setTimeout(() => {
                          this.listAllData();
                          this.alertType = 'success';
                          this.successMessage = 'Item Updated Successfully.';
                          this.hideErrorMessage();
                      }, 1000);

                  }, (err: any) => {
                      console.log(err);
                      this.alertType = 'danger';
                      this.successMessage = err.error.Message;
                      this.hideErrorMessage();
                  }
              );
          },
          enableEdit(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = false;
              this.isEditenabled = false;
              this.selectedRow = selectedRow;
              this.oldItem = Object.assign({}, selectedRow);
          },
          cancel(event: any) {
              this.addRow = false;
              this.count--;
          },
          reset(event: any, i: number) {
              this.list[i] = this.oldItem;
              this.isEdit[i] = true;
              this.isEditenabled = true;
          },
          AddNewRow(event: any) {
              this.addRow = true;
              this.count = (this.list) ? this.list.length + 1 : 1;
              this.addNewRow = {
                  //CSID: 1,
                  LeaseModification: "",
                  Year: this.currentYear, //this.currentYear,// need to do dynamic
                  SortOrder: 0,
                  CreatedBy: this.userName,//need to pass user login data
                  ModifiedBy: this.userName,
                  CreatedDate: this.currentDate,
                  ModifiedDate: this.currentDate,
                  BRID: this.bpmReferenceId
              }
          },
          // API for Add
          addNewItem(event: any) {
              //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
              this.bpmComponentService.addLeaseModificationsItem(this.addNewRow as LeaseModifications).subscribe((data: LeaseModifications) => {
                  console.log(data);
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Added Successfully.';
                      this.hideErrorMessage();
                  }, 1000);

                  this.addRow = false;
                  this.addNewRow.LeaseModification = "";
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          deleteItem(event: any, item: any) {
              this.bpmComponentService.deleteLeaseModificationsItem(item.LMID).subscribe((data: any) => {
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Deleted Successfully.';
                      this.hideErrorMessage();
                  }, 500);
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          }
      },
      capitalExpenses: {
          titles: ['#', 'Capital Expenses Description', 'Amount'],
          initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
              this.currentDate = currentDate;
              this.bpmReferenceId = bpmReferenceId;
              this.bpmComponentService = bpmComponentService;
              this.userName = userName;
              this.currentYear = currentYear;
              this.addNewRow = {};
              this.list = [];
              this.updatedData = "";
              this.listAllData();
              this.isEditenabled = true;
          },
          listAllData() {
              this.bpmComponentService.CapitalExpensesAll(this.bpmReferenceId, this.currentYear).subscribe((data: CapitalExpenses[]) => {
                  this.list = data;

                  if (this.list.length !== 0) {
                      this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                  }
                  console.log(this.list);
                  this.isEdit = [];
                  this.count = data.length;
                  this.isEditenabled = true;
                  for (let i = 0; i < data.length; i++) {
                      this.isEdit[i] = true;
                  }
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          hideErrorMessage() {
              setTimeout(() => {
                  this.successMessage = null;
              }, 2000);
          },
          searchText: 'abc',
          successMessage: '',
          alertType: '',
          addRow: false,
          selectedRow: {
          },
          Update(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = true;
              this.isEditenabled = true;
              this.selectedRow = selectedRow;
              this.selectedRow.ModifiedDate = this.currentDate;
              this.bpmComponentService.editCapitalExpensesItem(this.selectedRow as CapitalExpenses).subscribe(
                  (data: CapitalExpenses) => {
                      console.log(data);

                      setTimeout(() => {
                          this.listAllData();
                          this.alertType = 'success';
                          this.successMessage = 'Item Updated Successfully.';
                          this.hideErrorMessage();
                      }, 1000);

                  }, (err: any) => {
                      console.log(err);
                      this.alertType = 'danger';
                      this.successMessage = err.error.Message;
                      this.hideErrorMessage();
                  }
              );
          },
          enableEdit(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = false;
              this.isEditenabled = false;
              this.selectedRow = selectedRow;
              this.oldItem = Object.assign({}, selectedRow);
          },
          cancel(event: any) {
              this.addRow = false;
              this.count--;
          },
          reset(event: any, i: number) {
              this.list[i] = this.oldItem;
              this.isEdit[i] = true;
              this.isEditenabled = true;
          },
          AddNewRow(event: any) {
              this.addRow = true;
              this.count = (this.list) ? this.list.length + 1 : 1;
              this.addNewRow = {
                  //CSID: 1,
                  CapitalExpenses: "",
                  Year: this.currentYear, //this.currentYear,// need to do dynamic
                  SortOrder: 0,
                  CreatedBy: this.userName,//need to pass user login data
                  ModifiedBy: this.userName,
                  CreatedDate: this.currentDate,
                  ModifiedDate: this.currentDate,
                  BRID: this.bpmReferenceId
              }
          },
          // API for Add
          addNewItem(event: any) {
              //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
              this.bpmComponentService.addCapitalExpensesItem(this.addNewRow as CapitalExpenses).subscribe((data: CapitalExpenses) => {
                  console.log(data);
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Added Successfully.';
                      this.hideErrorMessage();
                  }, 1000);

                  this.addRow = false;
                  this.addNewRow.CapitalExpenses = "";
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          deleteItem(event: any, item: any) {
              this.bpmComponentService.deleteCapitalExpensesItem(item.CEID).subscribe((data: any) => {
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Deleted Successfully.';
                      this.hideErrorMessage();
                  }, 500);
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          }
      },
      recessionPlanning: {
          //titles: ['#', 'Data', 'VDC', 'Self Perform Work', 'Pre - fabrication', 'Recession planning','Actions'],
          initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
              this.currentDate = currentDate;
              this.bpmReferenceId = bpmReferenceId;
              this.bpmComponentService = bpmComponentService;
              this.userName = userName;
              this.currentYear = currentYear;
              this.addNewRow = {};
              this.list = [];
              this.updatedData = "";
              this.listAllData();
              this.isEditenabled = true;
          },
          listAllData() {
              this.bpmComponentService.RecessionPlanningAll(this.bpmReferenceId, this.currentYear).subscribe((data: RecessionPlanning[]) => {
                  this.list = data
                  
                  console.log(this.list);
                  this.isEdit = [];
                  this.count = data.length;
                  this.isEditenabled = true;
                  for (let i = 0; i < data.length; i++) {
                      this.isEdit[i] = true;
                  }
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          hideErrorMessage() {
              setTimeout(() => {
                  this.successMessage = null;
              }, 2000);
          },
          searchText: 'abc',
          successMessage: '',
          alertType: '',
          addRow: false,
          selectedRow: {
          },
          Update(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = true;
              this.isEditenabled = true;
              this.selectedRow = selectedRow;
              this.selectedRow.ModifiedDate = this.currentDate;
              this.bpmComponentService.editRecessionPlanningItem(this.selectedRow as RecessionPlanning).subscribe(
                  (data: RecessionPlanning) => {
                      console.log(data);

                      setTimeout(() => {
                          this.listAllData();
                          this.alertType = 'success';
                          this.successMessage = 'Item Updated Successfully.';
                          this.hideErrorMessage();
                      }, 1000);

                  }, (err: any) => {
                      console.log(err);
                      this.alertType = 'danger';
                      this.successMessage = err.error.Message;
                      this.hideErrorMessage();
                  }
              );
          },
          enableEdit(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = false;
              this.isEditenabled = false;
              this.selectedRow = selectedRow;
              this.oldItem = Object.assign({}, selectedRow);
          },
          cancel(event: any) {
              this.addRow = false;
              this.count--;
          },
          reset(event: any, i: number) {
              this.list[i] = this.oldItem;
              this.isEdit[i] = true;
              this.isEditenabled = true;
          },
          AddNewRow(event: any) {
              this.addRow = true;
              this.count = (this.list) ? this.list.length + 1 : 1;
              this.addNewRow = {
                  //CSID: 1,
                  Category: "",
                  Description: "",
                  Year: this.currentYear, //this.currentYear,// need to do dynamic
                  SortOrder: 0,
                  CreatedBy: this.userName,//need to pass user login data
                  ModifiedBy: this.userName,
                  CreatedDate: this.currentDate,
                  ModifiedDate: this.currentDate,
                  BRID: this.bpmReferenceId
              }
          },
          // API for Add
          addNewItem(event: any) {
              //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
              this.bpmComponentService.addRecessionPlanningItem(this.addNewRow as RecessionPlanning).subscribe((data: RecessionPlanning) => {
                  console.log(data);
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Added Successfully.';
                      this.hideErrorMessage();
                  }, 1000);

                  this.addRow = false;
                  //this.addNewRow.FocusAreas = "";
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          deleteItem(event: any, item: any) {
              this.bpmComponentService.deleteRecessionPlanningItem(item.CSRPID).subscribe((data: any) => {
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Deleted Successfully.';
                      this.hideErrorMessage();
                  }, 500);
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          }
      },
      integratedTopFive: {
          titles: ['#', 'Strategic Actions'],
          initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
              this.currentDate = currentDate;
              this.bpmReferenceId = bpmReferenceId;
              this.bpmComponentService = bpmComponentService;
              this.userName = userName;
              this.currentYear = currentYear;
              this.addNewRow = {};
              this.list = [];
              this.updatedData = "";
              this.listAllData();
              this.isEditenabled = true;
          },
          listAllData() {
              this.bpmComponentService.IntegratedTopFiveAll(this.bpmReferenceId, this.currentYear).subscribe((data: IntegratedTopFive[]) => {
                  this.list = data;
                  if (this.list.length !== 0) {
                      this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                  }
                  console.log(this.list);
                  this.isEdit = [];
                  this.count = data.length;
                  this.isEditenabled = true;
                  for (let i = 0; i < data.length; i++) {
                      this.isEdit[i] = true;
                  }
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          hideErrorMessage() {
              setTimeout(() => {
                  this.successMessage = null;
              }, 2000);
          },
          searchText: 'abc',
          successMessage: '',
          alertType: '',
          addRow: false,
          selectedRow: {
          },
          Update(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = true;
              this.isEditenabled = true;
              this.selectedRow = selectedRow;
              this.selectedRow.ModifiedDate = this.currentDate;
              this.bpmComponentService.editIntegratedTopFiveItem(this.selectedRow as IntegratedTopFive).subscribe(
                  (data: IntegratedTopFive) => {
                      console.log(data);

                      setTimeout(() => {
                          this.listAllData();
                          this.alertType = 'success';
                          this.successMessage = 'Item Updated Successfully.';
                          this.hideErrorMessage();
                      }, 1000);

                  }, (err: any) => {
                      console.log(err);
                      this.alertType = 'danger';
                      this.successMessage = err.error.Message;
                      this.hideErrorMessage();
                  }
              );
          },
          enableEdit(event: any, selectedRow: any, i: number) {
              this.isEdit[i] = false;
              this.isEditenabled = false;
              this.selectedRow = selectedRow;
              this.oldItem = Object.assign({}, selectedRow);
          },
          cancel(event: any) {
              this.addRow = false;
              this.count--;
          },
          reset(event: any, i: number) {
              this.list[i] = this.oldItem;
              this.isEdit[i] = true;
              this.isEditenabled = true;
          },
          AddNewRow(event: any) {
              this.addRow = true;
              this.count = (this.list) ? this.list.length + 1 : 1;
              this.addNewRow = {
                  //CSID: 1,
                  StrategicActions: "",
                  Year: this.currentYear, //this.currentYear,// need to do dynamic
                  SortOrder: 0,
                  CreatedBy: this.userName,//need to pass user login data
                  ModifiedBy: this.userName,
                  CreatedDate: this.currentDate,
                  ModifiedDate: this.currentDate,
                  BRID: this.bpmReferenceId
              }
          },
          // API for Add
          addNewItem(event: any) {
              //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
              this.bpmComponentService.addIntegratedTopFiveItem(this.addNewRow as IntegratedTopFive).subscribe((data: IntegratedTopFive) => {
                  console.log(data);
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Added Successfully.';
                      this.hideErrorMessage();
                  }, 1000);

                  this.addRow = false;
                  this.addNewRow.StrategicActions = "";
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          },
          deleteItem(event: any, item: any) {
              this.bpmComponentService.deleteFocusAreasItem(item.ITFID).subscribe((data: any) => {
                  setTimeout(() => {
                      this.listAllData();
                      this.alertType = 'success';
                      this.successMessage = 'Item Deleted Successfully.';
                      this.hideErrorMessage();
                  }, 500);
              }, (err: any) => {
                  console.log(err);
                  this.alertType = 'danger';
                  this.successMessage = err.error.Message;
                  this.hideErrorMessage();
              });
          }
      }


  }


  public DownloadFile() {
      this.spinner.show();
      //const pdfDownload = document.createElement("a");
      //document.body.appendChild(pdfDownload);
      //const fileName = this.currentYear + "TopPrioritySheet.pdf";
      //this.bpmComponentService.exportToPDF(this.bpmReferenceId, this.currentYear).subscribe(result => {
      //    this.spinner.hide();
      //    var fileBlob = new Blob([result], { type: 'application/pdf' });
      //    if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
      //        window.navigator.msSaveBlob(fileBlob, fileName);
      //    }
      //    else {
      //        var url = window.URL.createObjectURL(fileBlob);
      //        pdfDownload.href = url;
      //        pdfDownload.download = fileName;
      //        pdfDownload.click();
      //    }

      //}, (err: any) => {
      //        this.spinner.hide();
      //    console.log(err);
      //    //this.alertType = 'danger';
      //    //this.successMessage = err.error.Message;
      //    //this.hideErrorMessage();
      //}); 
      var fileName = this.currentYear + "TopPrioritySheet.xlsx";
      var excelDownload = document.createElement("a");
      document.body.appendChild(excelDownload);
      this.spinner.show();
      this.bpmComponentService.exportExcel(this.bpmReferenceId).subscribe((result: any) => {
          this.spinner.hide();
          //console.log(result);
          //var fileBlob = new Blob([result], { type: 'application/xlsx' });
          var fileBlob = new Blob([result], { type: "vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
          if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
              window.navigator.msSaveBlob(fileBlob, fileName);
          }
          else {
              var url = window.URL.createObjectURL(fileBlob);
              excelDownload.href = url;
              excelDownload.download = fileName;
              excelDownload.click();
              window.URL.revokeObjectURL(url);
          }
      },
          (err: any) => {
              //console.log(err);
              this.spinner.hide();

          });

  }

  toggle(acc: NgbAccordion) {
      let text = "Expand All";
      if (acc.activeIds.length) {
          acc.activeIds = [];
          text = "Expand All";
      }
      else {
          acc.activeIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map(i => `ngb-panel-${i}`);
          text = "Collapse All";
      }

      document.getElementsByClassName('expand-collapse-btn')[0].innerHTML = text;
  }

}

 
