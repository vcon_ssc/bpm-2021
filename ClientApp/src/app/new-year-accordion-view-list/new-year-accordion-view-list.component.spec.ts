import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewYearAccordionViewListComponent } from './new-year-accordion-view-list.component';

describe('NewYearAccordionViewListComponent', () => {
  let component: NewYearAccordionViewListComponent;
  let fixture: ComponentFixture<NewYearAccordionViewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewYearAccordionViewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewYearAccordionViewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
