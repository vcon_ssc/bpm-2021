import { TestBed } from '@angular/core/testing';

import { BpmComponentService } from './bpm-component.service';

describe('BpmComponentService', () => {
  let service: BpmComponentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BpmComponentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
