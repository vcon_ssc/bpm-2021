import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private messageSource = new BehaviorSubject('Select Year');
  currentMessage = this.messageSource.asObservable();
  private messageSource1 = new BehaviorSubject(true);
  currentMessage1 = this.messageSource1.asObservable();
  constructor() { }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  changeTitle(message1: boolean) {
    this.messageSource1.next(message1);
  }

}
