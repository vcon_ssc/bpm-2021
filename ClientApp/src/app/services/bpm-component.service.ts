import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Narrative } from '../models/Narrative';
import { BpmReference } from '../models/bpmreference.model';
import { CurrentSuccess } from '../models/currentSuccess.model';
import { CurrentOpportunity } from '../models/currentOpportunity.model';
import { Individual } from '../models/individual.model';
import { NextOpportunity } from '../models/nextOpportunity.model';
import { SSNeed } from '../models/SSNeed.model';
import { FocusAreas } from '../models/focusAreas.model';
import { LeaseModifications } from '../models/leaseModifications.model';
import { RecessionPlanning } from '../models/recessionPlanning.model';
import { IntegratedTopFive } from '../models/integratedTopFive';
import { CapitalExpenses } from '../models/capitalExpenses.model';

@Injectable({
  providedIn: 'root'
})
export class BpmComponentService {

  bpmReference: any[];

  constructor(private https: HttpClient) { }

  configUrl = 'https://localhost:44388/';
  // configUrl = 'https://dprbpm.azurewebsites.net/';
  // configUrl = 'https://dprbpm-dev.azurewebsites.net/';

  httpOptions = {
    headers: { 'Content-Type': 'application/pdf' },
    ResponseType: 'blob'
  };

  getRegionsForYear(year: any) {
    return this.https.get(this.configUrl + 'api/Region/GetRegionNameByYear/' + year

    ).pipe(

    );
  }

  getRegionNameByRespPer(year: any) {
    return this.https.get(this.configUrl + 'api/Region/GetRegionNameByRespPer/' + year

    ).pipe();
  }
  getBRIDByYearRegId(year: string, regionId: number) {
    return this.https.get(this.configUrl + 'api/BPMReference/GetBRIDByYearRegId/' + year + '/' + regionId
    ).pipe();
  }
  getGroupIdsByRegionId(year: string, regionId: number) {
    return this.https.get(this.configUrl + 'api/Region/GetGroupIDByYearRegId/' + year + '/' + regionId

    ).pipe();

  }

  getGroupByBRId(BRId: any, year: string) {
    return this.https.get(this.configUrl + 'api/BPMReference/getGroupByBRId/' + BRId + '/' + year

    ).pipe();

  }

  RequireGroupsAll(): any {
    return this.https.get(this.configUrl + 'api/PredefinedRequireGroup/Get');

  }

  SupportITFAll(): any {
    return this.https.get(this.configUrl + 'api/PredefinedSupportITF/Get');

  }

  narrativeAll(bpmId: any, year: any): any {
    return this.https.get(this.configUrl + 'api/Narrative/GetByBRId/' + bpmId + '/' + year);

  }

  editNarrative(narrative: Narrative) {
    return this.https.put(this.configUrl + 'api/Narrative/' + narrative.NID, narrative

    ).pipe();

  }
  deleteNarrative(Id: number) {
    return this.https.delete(this.configUrl + 'api/Narrative/' + Id, {}
    ).pipe();

  }
  addNarrative(narrative: Narrative): any {
    return this.https.post(this.configUrl + 'api/Narrative',
      narrative
    ).pipe(

    );
  }

  getBpmRefrencesByYear() {
    //throw new Error("Method not implemented.");

    return this.https.get(this.configUrl + 'api/BPMReference').subscribe(data => {
      console.log('data:', data);

    });
  }

  //exportToPdf(id: number): Observable<any> {
  //    let url = this.configUrl + "api/PDF/CreatePDFAsync/" + id;
  //    return this.https.get(url, { responseType: "blob" });

  //}

  //exportAllYearToPdf(id: number): Observable<any> {
  //    let url = this.configUrl + "api/PDF/CreateAllYearPDFAsync/" + id;
  //    return this.https.get(url, { responseType: "blob" });

  //}
  getBpmRefrencesAll(year: string): Observable<BpmReference[]> {

    return this.https.get<BpmReference[]>(this.configUrl + 'api/Region/GetBpmWithRegionByYear/' + year);
  }
  //Export specific Region to pdf
  exportToPDF(id: number, year: any): Observable<any> {
    if (year === '2020') {
      let url = this.configUrl + "api/Export2020/CreatePDFAsync/" + id;
      return this.https.get(url, { responseType: "blob" });
    }
    else {
      let url = this.configUrl + "api/Export/CreatePDFAsync/" + id;
      return this.https.get(url, { responseType: "blob" });
    }
  }

  exportExcel(id: number): Observable<any> {

    let url = this.configUrl + "api/Export/CreateExcel/" + id;
    return this.https.get(url, { responseType: "blob" });

  }

  //Export all reggion to pdf
  exportAllToPdf(year: any) {

    if (year === '2020') {
      let url = this.configUrl + "api/Export2020/CreateAllYearPDFAsync/" + year;
      return this.https.get(url, { responseType: "blob" });
    }
    else if (year === '2019') {
      let url = this.configUrl + "api/PDF/CreateAllYearPDFAsync/" + year;
      return this.https.get(url, { responseType: "blob" });
    }
    else {
      let url = this.configUrl + "api/Export/CreateAllYearPDFAsync/" + year;
      return this.https.get(url, { responseType: "blob" });
    }
  }

  //Export My regions to PDF
  exportMyTop5ToPDF(year: string): Observable<any> {
    let url = "";
    if (year === '2019') {
      url = this.configUrl + "api/PDF/CreateAllYearPDFAsync/" + year;
    }
    else if (year === '2020') {
      url = this.configUrl + "api/Export2020/CreateMyPDFAsync/" + year;
    }
    else {
      url = this.configUrl + "api/Export/CreateMyPDFAsync/" + year;

    }
    return this.https.get(url, { responseType: "blob" });
  }


  //Export all to excel
  exportToExcel(year: string) {
    let url = "";
    if (year === '2020') {
      url = this.configUrl + "api/Export2020/CreateExcelAsyncForYear/" + year;
    }
    else {
      url = this.configUrl + "api/Export/CreateExcelAsyncForYear/" + year;
    }
    return this.https.get(url, { responseType: "blob" });
  }


  exportToExcelGroupWise(year: string) {
    let url = "";
    url = this.configUrl + "api/Export/CreateExcelGroupWise/" + year;
    return this.https.get(url, { responseType: "blob" });
  }

  //getBpmRefrencesAllWithRegions(year: string): Observable<BpmReference[]> {

  //    return this.https.get<BpmReference[]>(this.configUrl + 'api/BPMReference/GetByYear/' + year, { responseType: 'json' });
  //}


  //exportAllNextYearToPdf1(year: string): Observable<any> {
  //    let url = this.configUrl + "api/Export/CreateAllYearPDFAsync/" + year;

  //    return this.https.get(url, { responseType: "blob" });

  //} 


  getBpmRefrencesByRespPer(year: string): Observable<BpmReference[]> {

    return this.https.get<BpmReference[]>(this.configUrl + 'api/BPMReference/GetByResPerYear/' + year);
  }

  addBpmRefrence(bpmReference: BpmReference) {
    return this.https.post(this.configUrl + 'api/BPMReference',
      bpmReference
    ).pipe();
  }

  editBpmRefrence(data: BpmReference)/*: Observable<BpmReference>*/ {
    return this.https.put(this.configUrl + 'api/BPMReference/' + data.BRID, data
    ).pipe();
  }
  BpmRefrencesDelete() {
    return this.https.delete(this.configUrl + 'api/BPMReference/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getBusinessUnitAll() {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/BusinessUnit');
    //debugger;

  }
  getBUById(id: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/BusinessUnit/' + id);
  }
  getBusinessUnitByRegion(regionId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/BusinessUnit/GetBUByRegion/' + regionId);

  }
  BusinessUnitAdd() {
    return this.https.post(this.configUrl + 'api/BusinessUnit',
      {
        "BUID": "5",
        "BUName": "SSC",
        "BRID": "56",
        "RegionID": "56"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  BusinessUnitEdit() {
    return this.https.post(this.configUrl + 'api/BusinessUnit/1',
      {
        "BUID": "5",
        "BUName": "SSC",
        "BRID": "56",
        "RegionID": "56"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  BusinessUnitDelete() {
    return this.https.delete(this.configUrl + 'api/BusinessUnit/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getCoreMarketAll() {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/CoreMarket');
  }
  getCMById(id: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/CoreMarket/' + id);
  }
  CoreMarketAdd() {
    return this.https.post(this.configUrl + 'api/CoreMarket',
      {
        "CMID": "5",
        "CMName": "LIFE",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  CoreMarketEdit() {
    return this.https.post(this.configUrl + 'api/CoreMarket/1',
      {
        "CMID": "5",
        "CMName": "LIFE",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  CoreMarketDelete() {
    return this.https.delete(this.configUrl + 'api/CoreMarket/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getAllCurrentChallenges(bpmId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/CurrentChallenge/GetByBRId/' + bpmId);
  }
  CurrentChallengesAdd() {
    return this.https.post(this.configUrl + 'api/CurrentChallenges',
      {
        "CCID": "2",
        "CurrentChallenges": "Hello",
        "Year": "2001",
        "SortOrder": "1",
        "CreatedBy": "Aman",
        "ModifiedBy": "2/2/2015",
        "Createddate": "1",
        "ModifiedDate": "2/8/2016",
        "BRID": "1"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  CurrentChallengesEdit() {
    return this.https.post(this.configUrl + 'api/CurrentChallenges/1',
      {
        "CCID": "2",
        "CurrentChallenges": "Hello",
        "Year": "2001",
        "SortOrder": "1",
        "CreatedBy": "Aman",
        "ModifiedBy": "2/2/2015",
        "Createddate": "1",
        "ModifiedDate": "2/8/2016",
        "BRID": "1"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  CurrentChallengesDelete() {
    return this.https.delete(this.configUrl + 'api/CurrentChallenges/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getAllCurrentOpportunities(bpmId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/CurrentOpportunity/GetByBRId/' + bpmId);
  }
  addCurrentOpportunities(currentOpportunity: CurrentOpportunity) {
    return this.https.post(this.configUrl + 'api/CurrentOpportunities', currentOpportunity).pipe();
  }
  editCurrentOpportunities(currentOpportunity: CurrentOpportunity) {
    return this.https.put(this.configUrl + 'api/CurrentOpportunities/' + currentOpportunity.COID, currentOpportunity).pipe();
  }
  deleteCurrentOpportunities(Id: number) {
    return this.https.delete(this.configUrl + 'api/CurrentOpportunities/' + Id, {}
    ).pipe();
  }
  CurrentSuccessAll(bpmId: number) {
    return this.https.get(this.configUrl + 'api/CurrentSuccess/GetByBRId/' + bpmId);
  }
  addCurrentSuccessItem(currentSuccess: CurrentSuccess) {
    return this.https.post(this.configUrl + 'api/CurrentSuccess',
      currentSuccess

    ).pipe(

    );

  }
  editCurrentSuccessItem(currentSuccess: CurrentSuccess) {
    return this.https.put(this.configUrl + 'api/CurrentSuccess/' + currentSuccess.CSID, currentSuccess

    ).pipe();

  }
  deleteCurrentSuccessItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/CurrentSuccess/' + Id, {}
    ).pipe();

  }
  getAllIndividuals(bpmId: number) {
    return this.https.get(this.configUrl + 'api/Individual/GetByBRId/' + bpmId);
  }
  addIndividualItem(individual: Individual) {
    return this.https.post(this.configUrl + 'api/Individual', individual).pipe();
  }
  editIndividualItem(individual: Individual) {
    return this.https.put(this.configUrl + 'api/Individual/' + individual.IndID, individual).pipe();
  }
  deleteIndividualItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/Individual/' + Id, {}
    ).pipe();
  }
  getAllMissedOpportunities(bpmId: number) {
    return this.https.get(this.configUrl + 'api/MissedOpportunity/GetByBRId/' + bpmId);
  }
  MissedOpportunityAdd() {
    return this.https.post(this.configUrl + 'api/ MissedOpportunity',
      {
        "MOID": "1",
        "MissedOpportunity": "ddD",
        "Year": "2051",
        "SortOrder": "5",
        "CreatedBy": "waddwa",
        "ModifiedBy": "sdvvaa",
        "CreatedDate": "2/8/95",
        "ModifiedDate": "5/89/98",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  MissedOpportunityEdit() {
    return this.https.post(this.configUrl + 'api/ MissedOpportunity/1',
      {
        "MOID": "1",
        "MissedOpportunity": "ddD",
        "Year": "2051",
        "SortOrder": "5",
        "CreatedBy": "waddwa",
        "ModifiedBy": "sdvvaa",
        "CreatedDate": "2/8/95",
        "ModifiedDate": "5/89/98",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  MissedOpportunityDelete() {
    return this.https.delete(this.configUrl + 'api/MissedOpportunity/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  NationalServiceAll() {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/NationalService');
  }
  getNSById(id: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/NationalService/' + id);
  }
  NationalServiceAdd() {
    return this.https.post(this.configUrl + 'api/NationalService',
      {
        "NSID": "2",
        "NSName": "sgesg",
        "BRID": "aefafgaa"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  NationalServiceEdit() {
    return this.https.post(this.configUrl + 'api/NationalService/1',
      {
        "NSID": "2",
        "NSName": "sgesg",
        "BRID": "aefafgaa"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  NationalServiceDelete() {
    return this.https.delete(this.configUrl + 'api/NationalService/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getAllNextBigIdeas(bpmId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/NextBigIdeas/GetByBRId/' + bpmId);
  }
  NextBigIdeasAdd() {
    return this.https.post(this.configUrl + 'api/NextBigIdeas',
      {
        "NBIID": "3",
        "NextBigIdeas": "gsgsg",
        "Year": "2014",
        "SortOrder": "1",
        "CreatedBy": "aegaa",
        "ModifiedBy": "vgsgse",
        "CreatedDate": "2/5/5",
        "ModifiedDate": "6/9/5",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  NextBigIdeasEdit() {
    return this.https.post(this.configUrl + 'api/NextBigIdeas/1',
      {
        "NBIID": "3",
        "NextBigIdeas": "gsgsg",
        "Year": "2014",
        "SortOrder": "1",
        "CreatedBy": "aegaa",
        "ModifiedBy": "vgsgse",
        "CreatedDate": "2/5/5",
        "ModifiedDate": "6/9/5",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  NextBigIdeasDelete() {
    return this.https.delete(this.configUrl + 'api/NextBigIdeas/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getAllNextChallenges(bpmId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/NextChallenges/GetByBRId/' + bpmId);
  }
  NextChallengesAdd() {
    return this.https.post(this.configUrl + 'api/NextChallenges',
      {
        "NCID": "5",
        "NextChallenges": "niuyihiuh",
        "Year": "5515",
        "SortOrder": "5",
        "CreatedBy": "esges",
        "ModifiedBy": "egshsehysysy",
        "CreatedDate": "5/8/9",
        "ModifiedDate": "5/9/96",
        "BRID": "6"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  NextChallengesEdit() {
    return this.https.post(this.configUrl + 'api/NextChallenges/1',
      {
        "NCID": "5",
        "NextChallenges": "niuyihiuh",
        "Year": "5515",
        "SortOrder": "5",
        "CreatedBy": "esges",
        "ModifiedBy": "egshsehysysy",
        "CreatedDate": "5/8/9",
        "ModifiedDate": "5/9/96",
        "BRID": "6"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  NextChallengesDelete() {
    return this.https.delete(this.configUrl + 'api/NextChallenges/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  // Next opportunity
  getAllNextOpportunities(bpmId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/NextOpportunities/GetByBRId/' + bpmId);
  }
  addNextOpportunityItem(nextOpportunity: NextOpportunity) {
    return this.https.post(this.configUrl + 'api/NextOpportunities',
      nextOpportunity
    ).pipe();
  }
  editNextOpportunityItem(nextOpportunity: NextOpportunity) {
    return this.https.put(this.configUrl + 'api/NextOpportunities/' + nextOpportunity.NOID, nextOpportunity
    ).pipe();

  }
  deleteNextOpportunityItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/NextOpportunities/' + Id, {}
    ).pipe();
  }


  RegionAll(year: string) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/Region/GetRegionNameByYear/' + year);
  }

  RegionByResPer(year: string) {
    return this.https.get(this.configUrl + 'api/Region/GetRegionNameByRespPer/' + year);
  }
  getRegionById(id: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/Region/' + id);
  }
  RegionAdd() {
    return this.https.post(this.configUrl + 'api/Region',
      {
        "RegionID": "2",
        "RegionName": "wfwaafwa",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  RegionEdit() {
    return this.https.post(this.configUrl + 'api/Region/1',
      {
        "RegionID": "2",
        "RegionName": "wfwaafwa",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  RegionDelete() {
    return this.https.delete(this.configUrl + 'api/Region/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getAllStrategicNeeds(bpmId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/SSNeeds/GetByBRId/' + bpmId);
  }
  addStrategicNeedsItem(ssneed: SSNeed) {
    return this.https.post(this.configUrl + 'api/SSNeeds', ssneed
    ).pipe();
  }
  editStrategicNeedsItem(ssneed: SSNeed) {
    return this.https.put(this.configUrl + 'api/SSNeeds/' + ssneed.SSNID, ssneed).pipe();
  }
  deleteStrategicNeedsItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/SSNeeds/' + Id, {}
    ).pipe();
  }
  StrategicPartnersAll() {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/StrategicPartners');
  }
  getSPById(id: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/StrategicPartners/' + id);
  }
  StrategicPartnersAdd() {
    return this.https.post(this.configUrl + 'api/StrategicPartners',
      {
        "SPID": "6",
        "SPName": "dsaafa",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  StrategicPartnersEdit() {
    return this.https.post(this.configUrl + 'api/StrategicPartners/1',
      {
        "SPID": "6",
        "SPName": "dsaafa",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  StrategicPartnersDelete() {
    return this.https.delete(this.configUrl + 'api/StrategicPartners/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  getAllUtilizedOpportunities(bpmId: number) {
    //throw new Error("Method not implemented.");
    return this.https.get(this.configUrl + 'api/UtilizedOpportunity/GetByBRId/' + bpmId);
  }
  UtilizedOpportunityAdd() {
    return this.https.post(this.configUrl + 'api/UtilizedOpportunity',
      {
        "UOID": "6",
        "UtilizedOpportunity": "eagsegsegeseghs",
        "Year": "2015",
        "SortOrder": "5",
        "CreatedBy": "awwafwa",
        "ModifiedBy": "wafarfa",
        "CreatedDate": "5/9/8",
        "ModifiedDate": "5/9/9",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  UtilizedOpportunityEdit() {
    return this.https.post(this.configUrl + 'api/UtilizedOpportunity/1',
      {
        "UOID": "6",
        "UtilizedOpportunity": "eagsegsegeseghs",
        "Year": "2015",
        "SortOrder": "5",
        "CreatedBy": "awwafwa",
        "ModifiedBy": "wafarfa",
        "CreatedDate": "5/9/8",
        "ModifiedDate": "5/9/9",
        "BRID": "2"
      }).subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }
  UtilizedOpportunityDelete() {
    return this.https.delete(this.configUrl + 'api/UtilizedOpportunity/1')
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        });
  }

  FocusAreasAll(bpmId: number, year: string) {
    return this.https.get(this.configUrl + 'api/FocusAreas/GetByBRId/' + bpmId + "/" + year);
  }

  addFocusAreasItem(focusArea: FocusAreas) {
    return this.https.post(this.configUrl + 'api/FocusAreas',
      focusArea

    ).pipe(

    );

  }
  editFocusAreasItem(focusArea: FocusAreas) {
    return this.https.put(this.configUrl + 'api/FocusAreas/' + focusArea.FAID, focusArea

    ).pipe();

  }
  deleteFocusAreasItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/FocusAreas/' + Id, {}
    ).pipe();

  }

  LeaseModificationsAll(bpmId: number, year: string) {
    return this.https.get(this.configUrl + 'api/LeaseModification/GetByBRId/' + bpmId + '/' + year);
  }
  addLeaseModificationsItem(leaseModification: LeaseModifications) {
    return this.https.post(this.configUrl + 'api/LeaseModification',
      leaseModification

    ).pipe(

    );

  }
  editLeaseModificationsItem(leaseModification: LeaseModifications) {
    return this.https.put(this.configUrl + 'api/LeaseModification/' + leaseModification.LMID, leaseModification

    ).pipe();

  }
  deleteLeaseModificationsItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/LeaseModification/' + Id, {}
    ).pipe();

  }

  CapitalExpensesAll(bpmId: number, year: string) {
    return this.https.get(this.configUrl + 'api/CapitalExpense/GetByBRId/' + bpmId + '/' + year);
  }

  addCapitalExpensesItem(capitalExpense: CapitalExpenses) {
    return this.https.post(this.configUrl + 'api/CapitalExpense',
      capitalExpense
    ).pipe();

  }
  editCapitalExpensesItem(capitalExpense: CapitalExpenses) {
    return this.https.put(this.configUrl + 'api/CapitalExpense/' + capitalExpense.CEID, capitalExpense
    ).pipe();
  }
  deleteCapitalExpensesItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/CapitalExpense/' + Id, {}
    ).pipe();

  }
  RecessionPlanningAll(bpmId: number, year: string) {
    return this.https.get(this.configUrl + 'api/RecessionPlanning/GetByBRId/' + bpmId + '/' + year);
  }
  addRecessionPlanningItem(recessionPlanning: RecessionPlanning) {
    return this.https.post(this.configUrl + 'api/RecessionPlanning',
      recessionPlanning
    ).pipe();

  }
  editRecessionPlanningItem(recessionPlanning: RecessionPlanning) {
    return this.https.put(this.configUrl + 'api/RecessionPlanning/' + recessionPlanning.CSRPID, recessionPlanning
    ).pipe();
  }
  deleteRecessionPlanningItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/RecessionPlanning/' + Id, {}
    ).pipe();

  }

  IntegratedTopFiveAll(bpmId: number, year: string) {
    return this.https.get(this.configUrl + 'api/IntegratedTopFive/GetByBRId/' + bpmId + '/' + year);
  }
  addIntegratedTopFiveItem(integratedTopFive: IntegratedTopFive) {
    return this.https.post(this.configUrl + 'api/IntegratedTopFive',
      integratedTopFive
    ).pipe();

  }
  editIntegratedTopFiveItem(integratedTopFive: IntegratedTopFive) {
    return this.https.put(this.configUrl + 'api/IntegratedTopFive/' + integratedTopFive.ITFID, integratedTopFive
    ).pipe();
  }
  deleteIntegratedTopFiveItem(Id: number) {
    return this.https.delete(this.configUrl + 'api/IntegratedTopFive/' + Id, {}
    ).pipe();

  }


}
