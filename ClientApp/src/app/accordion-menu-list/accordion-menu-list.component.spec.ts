import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionMenuListComponent } from './accordion-menu-list.component';

describe('AccordionMenuListComponent', () => {
  let component: AccordionMenuListComponent;
  let fixture: ComponentFixture<AccordionMenuListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccordionMenuListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionMenuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
