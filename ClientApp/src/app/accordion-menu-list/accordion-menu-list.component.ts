import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CurrentChallenge } from '../models/currentChallenge.model';
import { CurrentOpportunity } from '../models/currentOpportunity.model';
import { CurrentSuccess } from '../models/currentSuccess.model';
import { Individual } from '../models/individual.model';
import { NextOpportunity } from '../models/nextOpportunity.model';
import { SSNeed } from '../models/SSNeed.model';
import { BpmComponentService } from '../services/bpm-component.service';
import { DataService } from '../services/data.service';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import {NgbAccordion} from '@ng-bootstrap/ng-bootstrap/accordion/accordion';
@Component({
  selector: 'app-accordion-menu-list',
  templateUrl: './accordion-menu-list.component.html',
  styleUrls: ['./accordion-menu-list.component.css']
})
export class AccordionMenuListComponent implements OnInit {

  showActions: boolean;
  RegionName: string;
  prevYear: string;
  currentDate: string;
  year: string[];
  sortOrders: string[] = ["Red", "Green", "Yellow"];
  selectedSortOrder: string = "Get Work";
  angForm: FormGroup;

  ChangeSortOrder(newSortOrder: string) {
    this.selectedSortOrder = newSortOrder;
  }
  constructor(config: NgbDropdownConfig, private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute, private data: DataService, private bpmComponentService: BpmComponentService) {
    config.placement = 'top-left';
    //config.autoClose = false;
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      succname: ['', Validators.required],
      succnameadd: ['', Validators.required],
      oppToFnname: ['', Validators.required],
      oppToFnnameadd: ['', Validators.required],
      oppname: ['', Validators.required],
      oppnameadd: ['', Validators.required],
      indname: ['', Validators.required],
      indnameadd: ['', Validators.required],
      ssnname: ['', Validators.required],
      ssnnameadd: ['', Validators.required],
      quantity: ['', Validators.required],
      currentRole: ['', Validators.required],
      nextOpp: ['', Validators.required],
      nextOppadd: ['', Validators.required],
      currentRoleadd: ['', Validators.required],
      quantityadd: ['', Validators.required]
    });
  }
  //red: string;
  //green: string;
  //yellow: string;
  title: string;
  // @Input() nextYear: string;
  @Input() currentYear: string;
  CurrentSuccess: CurrentSuccess[];
  CurrentChallenge: CurrentChallenge[];

  bpmReferenceId: number;
  activeIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(i => `ngb-panel-${i}`);

  toggle(acc: NgbAccordion) {
    let text = "Expand All";
    if (acc.activeIds.length) {
      acc.activeIds = [];
      text = "Expand All";
    }
    else {
      acc.activeIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(i => `ngb-panel-${i}`);
      text = "Collapse All";
    }

    document.getElementsByClassName('expand-collapse-btn')[0].innerHTML = text;
  }

  userName = "" // dynamic logged in user name

  //method for pdf


  public DownloadFile() {
    var pdfDownload = document.createElement("a");
    document.body.appendChild(pdfDownload);
    var fileName = "2019Top5Sheet.pdf";
    this.bpmComponentService.exportToPDF(this.bpmReferenceId, this.currentYear).subscribe(result => {
      var fileBlob = new Blob([result], { type: 'application/pdf' });
      if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
        window.navigator.msSaveBlob(fileBlob, fileName);
      }
      else {
        var url = window.URL.createObjectURL(result);
        pdfDownload.href = url;
        pdfDownload.download = fileName;
        pdfDownload.click();
      }

    });
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.bpmReferenceId = params['id'];
    });
    if (this.currentYear == "Select Year" || !this.currentYear) {
      this.currentYear = localStorage.getItem('selectedYear');
      this.data.changeMessage(this.currentYear);
    }

    this.prevYear = "" + (parseInt(this.currentYear) - 1);
    this.RegionName = localStorage.getItem('RegionName');
    this.getCurrentDate();
    //console.log(this.bpmReferenceId);
    this.tableList.success.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.prevYear);
    // this.tableList.success.listAllData();
    //this.tableList.challenges.listAllData(this.bpmComponentService, this.bpmReferenceId);
    this.tableList.opportunitiestoFinish.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.prevYear);
    this.tableList.opportunities.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
    // this.tableList.challengesToOvercome.listAllData(this.bpmComponentService);
    // this.tableList.bigIdeas.listAllData(this.bpmComponentService);
    this.tableList.individualReady.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
    this.tableList.strategicneeds.initializeData(this.bpmComponentService, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
    //this.tableList.MissedOpportunities.listAllData(this.bpmComponentService);
    // this.tableList.organisationalFramework.listAllData(this.bpmComponentService);
    if (localStorage.getItem('showActions') == 'yes') {
      this.showActions = false;
    }

  }
  getCurrentDate() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();
    this.currentDate = dd + '/' + mm + '/' + yyyy;
    //this.currentDate = (dd < 10) ? '0' + dd as string : dd + '/' + (mm < 10) ? '0' + mm as string : mm + '/' + yyyy;
    //document.write(today1);
  }
  tableList = {
    success: {
      titles: ['#', 'Success', 'Actions'],
      initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
        this.currentDate = currentDate;
        this.bpmReferenceId = bpmReferenceId;
        this.bpmComponentService = bpmComponentService;
        this.userName = userName;
        this.currentYear = currentYear;
        this.addNewRow = {};
        this.list = [];
        this.listAllData();
        this.isEditenabled = true;
      },
      listAllData() {
        this.bpmComponentService.CurrentSuccessAll(this.bpmReferenceId).subscribe((data: CurrentSuccess[]) => {
          this.list = data;
          console.log(this.list);
          this.isEdit = [];
          this.count = data.length;
          this.isEditenabled = true;
          for (let i = 0; i < data.length; i++) {
            this.isEdit[i] = true;
          }
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      hideErrorMessage() {
        setTimeout(() => {
          this.successMessage = null;
        }, 2000);
      },
      searchText: 'abc',
      successMessage: '',
      alertType: '',
      addRow: false,
      selectedRow: {
      },
      Update(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = true;
        this.isEditenabled = true;
        this.selectedRow = selectedRow;
        this.selectedRow.ModifiedDate = this.currentDate;
        this.bpmComponentService.editCurrentSuccessItem(this.selectedRow as CurrentSuccess).subscribe(
          (data: CurrentSuccess) => {
            console.log(data);

            setTimeout(() => {
              this.listAllData();
              this.alertType = 'success';
              this.successMessage = 'Item Updated Successfully.';
              this.hideErrorMessage();
            }, 1000);

          }, (err: any) => {
            console.log(err);
            this.alertType = 'danger';
            this.successMessage = err.error.Message;
            this.hideErrorMessage();
          }
        );
      },
      enableEdit(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = false;
        this.isEditenabled = false;
        this.selectedRow = selectedRow;
        this.oldItem = Object.assign({}, selectedRow);
      },
      cancel(event: any) {
        this.addRow = false;
        this.count--;
      },
      reset(event: any, i: number) {
        this.list[i] = this.oldItem;
        this.isEdit[i] = true;
        this.isEditenabled = true;
      },
      AddNewRow(event: any) {
        this.addRow = true;
        this.count = (this.list) ? this.list.length + 1 : 1;
        this.addNewRow = {
          //CSID: 1,
          CurrentSuccess1: "",
          Year: this.currentYear, //this.currentYear,// need to do dynamic
          SortOrder: 0,
          CreatedBy: this.userName,//need to pass user login data
          ModifiedBy: this.userName,
          CreatedDate: this.currentDate,
          ModifiedDate: this.currentDate,
          BRID: this.bpmReferenceId
        }
      },
      // API for Add
      addNewItem(event: any) {
        //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
        this.bpmComponentService.addCurrentSuccessItem(this.addNewRow as CurrentSuccess).subscribe((data: CurrentSuccess) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Added Successfully.';
            this.hideErrorMessage();
          }, 1000);

          this.addRow = false;
          this.addNewRow.CurrentSuccess1 = "";
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      deleteItem(event: any, item: any) {
        this.bpmComponentService.deleteCurrentSuccessItem(item.CSID).subscribe((data: any) => {
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Deleted Successfully.';
            this.hideErrorMessage();
          }, 500);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      }
    },
    opportunitiestoFinish: {
      titles: ['#', 'Opportunities To Finish Strong', 'Actions'],
      initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
        this.currentDate = currentDate;
        this.bpmReferenceId = bpmReferenceId;
        this.bpmComponentService = bpmComponentService;
        this.userName = userName;
        this.currentYear = currentYear;
        this.addNewRow = {};
        this.list = [];
        this.isEditenabled = true;
        this.listAllData();
      },
      listAllData() {
        this.bpmComponentService.getAllNextOpportunities(this.bpmReferenceId).subscribe((data: NextOpportunity[]) => {
          this.list = data;
          console.log(this.list);
          this.isEdit = [];
          this.count = data.length;
          this.isEditenabled = true;
          for (let i = 0; i < this.list.length; i++) {
            this.isEdit[i] = true;
          }
        }, (err: any) => {
          console.log(err); console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      hideErrorMessage() {
        setTimeout(() => {
          this.successMessage = null;
        }, 2000);
      },
      searchText: 'abc',
      successMessage: '',
      alertType: '',
      addRow: false,
      selectedRow: {
      },
      Update(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = true;
        this.isEditenabled = true;
        this.selectedRow = selectedRow;
        this.selectedRow.ModifiedDate = this.currentDate;
        this.bpmComponentService.editNextOpportunityItem(this.selectedRow as NextOpportunity).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Updated Successfully.';
            this.hideErrorMessage();
          }, 1000);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      enableEdit(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = false;
        this.isEditenabled = false;
        this.selectedRow = selectedRow;
        this.oldItem = Object.assign({}, selectedRow);
      },
      cancel(event: any) {
        this.addRow = false;
        this.count--;
      },
      reset(event: any, i: number) {
        this.list[i] = this.oldItem;
        this.isEdit[i] = true;
        this.isEditenabled = true;
      },
      AddNewRow(event: any) {
        this.addRow = true;
        this.count = (this.list) ? this.list.length + 1 : 1;
        this.addNewRow = {
          NextOpportunities: "",
          Year: this.currentYear, //this.currentYear,// need to do dynamic
          SortOrder: 0,
          CreatedBy: this.userName,//need to pass user login data
          ModifiedBy: this.userName,
          CreatedDate: this.currentDate,
          ModifiedDate: this.currentDate,
          BRID: this.bpmReferenceId
        };
      },
      // API for Add
      addNewItem(event: any) {
        //  this.addNewRow.NOID = this.count; // temp auto increment it should be implemented at back end
        this.bpmComponentService.addNextOpportunityItem(this.addNewRow as NextOpportunity).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Added Successfully.';
            this.hideErrorMessage();
          }, 1000);

          this.addRow = false;
          this.addNewRow.NextOpportunities = "";
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      deleteItem(event: any, item: any) {
        this.bpmComponentService.deleteNextOpportunityItem(item.NOID).subscribe((data: any) => {
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Deleted Successfully.';
            this.hideErrorMessage();
          }, 500);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      }
    },
    opportunities: {
      titles: ['#', 'Opportunities', 'Actions'],
      // list: [{ no: 1, name: '2018 challenges' }],
      initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
        this.currentDate = currentDate;
        this.bpmReferenceId = bpmReferenceId;
        this.bpmComponentService = bpmComponentService;
        this.userName = userName;
        this.currentYear = currentYear
        this.addNewRow = {};
        this.list = [];
        this.listAllData();
        this.isEditenabled = true;
      },
      listAllData() {
        this.bpmComponentService.getAllCurrentOpportunities(this.bpmReferenceId).subscribe((data: CurrentOpportunity[]) => {
          this.list = data;
          console.log(this.list);
          this.isEdit = [];
          this.count = data.length;
          this.isEditenabled = true;
          for (let i = 0; i < this.list.length; i++) {
            this.isEdit[i] = true;
          }
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      hideErrorMessage() {
        setTimeout(() => {
          this.successMessage = null;
        }, 2000);
      },
      searchText: 'abc',
      successMessage: '',
      alertType: '',
      addRow: false,

      selectedRow: {
      },

      Update(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = true;
        this.isEditenabled = true;
        this.selectedRow = selectedRow;
        this.selectedRow.ModifiedDate = this.currentDate;
        this.bpmComponentService.editCurrentOpportunities(this.selectedRow as CurrentOpportunity).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Updated Successfully.';
            this.hideErrorMessage();
          }, 1000);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      enableEdit(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = false;
        this.isEditenabled = false;
        this.selectedRow = selectedRow;
        this.oldItem = Object.assign({}, selectedRow);
      },
      cancel(event: any) {
        this.addRow = false;
        this.count--;
      },
      reset(event: any, i: number) {
        this.list[i] = this.oldItem;
        this.isEdit[i] = true;
        this.isEditenabled = true;
      },
      AddNewRow(event: any) {
        this.addRow = true;
        this.count = (this.list) ? this.list.length + 1 : 1;
        this.addNewRow = {
          CurrentOpportunities: "",
          Year: this.currentYear,
          SortOrder: 0,
          CreatedBy: this.userName,//need�to�pass�user�login�data
          ModifiedBy: this.userName,
          CreatedDate: this.currentDate,
          ModifiedDate: this.currentDate,
          BRID: this.bpmReferenceId
        };
      },
      //�API�for�Add
      addNewItem(event: any) {
        //  this.addNewRow.COID = this.count;
        this.bpmComponentService.addCurrentOpportunities(this.addNewRow as CurrentOpportunity).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Added Successfully.';
            this.hideErrorMessage();
          }, 1000);
          this.addRow = false;
          this.addNewRow.CurrentOpportunities = "";

        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });

      },
      deleteItem(event: any, item: any) {
        this.bpmComponentService.deleteCurrentOpportunities(item.COID).subscribe((data: any) => {
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Deleted Successfully.';
            this.hideErrorMessage();
          }, 500);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      }
    },
    individualReady: {
      initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
        this.currentDate = currentDate;
        this.bpmReferenceId = bpmReferenceId;
        this.bpmComponentService = bpmComponentService;
        this.userName = userName;
        this.currentYear = currentYear;
        this.addNewRow = {};
        this.list = [];
        this.listAllData();
        this.isEditenabled = true;
      },
      listAllData() {

        this.bpmComponentService.getAllIndividuals(this.bpmReferenceId).subscribe((data: Individual[]) => {
          this.list = data;
          console.log(this.list);
          this.isEdit = [];
          this.count = data.length;
          this.isEditenabled = true;
          for (let i = 0; i < this.list.length; i++) {
            this.isEdit[i] = true;
          }
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      hideErrorMessage() {
        setTimeout(() => {
          this.successMessage = null;
        }, 2000);
      },
      searchText: 'abc',
      successMessage: '',
      alertType: '',
      addRow: false,
      selectedRow: {
      },
      Update(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = true;
        this.isEditenabled = true;
        this.selectedRow = selectedRow;
        this.selectedRow.ModifiedDate = this.currentDate;
        this.bpmComponentService.editIndividualItem(this.selectedRow as Individual).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Updated Successfully.';
            this.hideErrorMessage();
          }, 1000);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      enableEdit(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = false;
        this.isEditenabled = false;
        this.selectedRow = selectedRow;
        this.oldItem = Object.assign({}, selectedRow);
      },
      cancel(event: any) {
        this.addRow = false;
        this.count--;
      },
      reset(event: any, i: number) {
        this.list[i] = this.oldItem;
        this.isEdit[i] = true;
        this.isEditenabled = true;
      },
      AddNewRow(event: any) {
        this.addRow = true;
        this.count = (this.list) ? this.list.length + 1 : 1;
        this.addNewRow = {
          IndividualName: "",
          //Year: "2018",�//this.currentYear,�//this.currentYear,//�need�to�do�dynamic
          CurrentRole: "",
          NextRole: "",
          IsAvailable: "",
          Sales: "Red",
          Ops: "Red",
          People: "Red",
          SortOrder: 0,
          CreatedBy: this.userName,//need�to�pass�user�login�data
          ModifiedBy: this.userName,
          CreatedDate: this.currentDate,
          ModifiedDate: this.currentDate,
          BRID: this.bpmReferenceId
        };
      },
      addNewItem(event: any) {
        // this.addNewRow.IndID = this.count;
        this.bpmComponentService.addIndividualItem(this.addNewRow as Individual).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Added Successfully';
            this.hideErrorMessage();
          }, 2000);
          this.addRow = false;
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });

      },
      deleteItem(event: any, item: any) {
        this.bpmComponentService.deleteIndividualItem(item.IndID).subscribe((data: any) => {
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Deleted Successfully.';
            this.hideErrorMessage();
          }, 500);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      }
    },
    strategicneeds: {
      titles: ['#', 'Strategic Staffing Needs', 'Positions', 'Actions'],
      initializeData(bpmComponentService: BpmComponentService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
        this.currentDate = currentDate;
        this.bpmReferenceId = bpmReferenceId;
        this.bpmComponentService = bpmComponentService;
        this.userName = userName;
        this.currentYear = currentYear;
        this.addNewRow = {};
        this.list = [];
        this.listAllData();
        this.isEditenabled = true;
      },
      listAllData() {

        this.bpmComponentService.getAllStrategicNeeds(this.bpmReferenceId).subscribe((data: SSNeed[]) => {
          this.list = data;
          console.log(this.list);
          this.isEdit = [];
          this.count = data.length;
          this.isEditenabled = true;
          for (let i = 0; i < this.list.length; i++) {
            this.isEdit[i] = true;
          }
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      hideErrorMessage() {
        setTimeout(() => {
          this.successMessage = null;
        }, 2000);
      },
      searchText: 'abc',
      successMessage: '',
      alertType: '',
      addRow: false,

      selectedRow: {
      },
      Update(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = true;
        this.isEditenabled = true;
        this.selectedRow = selectedRow;
        this.selectedRow.ModifiedDate = this.currentDate;
        this.bpmComponentService.editStrategicNeedsItem(this.selectedRow as SSNeed).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Updated Successfully.';
            this.hideErrorMessage();
          }, 1000);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      },
      enableEdit(event: any, selectedRow: any, i: number) {
        this.isEdit[i] = false;
        this.isEditenabled = false;
        this.selectedRow = selectedRow;
        this.oldItem = Object.assign({}, selectedRow);
      },
      cancel(event: any) {
        this.addRow = false;
        this.count--;
      },
      reset(event: any, i: number) {
        this.list[i] = this.oldItem;
        this.isEdit[i] = true;
        this.isEditenabled = true;
      },
      AddNewRow(event: any) {
        this.addRow = true;
        this.count = (this.list) ? this.list.length + 1 : 1;
        this.addNewRow = {
          Role: "",
          Quantity: 0,
          Year: this.currentYear,
          SortOrder: 0,
          CreatedBy: this.userName,//need�to�pass�user�login�data
          ModifiedBy: this.userName,
          CreatedDate: this.currentDate,
          ModifiedDate: this.currentDate,
          BRID: this.bpmReferenceId
        }
      },
      //�API�for�Add
      addNewItem(event: any) {
        this.bpmComponentService.addStrategicNeedsItem(this.addNewRow as SSNeed).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Added Successfully';
            this.hideErrorMessage();
          }, 2000);
          this.addRow = false;
          this.addNewRow.Role = "";
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });

      },
      deleteItem(event: any, item: any) {
        this.bpmComponentService.deleteStrategicNeedsItem(item.SSNID).subscribe((data: any) => {
          setTimeout(() => {
            this.listAllData();
            this.alertType = 'success';
            this.successMessage = 'Item Deleted Successfully.';
            this.hideErrorMessage();
          }, 500);
        }, (err: any) => {
          console.log(err);
          this.alertType = 'danger';
          this.successMessage = err.error.Message;
          this.hideErrorMessage();
        });
      }
    },
  }
}

