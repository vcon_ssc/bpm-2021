import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SeeMySheetLandingPageComponent } from './see-my-sheet-landing-page/see-my-sheet-landing-page.component';


const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'dprgroups', component: LandingPageComponent },
  { path: 'mydprgroups', component: SeeMySheetLandingPageComponent },
  { path: 'unauthorized', component: PageNotFoundComponent },
  { path: '', redirectTo: './homePage', pathMatch: 'full' },
  { path: 'dashboard/:id', component: DashboardComponent, pathMatch: 'full' },
  { path: 'dashboardView/:id', component: DashboardViewComponent, pathMatch: 'full' },

 // { path: '/dashboard/:id', redirectTo, pathMatch: 'full' },
  { path: '**', redirectTo: '../unauthorized', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
