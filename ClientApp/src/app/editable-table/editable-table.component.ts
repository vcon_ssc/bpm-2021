import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { BpmReference } from '../models/bpmreference.model';
import { BusinessUnit } from '../models/businessUnit.model';
import { CoreMarket } from '../models/CoreMarket.model';
import { NationalService } from '../models/nationalService.model';
import { Region } from '../models/region.model';
import { StrategicPartner } from '../models/strategicPartner.model';
import { BpmComponentService } from '../services/bpm-component.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-editable-table',
  templateUrl: './editable-table.component.html',
  styleUrls: ['./editable-table.component.css']
})
export class EditableTableComponent implements OnInit {

  oldItem: any;
  bpmReference: any[] = [];
  addNewRow = {
    Year: "",
    RegionName: "Select DPR Group",
    CMID: 0,
    BUID: 0,
    RegionID: 0,
    SPID: 0,
    NSID: 0
  };
  isEdit: boolean[];
  businessUnits: BusinessUnit[];
  regions: Region[];
  strategicPartners: StrategicPartner[];
  nationalServices: NationalService[];
  coreMarkets: CoreMarket[];
  successMessage: string;
  alertType: string;
  //isEdit = true;
  searchText: any;
  addRow = false;
  count = 1;
  RegionIdArray: any[] = [];
  selectedRow: BpmReference;
  years: string[] = ["2019", "2020"];
  selectedYear: string;
  landingPage: boolean = false;
  topFiveAccordian: boolean = false;
  nextyearFiveAccordian: boolean = false;
  loggedInUser: string="";
  ShowExcel: boolean = false;
  constructor(private data: DataService, private spinner: NgxSpinnerService, private bpmComponentService: BpmComponentService, private route: ActivatedRoute, private router: Router) {
    this.data.changeTitle(this.landingPage);
  }

  ngOnInit() {
    this.spinner.show();
    this.selectedYear = localStorage.getItem('selectedYear');
    this.bpmReference = [];
    if (this.selectedYear === "2019") {
      this.topFiveAccordian = true;
    }
    else {
      this.nextyearFiveAccordian = true;
    }
    //this.loggedInUser = document.querySelector('#loginname').innerHTML;
    //var arrayOfAdmins = ['Mark Whitson', 'Srishti Gupta', 'Saurabh Saxena', 'Gretchen Kinsella', 'Camilo Garcia', 'Saurabh Tiwari', 'Atul Khanzode', 'Rishard Bitbaba', 'Moawia Abdelkarim', 'Justin Schmidt', 'Phil Bartkowski', 'Derek Kirkland', 'Matt Murphy', 'Jamie Moore', 'Nick Ertmer', 'Brandon Clark'];
    //var found = arrayOfAdmins.find(n => n === this.loggedInUser);
    //if (found) {
    //  this.ShowExcel = false;
    //}
    this.getRegions();

  }

  public ExportGroupWise() {
    this.spinner.show();
    var fileName = this.selectedYear + "TopPrioritySheetGroupWise.xlsx";
    var excelDownload = document.createElement("a");
    document.body.appendChild(excelDownload);
    this.spinner.show();
    this.bpmComponentService.exportToExcelGroupWise(this.selectedYear).subscribe((result: any) => {
      this.spinner.hide();
      var fileBlob = new Blob([result], { type: "vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
        window.navigator.msSaveBlob(fileBlob, fileName);
      }
      else {
        var url = window.URL.createObjectURL(fileBlob);
        excelDownload.href = url;
        excelDownload.download = fileName;
        excelDownload.click();
        window.URL.revokeObjectURL(url);
      }
    },
      (err: any) => {
        //console.log(err);
        this.spinner.hide();
        this.alertType = 'danger';
        this.successMessage = err.error.Message;
        this.hideErrorMessage();
      });
  }


  public ExportFile() {
    var fileName = this.selectedYear + "TopPrioritySheet.xlsx";
    var excelDownload = document.createElement("a");
    document.body.appendChild(excelDownload);
    this.spinner.show();
    this.bpmComponentService.exportToExcel(this.selectedYear).subscribe((result: any) => {
      this.spinner.hide();
      var fileBlob = new Blob([result], { type: "vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
        window.navigator.msSaveBlob(fileBlob, fileName);
      }
      else {
        var url = window.URL.createObjectURL(fileBlob);
        excelDownload.href = url;
        excelDownload.download = fileName;
        excelDownload.click();
        window.URL.revokeObjectURL(url);
      }
    },
      (err: any) => {
        this.spinner.hide();
        this.alertType = 'danger';
        this.successMessage = err.error.Message;
        this.hideErrorMessage();
      });

  }


  hideErrorMessage() {
    setTimeout(() => {
      this.successMessage = null;
    }, 1000);
  }

  navigateToDashboard(item: any) {
    localStorage.setItem('RegionName', item.regionName);
    localStorage.setItem('showActions', 'yes');
    this.router.navigate(['dashboard/' + item.brid]);
  }

  navigateToViewDashboard(item: any) {
    localStorage.setItem('RegionName', item.regionName);
    localStorage.setItem('showActions', 'yes');
    this.router.navigate(['dashboardView/' + item.brid]);
  }

  getRegions() {
    this.bpmComponentService.RegionAll(this.selectedYear).subscribe((data: Region[]) => {
      console.log(data);
      console.log("Region Count:" + data.length);
      this.regions = data;
      this.getAllBpmReferences1();
    }, (err: any) => {
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });
  }

  getAllBpmReferences1() {
    this.bpmComponentService.getBpmRefrencesAll(this.selectedYear).subscribe((data: BpmReference[]) => {
      this.bpmReference = data;
      this.bpmReference.forEach(obj => {
        if (obj.GroupName == 'CM') {
          obj.GroupName = 'CORE MARKET GROUPS'
        }
        else if (obj.GroupName == 'CSG') {
          obj.GroupName = 'CORPORATE SERVICE GROUPS'
        }
        else if (obj.GroupName == 'SP') {
          obj.GroupName = 'STRATEGIC BUSINESS PARTNERS'
        }
        else if (obj.GroupName == 'WVE') {
          obj.GroupName = 'WND VENTURES ENTITIES'
        }
        else if (obj.GroupName == 'IP') {
          obj.GroupName = 'INTEGRATED PRIORITIES'
        }
        else {
          obj.GroupName = 'REGIONS & BUSINESS UNITS'
        }
      });
      this.spinner.hide();
      //console.log(this.bpmReference);
      this.filterAllData(data);
      console.log(data);
    }, (err: any) => {
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });
  }


  filterAllData(data: BpmReference[]) {
    let regionsData: any = [];
    this.regions.forEach((value, index1, array) => {
      var found = data.some(function (el) {
        return el.RegionID == value.RegionID;
      });
      if (!found) {
        regionsData.push(value);
      }
    });

    this.regions = regionsData;

  }

  enableEdit(event: any, selectedRow: any, i: number) {
    this.isEdit[i] = false;
    this.selectedRow = selectedRow;
    this.oldItem = Object.assign({}, selectedRow);
    if (!this.selectedRow.RegionID) {
      this.selectedRow.RegionName = "Select DPR Group";
    }
  }
  Update(event: any, selectedRow: any, i: number) {
    this.isEdit[i] = true;
    this.selectedRow = selectedRow;

    this.bpmComponentService.editBpmRefrence(this.selectedRow).subscribe((data: BpmReference) => {
      console.log(data);

      setTimeout(() => {
        this.getAllBpmReferences1();
        this.alertType = 'success';
        this.successMessage = 'Item Updated Successfully.';
        this.hideErrorMessage();
      }, 1000);

    }, (err: any) => {
      console.log(err);
      this.alertType = 'danger';
      this.successMessage = err.error.Message;
      this.hideErrorMessage();
    });

  }
  reset(event: any, item: any, i: number) {
    this.isEdit[i] = true;
    this.bpmReference[i] = this.oldItem;
  }
  cancel(event: any, item: any) {
    this.addRow = false;
    this.count--;
  }
  AddNewRow(event: any) {
    this.addRow = true;
    this.count++;
    this.addNewRow = {
      Year: "",
      RegionName: "Select DPR Group",
      CMID: null,
      BUID: null,
      RegionID: null,
      SPID: null,
      NSID: null
    };
  }

  addNewItem(event: any) {

    let addNewRow = {
      Year: this.selectedYear,
      RegionID: this.addNewRow.RegionID,
      BUID: this.addNewRow.BUID,
      SPID: this.addNewRow.SPID,
      NSID: this.addNewRow.NSID,
      CMID: this.addNewRow.CMID
    }

    this.bpmComponentService.addBpmRefrence(addNewRow as BpmReference).subscribe(
      (data: BpmReference) => {
        console.log(data);
        setTimeout(() => {
          this.getRegions();
          this.alertType = 'success';
          this.successMessage = 'Item Added Successfully.';
          this.hideErrorMessage();
        }, 1000);
        this.addRow = false;
      }, (err: any) => {
        console.log(err);
        this.alertType = 'danger';
        this.successMessage = err.error.Message;
        this.hideErrorMessage();
      }
    );
  }

  ChangeRegion(menu: any, item: BpmReference) {
    item.RegionName = menu.RegionName;
    item.RegionID = menu.RegionID;
  }


}

