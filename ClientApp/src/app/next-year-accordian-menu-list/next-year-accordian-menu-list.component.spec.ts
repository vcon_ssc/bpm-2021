import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextYearAccordianMenuListComponent } from './next-year-accordian-menu-list.component';

describe('NextYearAccordianMenuListComponent', () => {
  let component: NextYearAccordianMenuListComponent;
  let fixture: ComponentFixture<NextYearAccordianMenuListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextYearAccordianMenuListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextYearAccordianMenuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
