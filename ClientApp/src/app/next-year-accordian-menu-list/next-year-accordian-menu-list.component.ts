import { Component, Input, OnInit } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgbAccordionConfig, NgbPanelChangeEvent, NgbAccordion, NgbDropdown, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { CapitalExpenses } from '../models/capitalExpenses.model';
import { FocusAreas } from '../models/focusAreas.model';
import { IntegratedTopFive } from '../models/integratedTopFive';
import { LeaseModifications } from '../models/leaseModifications.model';
import { Narrative } from '../models/Narrative';
import { RecessionPlanning } from '../models/recessionPlanning.model';
import { DataService } from '../services/data.service';
import { BpmComponentService } from '../services/bpm-component.service';

@Component({
  selector: 'app-next-year-accordian-menu-list',
  templateUrl: './next-year-accordian-menu-list.component.html',
  styleUrls: ['./next-year-accordian-menu-list.component.css'],
  providers: [NgbDropdownConfig]
})
export class NextYearAccordianMenuListComponent implements OnInit {
 
  model: any = {};
  // expanded = false;
   data1: boolean = false;
   vdc: boolean = false;
   spw: boolean = false;
   prefab: boolean = false;
   rp: boolean = false;
  
   RegionName: string;
   bpmReferenceId: number;
   activeIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map(i => `ngb-panel-${i}`);
   dropdownList: any = [];

dropdownSettings: IDropdownSettings = {
   singleSelection: false,
   idField: 'RequireGroupId',
   textField: 'Name',
   selectAllText: 'Select All',
   unSelectAllText: 'UnSelect All',
   itemsShowLimit: 2,
   allowSearchFilter: true
   };

   dropdownSettings2: IDropdownSettings = {
       singleSelection: false,
       idField: 'SupportItfId',
       textField: 'Name',
       selectAllText: 'Select All',
       unSelectAllText: 'UnSelect All',
       itemsShowLimit: 2,
       allowSearchFilter: true
   };

   richTextHandler = {
       height: 500,
       minHeight: 200,
       maxHeight: 200,
       focus: true,
       toolbar: [
           ["style", ["bold", "italic", "underline", "clear"]],
           ["font", ["strikethrough", "superscript", "subscript"]],
           ["fontsize", ["fontsize"]],
           ["color", ["color"]],
           ["para", ["ul", "ol", "paragraph"]],
           ["height", ["height"]]
       ],
       setText: function () {
       }
   };

 

   prevYear: string;
   showActions: boolean;
   selectedSortOrder: string = "Data";
  // currentDate: Date = new Date();
   currentDate: string;
   userName = "";
   sortOrders = [ 'Data',
       'VDC',
      'SelfPerformWork',
       'PreFabrication',
      'RecessionPlanning' ]
  // sortOrders: string[] = ["Data", "VDC", "SelfPerformWork", "PreFabrication", "RecessionPlanning"];
   @Input() currentYear: string;
   @Input() BRID: string;
   angForm: FormGroup;
   caseOneAccordian: boolean = false;
   caseThreeAccordian: boolean = false;
   caseTwoAccordian: boolean = false;
   caseFourAccordian: boolean = false;
   groupName: any;
   RBFA: string;
   RBLM: string;
   RBCE: string;
   FA: string;
   LM: string;
   CE: string;
   disabled = true;

    myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'mm/dd/yyyy',
        editableDateField :false
   };  
   ITFDropdown: any;
 
   constructor(config: NgbDropdownConfig, private fb: FormBuilder, private spinner: NgxSpinnerService,private activatedRoute: ActivatedRoute, private bpmComponentService: BpmComponentService, private data: DataService) {
       config.placement = 'top-left';
       this.createForm();
   }
   ChangeSortOrder(newSortOrder: string) {
       this.selectedSortOrder = newSortOrder;
   }

   
   createForm() {
       this.angForm = this.fb.group({
           narrativename: ['', Validators.required],
           narrativenameadd: ['', Validators.required],
           chk1: [''],
           chk2: [''],
           chk3: [''],
           requireGroup: ['', Validators.required],
           supportITF: ['', Validators.required],
           incrementalCost:[''],
           succname: ['', Validators.required],
           succnameadd: ['', Validators.required],
           leasename: ['', Validators.required],
           leasenameadd: ['', Validators.required],
           datename: ['', Validators.required],
           datenameadd: ['', Validators.required],
           planname: ['', Validators.required],
           plannameadd: ['', Validators.required],
           descriptionname: ['', Validators.required],
           descriptionnameadd: ['', Validators.required],
           amountname: ['', Validators.required],
           amountnameadd: ['', Validators.required],
           planningname: ['', Validators.required],
           planningnameadd: ['', Validators.required],
           description: ['', Validators.required],
           descriptionadd: ['', Validators.required],
           integrated: ['', Validators.required],
           integratedadd: ['', Validators.required],
           category: ['', Validators.required],
           categoryadd: ['', Validators.required]

       });
   }

   ngOnInit() {
       this.spinner.show();
       setTimeout(() => {
           /** spinner ends after 5 seconds */
           this.spinner.hide();
       }, 5000);
       this.activatedRoute.params.subscribe((params: Params) => {
           this.bpmReferenceId = params['id'];
       });
       this.bpmComponentService.getGroupByBRId(this.bpmReferenceId, this.currentYear).subscribe((data: any) => {
           this.groupName = data[0].groupName;
           console.log("ddd",data[0]);
           console.log(data[0].groupName);
           if (data[0].groupName === "RBU") {
               this.caseOneAccordian = true;
               this.RBFA = "Please identify the Top Priorities that your Business Unit or Region will take in " + this.currentYear+ " to improve its performance.";
               this.RBLM = "Is your work  group planning to lease any additional space or have a lease that is reaching expiration in " + this.currentYear + "  If so, how much net additional space are you anticipating leasing or what is the lease expiration date and what are your plans for renewal?";
               this.RBCE = "Is your work group planning any major capital expenses in " + this.currentYear + " over $250,000 in a single transaction (leasehold improvements; OES equipment including formwork; etc..)  If so, please list those items and identify the anticipated quarter that the cash outlay would hit the books.";
           }
           else if (data[0].groupName==='IP') {
               this.caseThreeAccordian = true;
           }
           else if (data[0].groupName === "CSG" || data[0].groupName === "CM" )
           {
               this.caseTwoAccordian = true;
               this.FA = "Please identify the Top Priorities that your work group will take in " + this.currentYear + " to improve its performance.";
               this.LM = "Is your work  group planning to lease any additional space or have a lease that is reaching expiration in " + this.currentYear + "?   If so, how much net additional space are you anticipating leasing or what is the lease expiration date and what are your plans for renewal?";
               this.CE = "Is your work group planning any major capital expenses in " + this.currentYear + " over $250,000 in a single transaction (leasehold improvements; OES equipment including formwork; etc..)  If so, please list those items and identify the anticipated quarter that the cash outlay would hit the books.";
           }
           if (data[0].groupName === "SP" || data[0].groupName === "RBU" || data[0].groupName === "CM" || data[0].groupName === "WVE") {
               this.FA = "Please identify the Top Priorities that your work group will take in " + this.currentYear + " to improve its performance.";
               this.LM = "Is your work  group planning to lease any additional space or have a lease that is reaching expiration in " + this.currentYear + "?   If so, how much net additional space are you anticipating leasing or what is the lease expiration date and what are your plans for renewal?";
               this.CE = "Is your work group planning any major capital expenses in " + this.currentYear + " over $250,000 in a single transaction (leasehold improvements; OES equipment including formwork; etc..)  If so, please list those items and identify the anticipated quarter that the cash outlay would hit the books.";

               this.caseFourAccordian = true;
           }
          // console.log("Count:" + data.length);
           
       }, (err: any) => {
               this.spinner.hide();
           console.log(err);
       });

       this.bpmComponentService.RequireGroupsAll().subscribe((data: any) => {
           this.dropdownList = data;
           console.log(this.dropdownList);
       });
       this.bpmComponentService.SupportITFAll().subscribe((data: any) => {
           this.ITFDropdown = data;
           console.log(this.ITFDropdown);
       });
       if (this.currentYear == "Select Year" || !this.currentYear) {
           this.currentYear = localStorage.getItem('selectedYear');
           this.data.changeMessage(this.currentYear);
       }
       this.prevYear = "" + (parseInt(this.currentYear) - 1);
       this.RegionName = localStorage.getItem('RegionName');
       this.tableList.narrative.initializeData(this.bpmComponentService, this.spinner, this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
       this.tableList.focusAreas.initializeData(this.bpmComponentService, this.spinner ,this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
       this.tableList.leaseModifications.initializeData(this.bpmComponentService, this.spinner ,this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
       this.tableList.capitalExpenses.initializeData(this.bpmComponentService, this.spinner , this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
       this.tableList.recessionPlanning.initializeData(this.bpmComponentService, this.spinner , this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
       this.tableList.integratedTopFive.initializeData(this.bpmComponentService, this.spinner ,this.bpmReferenceId, this.currentDate, this.userName, this.currentYear);
       if (localStorage.getItem('showActions') === 'yes') {
           this.showActions = false;
       }
   }

   getCurrentDate() {
       let today = new Date();
       let dd = today.getDate();
       let mm = today.getMonth() + 1; //January is 0!
       let yyyy = today.getFullYear();
       this.currentDate = dd + '/' + mm + '/' + yyyy;
   }


   tableList = {
       narrative: {
           titles: [ 'Description','Action'],
           initializeData(bpmComponentService: BpmComponentService, spinner: NgxSpinnerService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
               this.currentDate = currentDate;
               this.bpmReferenceId = bpmReferenceId;
               this.bpmComponentService = bpmComponentService;
               this.userName = userName;
               this.currentYear = currentYear;
               this.spinner = spinner;
               this.addNewRow = {};
               this.list = [];
               this.updatedData = "";
               this.listAllData();
               this.isEditenabled = true;
           },
           listAllData() {
               //************************************************
               this.bpmComponentService.narrativeAll(this.bpmReferenceId, this.currentYear).subscribe((data: Narrative[]) => {
                   this.list = data;
                   console.log(this.list);
                   if (this.list.length !== 0) {
                       this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                   }
                   this.isEdit = [];
                   this.count = data.length;
                   this.isEditenabled = true;
                   for (let i = 0; i < data.length; i++) {
                       this.isEdit[i] = true;
                   }
               }, (err: any) => {
                   this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           hideErrorMessage() {
               setTimeout(() => {
                   this.successMessage = null;
               }, 2000);
           },
           successMessage: '',
           alertType: '',
           addRow: false,
           selectedRow: {
           },
           Update(event: any, selectedRow: any, i: number) {
               this.spinner.show();
               this.isEdit[i] = true;
               this.isEditenabled = true;
               this.selectedRow = selectedRow;
               this.selectedRow.ModifiedDate = this.currentDate;
               //***********************************
               this.bpmComponentService.editNarrative(this.selectedRow as Narrative).subscribe(
                   (data: Narrative) => {
                       console.log(data);

                       setTimeout(() => {
                           this.listAllData();
                           this.spinner.hide();
                           this.alertType = 'success';
                           this.successMessage = 'Item Updated Successfully.';
                           this.hideErrorMessage();
                       }, 1000);

                   }, (err: any) => {
                       this.spinner.hide();
                       console.log(err);
                       this.alertType = 'danger';
                       this.successMessage = err.error.Message;
                       this.hideErrorMessage();
                   }
               );
           },
           enableEdit(event: any, selectedRow: any, i: number) {
               this.isEdit[i] = false;
               this.isEditenabled = false;
               this.selectedRow = selectedRow;
               this.oldItem = Object.assign({}, selectedRow);
           },
           cancel(event: any) {
               this.addRow = false;
               this.count--;
           },
           reset(event: any, i: number) {
               this.list[i] = this.oldItem;
               this.isEdit[i] = true;
               this.isEditenabled = true;
           },
           AddNewRow(event: any) {
               this.addRow = true;
               this.count = (this.list) ? this.list.length + 1 : 1;
               this.addNewRow = {
                   //CSID: 1,
                   Description: "",
                   Year: this.currentYear, //this.currentYear,// need to do dynamic
                   SortOrder: 0,
                   CreatedBy: this.userName,//need to pass user login data
                   ModifiedBy: this.userName,
                   CreatedDate: this.currentDate,
                   ModifiedDate: this.currentDate,
                   BRID: this.bpmReferenceId
               }
           },
           // API for Add
           addNewItem(event: any) {
               this.spinner.show();
               //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
               //**************************
               this.bpmComponentService.addNarrative(this.addNewRow as Narrative).subscribe((data: Narrative) => {
                   console.log(data);
                   setTimeout(() => {
                       this.spinner.hide();
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Added Successfully.';
                       this.hideErrorMessage();
                   }, 1000);

                   this.addRow = false;
                   this.addNewRow.narrative = "";
               }, (err: any) => {
                   this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           deleteItem(event: any, item: any) {
               this.spinner.show();
               this.bpmComponentService.deleteNarrative(item.NID).subscribe((data: any) => {
                   setTimeout(() => {
                       this.spinner.hide();
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Deleted Successfully.';
                       this.hideErrorMessage();
                   }, 500);
               }, (err: any) => {
                   this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           }
       },
       focusAreas: {
           titles: ['#', 'Description', 'Actions'],
           initializeData(bpmComponentService: BpmComponentService, spinner: NgxSpinnerService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
               this.currentDate = currentDate;
               this.bpmReferenceId = bpmReferenceId;
               this.bpmComponentService = bpmComponentService;
               this.userName = userName;
               this.currentYear = currentYear;
               this.spinner = spinner;
               this.addNewRow = {};
               this.list = [];
               this.dropdown1 = [];
               this.dropdown2 = [];
               this.updatedData = "";
               this.listAllData();
               this.isEditenabled = true;
           },
           listAllData() {
               this.bpmComponentService.FocusAreasAll(this.bpmReferenceId, this.currentYear).subscribe((data: FocusAreas[]) => {
                   this.list = data;
                   if (this.list.length !== 0) {
                       this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                   }
                   this.isEdit = [];
                   this.count = data.length;
                   this.isEditenabled = true;
                   for (let i = 0; i < data.length; i++) {
                       this.isEdit[i] = true;
                   }
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           hideErrorMessage() {
               setTimeout(() => {
                   this.successMessage = null;
               }, 2000);
           },
           searchText: 'abc',
           successMessage: '',
           alertType: '',
           addRow: false,
           selectedRow: {},
           onGroupSelectAll(item: any, selectedArray: any[]) {
                   this.dropdown1 = selectedArray.filter((b: any) => b.Name !== "None");
           },

           onGroupSelect(item: any, selectedArray: any[]) {
               if (item.Name === 'None') {
                   this.dropdown1 = selectedArray.filter((b: any) => b.Name === "None");
               }
               else {
                   this.dropdown1 = selectedArray.filter((b: any) => b.Name !== "None");
               } 
           },
           onItemSelect(item: any, selectedArray: any[]) {
               if (item.Name === 'None') {
                   this.dropdown2 = selectedArray.filter((b: any) => b.Name === "None");
               }
               else {
                   this.dropdown2 = selectedArray.filter((b: any) => b.Name !== "None");
               }
           },
           Update(angForm: FormGroup, selectedRow: any, i: number) {
               console.log("dfoooo", this.angForm);
              
               this.addRow = false;
               this.spinner.show();
               this.isEdit[i] = true;
               this.isEditenabled = true;
               this.selectedRow = selectedRow;
               this.selectedRow.FocusAreaRequireGroups = this.dropdown1
               this.selectedRow.FocusAreaSupportITFs = this.dropdown2
               this.selectedRow.ModifiedDate = this.currentDate;
               
               this.bpmComponentService.editFocusAreasItem(this.selectedRow as FocusAreas).subscribe(
                   (data: FocusAreas) => {
                       console.log(data);

                       setTimeout(() => {
                          
                           this.listAllData();
                           this.spinner.hide();
                           this.dropdown1 = [];
                           this.dropdown2 = [];
                           this.alertType = 'success';
                           this.successMessage = 'Item Updated Successfully.';

                           this.hideErrorMessage();
                       }, 1000);

                   }, (err: any) => {
                       this.spinner.hide();
                       console.log(err);
                       this.alertType = 'danger';
                       this.successMessage = err.error.Message;
                       this.hideErrorMessage();
                   }
               );
           },
           enableEdit(event: any, selectedRow: any, i: number) {
               //this.addRow = true;
               this.isEdit[i] = false;
               this.isEditenabled = false;
               this.selectedRow = selectedRow;
               this.dropdown1 = selectedRow.focusAreaRequireGroups
               this.dropdown2 = selectedRow.focusAreaSupportITFs
               this.oldItem = Object.assign({}, selectedRow);
           },
           cancel(event: any) {
               this.addRow = false;
               this.count--;
           },
           reset(event: any, i: number) {
               //this.addRow = false;
               this.list[i] = this.oldItem;
               this.isEdit[i] = true;
               this.isEditenabled = true;
           },
           AddNewRow(event: any) {
               this.addRow = true;
               
               this.count = (this.list) ? this.list.length + 1 : 1;
               //this.isEdit[this.count] = true;
               this.addNewRow = {
                   //CSID: 1,
                   StrategicActions: "",
                   IsImpact: "",
                   RequireHelp: "",
                   FocusAreas: "",
                   IncrementalCosts: "",
                   OngingEffort: "",
                   Year: this.currentYear, //this.currentYear,// need to do dynamic
                   SortOrder: 0,
                   CreatedBy: this.userName,//need to pass user login data
                   ModifiedBy: this.userName,
                   CreatedDate: this.currentDate,
                   ModifiedDate: this.currentDate,
                   BRID: this.bpmReferenceId,
                   FocusAreaRequireGroups: [],
                   FocusAreaSupportITFs: [],
               }
           },
           // API for Add
           addNewItem(event: any) {
               this.spinner.show();
               this.addNewRow.FocusAreaRequireGroups = this.dropdown1
               this.addNewRow.FocusAreaSupportITFs = this.dropdown2
               //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
               this.bpmComponentService.addFocusAreasItem(this.addNewRow as FocusAreas).subscribe((data: FocusAreas) => {
                   console.log(data);
                   setTimeout(() => {
                       this.spinner.hide();
                       this.listAllData();
                       this.dropdown1 = [];
                       this.dropdown2 = [];
                       this.alertType = 'success';
                       this.successMessage = 'Item Added Successfully.';
                       this.hideErrorMessage();
                   }, 1000);

                   this.addRow = false;
                   this.addNewRow.FocusAreas = "";
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           deleteItem(event: any, item: any) {
               this.spinner.show();
               this.bpmComponentService.deleteFocusAreasItem(item.FAID).subscribe((data: any) => {
                   setTimeout(() => {
                       this.spinner.hide();
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Deleted Successfully.';
                       this.hideErrorMessage();
                   }, 500);
               }, (err: any) => {
                   this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           }
       },
       leaseModifications: {
           titles: ['#', 'Lease Description', 'Current Expiration Date', 'Plan Of Renewal','Actions'],
           initializeData(bpmComponentService: BpmComponentService, spinner: NgxSpinnerService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
               this.currentDate = currentDate;
               this.bpmReferenceId = bpmReferenceId;
               this.bpmComponentService = bpmComponentService;
               this.userName = userName;
               this.currentYear = currentYear;
               this.spinner = spinner;
               this.addNewRow = {};
               this.list = [];
               this.updatedData = "";
               this.listAllData();
               this.isEditenabled = true;
           },
           listAllData() {
               this.bpmComponentService.LeaseModificationsAll(this.bpmReferenceId, this.currentYear).subscribe((data: LeaseModifications[]) => {
                   
                   this.list = data;
                   if (this.list.length !== 0) {
                       this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                   }
                   //var d = new Date('yourDate');
                   //d.setMinutes(d.getMinutes() + d.getTimezoneOffset()); 
                   this.list.forEach((m:any) => {
                       if (m.Date != null) {
                           //m.Date = this.convertUTCDateToLocalDate(new Date(m.Date));
                           m.Date = new Date(m.Date).toLocaleDateString()
                       }

                   });
                   console.log("yuio",this.list);
                   this.isEdit = [];
                   this.count = data.length;
                   this.isEditenabled = true;
                   for (let i = 0; i < data.length; i++) {
                       this.isEdit[i] = true;
                   }
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           hideErrorMessage() {
               setTimeout(() => {
                   this.successMessage = null;
               }, 2000);
           },
           searchText: 'abc',
           successMessage: '',
           alertType: '',
           addRow: false,
           selectedRow: {
           },
           Update(event: any, selectedRow: any, i: number) {
               this.spinner.show();
               this.isEdit[i] = true;
               this.isEditenabled = true;
               this.selectedRow = selectedRow;
               this.selectedRow.ModifiedDate = this.currentDate;
               this.selectedRow.Date = this.selectedRow.Date.jsdate;
               this.bpmComponentService.editLeaseModificationsItem(this.selectedRow as LeaseModifications).subscribe(
                   (data: LeaseModifications) => {
                       this.spinner.hide();
                       console.log(data);

                       setTimeout(() => {
                           this.listAllData();
                           this.alertType = 'success';
                           this.successMessage = 'Item Updated Successfully.';
                           this.hideErrorMessage();
                       }, 1000);

                   }, (err: any) => {
                       this.spinner.hide();
                       console.log(err);
                       this.alertType = 'danger';
                       this.successMessage = err.error.Message;
                       this.hideErrorMessage();
                   }
               );
           },
           enableEdit(event: any, selectedRow: any, i: number) {
               this.isEdit[i] = false;
               this.isEditenabled = false;
               this.selectedRow = selectedRow;
               this.selectedRow.Date = { jsdate: new Date(this.selectedRow.Date) };
               this.oldItem = Object.assign({}, selectedRow);
               //this.oldItem.Date= this.oldItem.Date.jsdate;
           },
           cancel(event: any) {
               this.addRow = false;
               this.count--;
           },
           reset(event: any, i: number) {
               this.oldItem.Date = new Date(this.oldItem.Date.jsdate).toLocaleDateString();
               this.list[i] = this.oldItem;
               this.isEdit[i] = true;
               this.isEditenabled = true;
           },
           AddNewRow(event: any) {
               this.addRow = true;
               this.count = (this.list) ? this.list.length + 1 : 1;
               this.addNewRow = {
                   //CSID: 1,
                   Description: "",
                   Date: "",
                   PlanOfRenewal: "",
                   Year: this.currentYear, //this.currentYear,// need to do dynamic
                   SortOrder: 0,
                   CreatedBy: this.userName,//need to pass user login data
                   ModifiedBy: this.userName,
                   CreatedDate: this.currentDate,
                   ModifiedDate: this.currentDate,
                   BRID: this.bpmReferenceId
               }
           },
           // API for Add
           addNewItem(event: any) {
               this.spinner.show();
              // console.log('pppp', new Date());
               //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
              // const jsDate = new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
               this.addNewRow.Date = this.addNewRow.Date.jsdate;
               this.bpmComponentService.addLeaseModificationsItem(this.addNewRow as LeaseModifications).subscribe((data: LeaseModifications) => {
                   console.log(data);
                   this.spinner.hide();
                   setTimeout(() => {
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Added Successfully.';
                       this.hideErrorMessage();
                   }, 1000);

                   this.addRow = false;
                   this.addNewRow.LeaseModifications = "";
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           deleteItem(event: any, item: any) {
               this.spinner.show();
               this.bpmComponentService.deleteLeaseModificationsItem(item.lmid).subscribe((data: any) => {
                   setTimeout(() => {
                       this.spinner.hide();
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Deleted Successfully.';
                       this.hideErrorMessage();
                   }, 500);
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           }
       },
       capitalExpenses: {
           titles: ['#', 'Capital Expenses Description', 'Amount','Actions'],
           initializeData(bpmComponentService: BpmComponentService, spinner: NgxSpinnerService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
               this.currentDate = currentDate;
               this.bpmReferenceId = bpmReferenceId;
               this.bpmComponentService = bpmComponentService;
               this.userName = userName;
               this.currentYear = currentYear;
               this.spinner = spinner;
               this.addNewRow = {};
               this.list = [];
               this.updatedData = "";
               this.listAllData();
               this.isEditenabled = true;
           },
           listAllData() {
               this.bpmComponentService.CapitalExpensesAll(this.bpmReferenceId,this.currentYear).subscribe((data: CapitalExpenses[]) => {
                   this.list = data;
                   console.log(this.list);
                   if (this.list.length !== 0) {
                       this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                   }
                   this.isEdit = [];
                   this.count = data.length;
                   this.isEditenabled = true;
                   for (let i = 0; i < data.length; i++) {
                       this.isEdit[i] = true;
                   }
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           hideErrorMessage() {
               setTimeout(() => {
                   this.successMessage = null;
               }, 2000);
           },
           searchText: 'abc',
           successMessage: '',
           alertType: '',
           addRow: false,
           selectedRow: {
           },
           Update(event: any, selectedRow: any, i: number) {
               this.spinner.show();
               this.isEdit[i] = true;
               this.isEditenabled = true;
               this.selectedRow = selectedRow;
               this.selectedRow.ModifiedDate = this.currentDate;
               this.bpmComponentService.editCapitalExpensesItem(this.selectedRow as CapitalExpenses).subscribe(
                   (data: CapitalExpenses) => {
                       console.log(data);
                       this.spinner.hide();
                       setTimeout(() => {
                           this.listAllData();
                           this.alertType = 'success';
                           this.successMessage = 'Item Updated Successfully.';
                           this.hideErrorMessage();
                       }, 1000);

                   }, (err: any) => {
                       this.spinner.hide();
                       console.log(err);
                       this.alertType = 'danger';
                       this.successMessage = err.error.Message;
                       this.hideErrorMessage();
                   }
               );
           },
           enableEdit(event: any, selectedRow: any, i: number) {
               this.isEdit[i] = false;
               this.isEditenabled = false;
               this.selectedRow = selectedRow;
               this.oldItem = Object.assign({}, selectedRow);
           },
           cancel(event: any) {
               this.addRow = false;
               this.count--;
           },
           reset(event: any, i: number) {
               this.list[i] = this.oldItem;
               this.isEdit[i] = true;
               this.isEditenabled = true;
           },
           AddNewRow(event: any) {
               this.addRow = true;
               this.count = (this.list) ? this.list.length + 1 : 1;
               this.addNewRow = {
                   //CSID: 1,
                   Description: "",
                   Amount:"",
                   Year: this.currentYear, //this.currentYear,// need to do dynamic
                   SortOrder: 0,
                   CreatedBy: this.userName,//need to pass user login data
                   ModifiedBy: this.userName,
                   CreatedDate: this.currentDate,
                   ModifiedDate: this.currentDate,
                   BRID: this.bpmReferenceId
               }
           },
           // API for Add
           addNewItem(event: any) {
               this.spinner.show();
               //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
               this.bpmComponentService.addCapitalExpensesItem(this.addNewRow as CapitalExpenses).subscribe((data: CapitalExpenses) => {
                   console.log(data);
                   this.spinner.hide();
                   setTimeout(() => {
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Added Successfully.';
                       this.hideErrorMessage();
                   }, 1000);

                   this.addRow = false;
                   this.addNewRow.CapitalExpenses = "";
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           deleteItem(event: any, item: any) {
               this.spinner.show();
               this.bpmComponentService.deleteCapitalExpensesItem(item.ceid).subscribe((data: any) => {
                   setTimeout(() => {
                       this.spinner.hide();
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Deleted Successfully.';
                       this.hideErrorMessage();
                   }, 500);
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           }
       },
       recessionPlanning: {
           //titles: ['#', 'Data', 'VDC', 'Self Perform Work', 'Pre - fabrication', 'Recession planning','Actions'],
           initializeData(bpmComponentService: BpmComponentService, spinner: NgxSpinnerService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
               this.currentDate = currentDate;
               this.bpmReferenceId = bpmReferenceId;
               this.bpmComponentService = bpmComponentService;
               this.userName = userName;
               this.currentYear = currentYear;
               this.spinner = spinner;
               this.addNewRow = {};
               this.list = [];
               this.updatedData = "";
               this.isEdit =[];
               this.sortOrders = ['Data',
                   'VDC',
                   'Self Perform Work',
                   'Pre-Fabrication',
                   'Recession Planning'];
              
               this.listAllData();
               this.isEditenabled = true;
               this.strategies = [];
               this.sortOrders.forEach((m: any) => {
                   this.list.push({
                       'Category': m, 'CSRPID': 0, 'Description': "", 'isEdit':false
                   })
               });
               console.log("jkj", this.list);
           },
           
           listAllData() {
               this.bpmComponentService.RecessionPlanningAll(this.bpmReferenceId, this.currentYear).subscribe((data: RecessionPlanning[]) => {
                   console.log("fgdsvsgvv", data);
                   data.forEach((m: any) => {
                     
                       var a = this.list.find((s: any) => s.Category == m.Category);
                       console.log("index: " + a);
                       var ind = this.list.indexOf(a);
                       console.log("index: "+ind);
                       this.list[ind].Description = m.Description; 
                       this.list[ind].CSRPID = m.CSRPID; 
                       console.log("lisy: ",this.list);
                       
                   });
                   
                   console.log(this.list);
                   
                   this.count = data.length;
                   this.isEditenabled = true;
                  
             
                   //for (let i = 0; i < data.length; i++) {
                   //    this.isEdit[i] = true;
                   //}
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           hideErrorMessage() {
               setTimeout(() => {
                   this.successMessage = null;
               }, 2000);
           },
           searchText: 'abc',
           successMessage: '',
           alertType: '',
           addRow: false,
           selectedRow: {
           },
           Update(event: any, selectedRow: any, i: number)
           {

               this.spinner.show();
               this.isEdit[i] = true;
               this.isEditenabled = true;
               this.selectedRow = selectedRow;
               this.selectedRow.ModifiedDate = this.currentDate;

               if (selectedRow.CSRPID == 0) {
                   //ADD
                   this.AddReccessionPlanning(this.selectedRow, i);
               }
               else {
                   //Update
                   this.UpdateReccessionPlanning(this.selectedRow,i)
               }
               
           },
           UpdateReccessionPlanning(selectedRow: any, i: number) {
               selectedRow.BRID = this.bpmReferenceId;
               selectedRow.Year = this.currentYear;
               this.bpmComponentService.editRecessionPlanningItem(selectedRow as RecessionPlanning).subscribe(
                   (data: RecessionPlanning) => {
                       console.log(data);
                       this.spinner.hide();
                       setTimeout(() => {
                           this.listAllData();
                           this.alertType = 'success';
                           this.successMessage = 'Item Updated Successfully.';
                           this.hideErrorMessage();
                       }, 1000);

                   }, (err: any) => {
                       this.spinner.hide();
                       console.log(err);
                       this.alertType = 'danger';
                       this.successMessage = err.error.Message;
                       this.hideErrorMessage();
                   }
               );
               this.list[i].isEdit = false;
           },
           enableEdit(event: any, selectedRow: any, i: number) {
               this.list[i].isEdit = true;
               this.isEdit[i] = false;
               this.isEditenabled = false;
               this.selectedRow = selectedRow;
               this.oldItem = Object.assign({}, selectedRow);
           },
           cancel(event: any) {
               this.addRow = false;
               this.count--;
           },
           reset(event: any, i: number) {
               this.list[i] = this.oldItem;
               this.list[i].isEdit = false;
               this.isEditenabled = true;
           },
           AddNewRow(event: any) {
               this.addRow = true;
               this.count = (this.list) ? this.list.length + 1 : 1;
               this.addNewRow = {
                   //CSID: 1,
                   Category: "Data",
                   Description: "",
                   Year: this.currentYear, //this.currentYear,// need to do dynamic
                   SortOrder: 0,
                   CreatedBy: this.userName,//need to pass user login data
                   ModifiedBy: this.userName,
                   CreatedDate: this.currentDate,
                   ModifiedDate: this.currentDate,
                   BRID: this.bpmReferenceId
               }
           },
           // API for Add
           addNewItem(event: any) {
              
               var CategoryArray = [];
               CategoryArray = this.list;
              // var checkCategory = CategoryArray.some(function (el: any) { return el.Category === this.addNewRow.Category });
               var checkCategory = CategoryArray.find((x: any) => x.Category == this.addNewRow.Category);
               if (!checkCategory) {
                   this.AddReccessionPlanning();
               }
               else {
                   
                   this.alertType = 'danger';
                   this.successMessage = 'You have already entered this category';
                   this.hideErrorMessage();
               }
               
           },
           AddReccessionPlanning(SelectedRow: any, i: number) {
               this.spinner.show();
               SelectedRow.CSRPID = null;
               SelectedRow.Year = this.currentYear;
              // SelectedRow.CreatedBy = this.userName;//need to pass user login data
              // SelectedRow.ModifiedBy = this.userName;
               SelectedRow.CreatedDate = new Date();
               SelectedRow.ModifiedDate = new Date();
               SelectedRow.BRID = this.bpmReferenceId;
               
               this.bpmComponentService.addRecessionPlanningItem(SelectedRow).subscribe((data: RecessionPlanning) => {

                   console.log(data);
                   this.spinner.hide();
                   setTimeout(() => {
                       this.listAllData();
                       this.alertType = 'success';
                       this.successMessage = 'Item Added Successfully.';
                       this.hideErrorMessage();
                   }, 1000);

                   this.list[i].isEdit = false;
                   //this.addNewRow.FocusAreas = "";
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });

           },

           deleteItem(event: any, item: any) {
               this.spinner.show();
               this.bpmComponentService.deleteRecessionPlanningItem(item.CSRPID).subscribe((data: any) => {
                   setTimeout(() => {
                       this.listAllData();
                       this.spinner.hide();
                       this.alertType = 'success';
                       this.successMessage = 'Item Deleted Successfully.';
                       this.hideErrorMessage();
                   }, 500);
               }, (err: any) => {
                   this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
                   });

               //var a = this.list.find((s: any) => s.Category == Category);
               //console.log("index: " + a);
               var ind = this.list.indexOf(item);
               console.log("index delete : " + ind);
               this.list[ind].Description = "";
               this.list[ind].CSRPID = 0; 
               this.list[ind].isEdit = false; 
           }
       },
       integratedTopFive: {
           titles: ['#', 'Strategic Actions', 'Actions'],
           initializeData(bpmComponentService: BpmComponentService, spinner: NgxSpinnerService, bpmReferenceId: number, currentDate: string, userName: string, currentYear: string) {
               this.currentDate = currentDate;
               this.bpmReferenceId = bpmReferenceId;
               this.bpmComponentService = bpmComponentService;
               this.userName = userName;
               this.currentYear = currentYear;
               this.spinner = spinner;
               this.addNewRow = {};
               this.list = [];
               this.updatedData = "";
               this.listAllData();
               this.isEditenabled = true;
           },
           listAllData() {
               this.bpmComponentService.IntegratedTopFiveAll(this.bpmReferenceId,this.currentYear).subscribe((data: IntegratedTopFive[]) => {
                   this.list = data;
                   console.log(this.list);
                   if (this.list.length !== 0) {
                       this.updatedData = " " + data[data.length - 1].ModifiedBy + " , " + new Date(data[data.length - 1].ModifiedDate).toLocaleDateString();
                   }
                   this.isEdit = [];
                   this.count = data.length;
                   this.isEditenabled = true;
                   for (let i = 0; i < data.length; i++) {
                       this.isEdit[i] = true;
                   }
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           hideErrorMessage() {
               setTimeout(() => {
                   this.successMessage = null;
               }, 2000);
           },
           searchText: 'abc',
           successMessage: '',
           alertType: '',
           addRow: false,
           selectedRow: {
           },
           Update(event: any, selectedRow: any, i: number) {
               this.spinner.show();
               this.isEdit[i] = true;
               this.isEditenabled = true;
               this.selectedRow = selectedRow;
               this.selectedRow.ModifiedDate = this.currentDate;
               this.bpmComponentService.editIntegratedTopFiveItem(this.selectedRow as IntegratedTopFive).subscribe(
                   (data: IntegratedTopFive) => {
                       console.log(data);
                       this.spinner.hide();
                       setTimeout(() => {
                           this.listAllData();
                           this.alertType = 'success';
                           this.successMessage = 'Item Updated Successfully.';
                           this.hideErrorMessage();
                       }, 1000);

                   }, (err: any) => {
                       this.spinner.hide();
                       console.log(err);
                       this.alertType = 'danger';
                       this.successMessage = err.error.Message;
                       this.hideErrorMessage();
                   }
               );
           },
           enableEdit(event: any, selectedRow: any, i: number) {
               this.isEdit[i] = false;
               this.isEditenabled = false;
               this.selectedRow = selectedRow;
               this.oldItem = Object.assign({}, selectedRow);
           },
           cancel(event: any) {
               this.addRow = false;
               this.count--;
           },
           reset(event: any, i: number) {
               this.list[i] = this.oldItem;
               this.isEdit = true;
               this.isEditenabled = true;
           },
           AddNewRow(event: any) {
               this.addRow = true;
               this.count = (this.list) ? this.list.length + 1 : 1;
               this.addNewRow = {
                   //CSID: 1,
                   StrategicActions: "",
                   Year: this.currentYear, //this.currentYear,// need to do dynamic
                   SortOrder: 0,
                   CreatedBy: this.userName,//need to pass user login data
                   ModifiedBy: this.userName,
                   CreatedDate: this.currentDate,
                   ModifiedDate: this.currentDate,
                   BRID: this.bpmReferenceId
               }
           },
           // API for Add
           addNewItem(event: any) {
               this.spinner.show();
               //  this.addNewRow.CSID = this.count; // temp auto increment it should be implemented at back end
               this.bpmComponentService.addIntegratedTopFiveItem(this.addNewRow as IntegratedTopFive).subscribe((data: IntegratedTopFive) => {
                   console.log(data);
                   setTimeout(() => {
                       this.listAllData();
                       this.spinner.hide();
                       this.alertType = 'success';
                       this.successMessage = 'Item Added Successfully.';
                       this.hideErrorMessage();
                   }, 1000);

                   this.addRow = false;
                   this.addNewRow.StrategicActions = "";
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           },
           deleteItem(event: any, item: any) {
               this.spinner.show();
               this.bpmComponentService.deleteFocusAreasItem(item.itfid).subscribe((data: any) => {
                   setTimeout(() => {
                       this.listAllData();
                       this.spinner.hide();
                       this.alertType = 'success';
                       this.successMessage = 'Item Deleted Successfully.';
                       this.hideErrorMessage();
                   }, 500);
               }, (err: any) => {
                       this.spinner.hide();
                   console.log(err);
                   this.alertType = 'danger';
                   this.successMessage = err.error.Message;
                   this.hideErrorMessage();
               });
           }
       }

   }

  
   public DownloadFile() {
       this.spinner.show();
   //const pdfDownload = document.createElement("a");
   //document.body.appendChild(pdfDownload);
   //    const fileName = this.currentYear + "TopPrioritySheet.pdf";
   //    this.bpmComponentService.exportToPDF(this.bpmReferenceId, this.currentYear).subscribe(result => {
   //        this.spinner.hide();
   //        console.log("fgh",result);
   //    var fileBlob = new Blob([result], { type: 'application/pdf' });
   //    if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   //        window.navigator.msSaveBlob(fileBlob, fileName);
   //    }
   //    else {
   //        var url = window.URL.createObjectURL(fileBlob);
   //        pdfDownload.href = url;
   //        pdfDownload.download = fileName;
   //        pdfDownload.click();
   //    }

   //    }, (err: any) => {
   //            this.spinner.hide();
   //        console.log(err);
   //        //this.alertType = 'danger';
   //        //this.successMessage = err.error.Message;
   //        //this.hideErrorMessage();
   //    });


       var fileName = this.currentYear + "TopPrioritySheet.xlsx";
       var excelDownload = document.createElement("a");
       document.body.appendChild(excelDownload);
       this.spinner.show();
       this.bpmComponentService.exportExcel(this.bpmReferenceId).subscribe((result: any) => {
           this.spinner.hide();
           //console.log(result);
           //var fileBlob = new Blob([result], { type: 'application/xlsx' });
           var fileBlob = new Blob([result], { type: "vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
           if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
               window.navigator.msSaveBlob(fileBlob, fileName);
           }
           else {
               var url = window.URL.createObjectURL(fileBlob);
               excelDownload.href = url;
               excelDownload.download = fileName;
               excelDownload.click();
               window.URL.revokeObjectURL(url);
           }
       },
           (err: any) => {
               //console.log(err);
               this.spinner.hide();
              
           });

   } 

   toggle(acc: NgbAccordion) {
       let text = "Expand All";
       if (acc.activeIds.length) {
           acc.activeIds = [];
           text = "Expand All";
       }
       else {
           acc.activeIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map(i => `ngb-panel-${i}`);
           text = "Collapse All";
       }

       document.getElementsByClassName('expand-collapse-btn')[0].innerHTML = text;
   }

}
