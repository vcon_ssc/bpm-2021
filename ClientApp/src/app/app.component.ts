import { Component } from '@angular/core';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedYear: string;
  landingPage: boolean;
  name = 'Business Planning Meeting';
  title = 'Business Planning Meeting';
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.selectedYear = message);
    this.data.currentMessage1.subscribe(message1 => this.landingPage = message1);
  }
}
