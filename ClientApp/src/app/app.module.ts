import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccordionMenuListComponent } from './accordion-menu-list/accordion-menu-list.component';
import { NextYearAccordianMenuListComponent } from './next-year-accordian-menu-list/next-year-accordian-menu-list.component';
import { AccordionViewListComponent } from './accordion-view-list/accordion-view-list.component';
import { NewYearAccordionViewListComponent } from './new-year-accordion-view-list/new-year-accordion-view-list.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { EditableTableComponent } from './editable-table/editable-table.component';
import { CategoryPipe } from './category.pipe';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SeeMySheetLandingPageComponent } from './see-my-sheet-landing-page/see-my-sheet-landing-page.component';
import { GroupByPipe } from './group-by.pipe';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MyDatePickerModule } from 'mydatepicker'; 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    DashboardComponent,
    AccordionMenuListComponent,
    NextYearAccordianMenuListComponent,
    AccordionViewListComponent,
    NewYearAccordionViewListComponent,
    LandingPageComponent,
    EditableTableComponent,
    CategoryPipe,
    PageNotFoundComponent,
    SeeMySheetLandingPageComponent,
    GroupByPipe,
    DashboardViewComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FormsModule,ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgxSpinnerModule,
    NgbModule, NgMultiSelectDropDownModule,
    MyDatePickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
