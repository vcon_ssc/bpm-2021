import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionViewListComponent } from './accordion-view-list.component';

describe('AccordionViewListComponent', () => {
  let component: AccordionViewListComponent;
  let fixture: ComponentFixture<AccordionViewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccordionViewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionViewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
