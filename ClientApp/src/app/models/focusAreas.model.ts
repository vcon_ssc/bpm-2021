﻿export interface FocusAreas {
    FAID: number;
    StrategicActions: string;
    IsImpact: boolean;
    RequireHelp: boolean;
    OngoingEffort: boolean;
    IncrementalCosts: string;
    Year: string;
    SortOrder: number;
    CreatedBy: string;
    ModifiedBy: string;
    CreatedDate: Date;
    ModifiedDate: Date;
    BRID: number;
    FocusAreaRequireGroups: any;
    FocusAreaSupportITFs: any;
} 

