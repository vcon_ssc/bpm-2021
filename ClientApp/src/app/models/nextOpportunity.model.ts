export interface NextOpportunity {
    NOID: number,
    NextOpportunities: string,
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number,
}
