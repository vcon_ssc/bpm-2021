﻿export interface CapitalExpenses {
    CEID: number,
    Description: string,
    Amount: string;
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number
} 