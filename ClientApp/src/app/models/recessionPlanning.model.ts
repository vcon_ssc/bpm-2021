﻿export interface RecessionPlanning {
    CSRPID: number;
    Category: string;
    Description: string;
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number
} 