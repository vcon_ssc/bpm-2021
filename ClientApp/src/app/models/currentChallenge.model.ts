export interface CurrentChallenge {
    CCID: number,
    CurrentChallenges: string,
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number
}