export interface BusinessUnit{
    BUID:number,
    BUName: string,
    RegionID: number
}