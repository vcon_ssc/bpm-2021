import { BusinessUnit } from "./businessUnit.model";
import {CoreMarket} from "./CoreMarket.model";
import {CurrentChallenge} from "./currentChallenge.model";
import {CurrentOpportunity} from "./currentOpportunity.model";
import {CurrentSuccess} from "./currentSuccess.model";
import {Individual} from "./individual.model";
import {MissedOpportunity} from "./missedOpportunity.model";
import {NationalService} from "./nationalService.model";
import {NextBigIdea} from "./NextBigIdea.model";
import {NextChallenge} from "./nextChallenge.model";
import {NextOpportunity} from "./nextOpportunity.model";
import {Region} from "./region.model";
import {SSNeed} from "./SSNeed.model";
import {StrategicPartner} from "./strategicPartner.model";
import {UtilizedOpportunity} from "./utilizedOpportunity.model";
export interface BpmReference{
    BRID: number,
    Year: string,
    RegionID: number,
    RegionName: string,
    GroupName: string,
    BUID: number,
    BUName: string,
    SPID: number,
    SPName:string,
    NSID: number,
    NSName:string,
    CMID: number,
    CMName:string,
	BusinessUnits: BusinessUnit[],
	CoreMarkets: CoreMarket[],
	CurrentChallenges:CurrentChallenge[],
	CurrentOpportunities:CurrentOpportunity[],
	CurrentSuccesses:CurrentSuccess[],
	Individuals:Individual[],
	MissedOpportunity:MissedOpportunity[],
	NationalServices:NationalService[],
	NextBigIdeas:NextBigIdea[],
	NextChallenges:NextChallenge[],
	NextOpportunity:NextOpportunity[],
	Regions:Region[],
	SSNeeds:SSNeed[],
	StrategicPartners:StrategicPartner[],
	UtilizedOpportunity:UtilizedOpportunity[]
    


}



