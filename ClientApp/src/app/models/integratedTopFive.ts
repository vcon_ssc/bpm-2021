﻿export interface IntegratedTopFive {
    ITFID: number,
    StrategicActions: string,
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number
} 