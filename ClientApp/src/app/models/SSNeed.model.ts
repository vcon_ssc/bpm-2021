export interface SSNeed {
    SSNID: number,
    Role: string,
    Quantity: number,
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number

}
