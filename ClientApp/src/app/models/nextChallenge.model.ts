export interface NextChallenge {
    NCID: number,
    NextChallenges: string,
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number,
}
