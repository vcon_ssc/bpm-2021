export interface Region {
    RegionID: number,
    RegionName: string,
    GroupName: string
}
