﻿export interface LeaseModifications {
    LMID: number,
    BRID: number,
    Description: string,
    Date: Date,
    PlanOfRenewal: string;
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
   
} 