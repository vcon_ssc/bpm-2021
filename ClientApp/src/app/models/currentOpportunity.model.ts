﻿export interface CurrentOpportunity {
    COID: number,
    CurrentOpportunities: string,
    Year: string,
    SortOrder : number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number
} 