export interface UtilizedOpportunity {
    UOID:number,
    UtilizedOpportunity1: string,
    Year: string,
    SortOrder : number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number,
}