export interface Individual {
    IndID: number,
    IndividualName: string,
    CurrentRole: string,
    NextRole: string,
    IsAvailable: boolean,
    Sales: string,
    Ops: string,
    People: string,


    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number,
}