export interface CurrentSuccess {
    CSID: number,
    CurrentSuccess1: string,
    Year: string,
    SortOrder: number,
    CreatedBy: string,
    ModifiedBy: string,
    CreatedDate: Date,
    ModifiedDate: Date,
    BRID: number,
}
