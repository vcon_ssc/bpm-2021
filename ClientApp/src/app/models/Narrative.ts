﻿export interface Narrative {
    NID: number;
    Description: string;
    Year: string;
    SortOrder: number;
    CreatedBy: string;
    ModifiedBy: string;
    CreatedDate: Date;
    ModifiedDate: Date;
    BRID: number;
} 