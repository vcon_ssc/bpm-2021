﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    [Authorize]
    public class UtilizedOpportunityController : ControllerBase
    {
        private IGenericRepository<UtilizedOpportunity> _repository;
        public UtilizedOpportunityController()
        {
            this._repository = new BPMRepository<UtilizedOpportunity>(new bpmdbdevContext());
        }

        public UtilizedOpportunityController(IGenericRepository<UtilizedOpportunity> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<UtilizedOpportunity>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in UtilizedOpportunity Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public UtilizedOpportunity GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in UtilizedOpportunity GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/UtilizedOpportunity/GetByYear/{year}")]
        public async Task<List<UtilizedOpportunity>> GetByYear(string year)
        {
            try
            {
                List<UtilizedOpportunity> yearList = new List<UtilizedOpportunity>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in UtilizedOpportunity GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/UtilizedOpportunity/GetByBRId/{BRId}")]
        public async Task<List<UtilizedOpportunity>> GetByBRId(int BRId)
        {
            try
            {
                List<UtilizedOpportunity> brList = new List<UtilizedOpportunity>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in UtilizedOpportunity GetByBRId Get method :", ex);
            }

        }


        // POST api/<controller>
        public void Post(UtilizedOpportunity collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in UtilizedOpportunity Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, UtilizedOpportunity collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in UtilizedOpportunity put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in UtilizedOpportunity Controller Delete method:", e);
            }
        }
    }
}