﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    [Authorize]
    public class SsneedsController : ControllerBase
    {
        private IGenericRepository<Ssneed> _repository;
        public SsneedsController()
        {
            this._repository = new BPMRepository<Ssneed>(new bpmdbdevContext());
        }

        public SsneedsController(IGenericRepository<Ssneed> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<Ssneed>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Ssneeds Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public Ssneed GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Ssneeds GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/Ssneed/GetByYear/{year}")]
        public async Task<List<Ssneed>> GetByYear(string year)
        {
            try
            {
                List<Ssneed> yearList = new List<Ssneed>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Ssneeds GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/Ssneeds/GetByBRId/{BRId}")]
        public async Task<List<Ssneed>> GetByBRId(int BRId)
        {
            try
            {
                List<Ssneed> brList = new List<Ssneed>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Ssneed GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(Ssneed collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Ssneeds Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
       // [HttpPost]
        public void put(int id, Ssneed collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Ssneeds put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Ssneeds Controller Delete method:", e);
            }
        }
    }
}