using System;
using System.Collections.Generic;
using System.Net.Http;
using iTextSharp.text;
using iTextSharp.text.pdf;
using BPM.Models;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

namespace BPM.Controllers.API
{
    public class PDFController : ControllerBase
    {
        private IElement chunk;
        private Chunk font;
        private object string1;
        private float pageWidth;
        private bool wrapText;
        private HttpResponseMessage httpResponseMessage;
        private object httpRequestMessage;
        private int csRowNo = 0;
        private int otfsRowNo = 0;
        private int goRowNo = 0;
        private int irnsRowNo = 0;
        private int ssnRowNo = 0;
        private readonly IHostEnvironment _env;
        public PDFController(IHostEnvironment env)
        {
            _env = env;
        }

        //api/PDF/CreateAllYearPDFAsync/
        //[System.Web.Http.HttpGet]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //[System.Web.Http.Route("api/PDF/CreateAllYearPDFAsync/{YEAR}")]
        //public async Task CreateYearPDFAsync(int YEAR)
        //{
        //    HttpResponse Response = HttpContext.Response;
        //    Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
        //    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.Body);

        //    string pdfFilePath = Path.Combine(_env.ContentRootPath, ".") + "/PDFFiles";


        //    pdfDoc.Open();

        //    BPMReferenceController bpm = new BPMReferenceController();
        //    List <Bpmreference> bpmRef = await bpm.GetByYear(YEAR.ToString());
        //    int i = 0;
        //    foreach (var item in bpmRef)
        //    {
        //        int yearBRID = item.Brid;
        //        await createPDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
        //        i++;
        //        pdfDoc.NewPage();
        //    }

        //    //await 
        //    pdfWriter.CloseStream = false;
        //    pdfDoc.Close();



        //    Response.Buffer = true;
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=2019 Top 5 by xxx Group.pdf");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Write(pdfDoc);
        //    Response.End();
        //}


        //[System.Web.Http.HttpGet]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //[System.Web.Http.Route("api/PDF/CreatePDFAsync/{BRID}")]
        //public async Task CreatePDFAsync(int BRID)
        //{

        //    HttpResponse Response = HttpContext.Response;
        //    Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
        //    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.Body);

        //    string pdfFilePath = HttpContext.Server.MapPath(".") + "/PDFFiles";


        //    pdfDoc.Open();

        //    await CreateNewWorkbook();//createPDFContentFromBRID(BRID, pdfDoc, pdfWriter);
        //    pdfWriter.CloseStream = false;
        //    pdfDoc.Close();



        //  //  Response.Buffer = true;
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=2019 Top 5 by xxx Group.pdf");
        //    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Write(pdfDoc);
        //    Response.End();
        //    //return;
        //}

        public async Task CreateNewWorkbook()
        {
            BPMReferenceController bpm = new BPMReferenceController();
            List<Bpmreference> bpmRef = await bpm.GetByYear("2019");
            
            int i = 0;
            foreach (var item in bpmRef)
            {
                int yearBRID = item.Brid;
                i++;
                await createExcelForEachGroup(yearBRID);//createPDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
            }


        }

        private async Task createExcelForEachGroup(int BRID)
        {
            Excel.Application app = null;
            Excel.Application csApp = null;
            Excel.Application otfsApp = null;
            Excel.Application goApp = null;
            Excel.Application irnsApp = null;
            Excel.Application ssnApp = null;

            Excel.Workbook book = null;
            Excel.Workbook csBook = null;
            Excel.Workbook otfsBook = null;
            Excel.Workbook goBook = null;
            Excel.Workbook irnsBook = null;
            Excel.Workbook ssnBook = null;

            Excel.Worksheet sheet = null;
            Excel.Worksheet sheetCS = null;
            Excel.Worksheet sheetOTFS = null;
            Excel.Worksheet sheetGO = null;
            Excel.Worksheet sheetIRNS = null;
            Excel.Worksheet sheetSSN = null;
            
            try
            {

                string startPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                string filePath = System.IO.Path.Combine(startPath, "sal1011forms.xlsx");
                //Microsoft.Win32.SaveFileDialog sfd = new Microsoft.Win32.SaveFileDialog();

                app = new Excel.Application();
                csApp = new Excel.Application();
                otfsApp = new Excel.Application();
                goApp = new Excel.Application();
                irnsApp = new Excel.Application();
                ssnApp = new Excel.Application();

                book = app.Workbooks.Open(filePath);
                sheet = (Excel.Worksheet)book.Worksheets.get_Item(3);

                csBook = csApp.Workbooks.Open("C:\\Users\\jagrutiv\\Documents\\CurrentSuccess2018.xlsx");
                sheetCS = (Excel.Worksheet)csBook.Worksheets.get_Item(1);

                otfsBook = otfsApp.Workbooks.Open("C:\\Users\\jagrutiv\\Documents\\OppToFinishStrong2018.xlsx");
                sheetOTFS = (Excel.Worksheet)otfsBook.Worksheets.get_Item(1);

                goBook = goApp.Workbooks.Open("C:\\Users\\jagrutiv\\Documents\\GoalsAndOpp.xlsx");
                sheetGO = (Excel.Worksheet)goBook.Worksheets.get_Item(1);

                irnsBook = irnsApp.Workbooks.Open("C:\\Users\\jagrutiv\\Documents\\IndividualReadyForNextOpp.xlsx");
                sheetIRNS = (Excel.Worksheet)irnsBook.Worksheets.get_Item(1);

                ssnBook = ssnApp.Workbooks.Open("C:\\Users\\jagrutiv\\Documents\\StrategicStaffingNeeds.xlsx");
                sheetSSN = (Excel.Worksheet)ssnBook.Worksheets.get_Item(1);

                //Copies sheet and puts the copy into a new workbook
                sheet.Copy(Type.Missing, Type.Missing);
                /**if(csRowNo == 0)
                {
                    sheetCS.Copy(Type.Missing, Type.Missing);
                    sheetCS = app.Workbooks[2].Sheets[1];
                    sheetCS.Name = "Current Success";
                }*/


                //sets the sheet variable to the copied sheet in the new workbook
                sheet = (Excel.Worksheet)app.Workbooks[2].Sheets[1];
                //sfd.AddExtension = true;

                RegionController rc = new RegionController();
                var dprGroupName = await rc.GetRegionNameByBRID(BRID);

                string FileName = "BPM_" + dprGroupName[0].RegionName + ".xlsx";
                //string InitialDirectory = "C:/Saurabhs/Projects/BPM/Dump";//Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                //Name of the Group
                sheet.Name = dprGroupName[0].RegionName;

                sheet.Cells[5, 3] = dprGroupName;

                CurrentSuccessController cs = new CurrentSuccessController();
                List<CurrentSuccess> csc = await cs.GetByBRId(BRID);
                //Current Success
                int i;
                for (i = 0; i < csc.Count; i++)
                {

                    sheet.Cells[10 + i, 3] = csc[i].CurrentSuccesses;
                    sheetCS.Cells[10 + csRowNo + i, 2] = csRowNo + 1;
                    sheetCS.Cells[10 + csRowNo + i, 3] = csc[i].CurrentSuccesses;
                    sheetCS.Cells[10 + csRowNo + i, 4] = dprGroupName;
                }
                csRowNo = csRowNo + csc.Count;

                //OPPORTUNITIES TO FINISH STRONG
                NextOpportunitiesController no = new NextOpportunitiesController();
                List<NextOpportunity> noc = await no.GetByBRId(BRID);
                for (i = 0; i < noc.Count; i++)
                {
                    sheet.Cells[17 + i, 3] = noc[i].NextOpportunities;
                    sheetOTFS.Cells[10 + otfsRowNo + i, 2] = otfsRowNo + 1;
                    sheetOTFS.Cells[10 + otfsRowNo + i, 3] = noc[i].NextOpportunities;
                    sheetOTFS.Cells[10 + otfsRowNo + i, 4] = dprGroupName;
                }
                otfsRowNo = otfsRowNo + noc.Count;

                // GOALS / OPPORTUNITIES
                CurrentOpportunitiesController co = new CurrentOpportunitiesController();
                List<CurrentOpportunity> coc = await co.GetByBRId(BRID);
                for (i = 0; i < coc.Count; i++)
                {
                    sheet.Cells[24 + i, 3] = coc[i].CurrentOpportunities;

                    sheetGO.Cells[10 + goRowNo + i, 2] = goRowNo + 1;
                    sheetGO.Cells[10 + goRowNo + i, 3] = coc[i].CurrentOpportunities;
                    sheetGO.Cells[10 + goRowNo + i, 4] = dprGroupName;
                }
                goRowNo = goRowNo + coc.Count;

                //INDIVIDUALS READY FOR THE NEXT OPPORTUNITY 
                IndividualController individualController = new IndividualController();
                List<Individual> ic = await individualController.GetByBRId(BRID);
                for (i = 0 ;  i < ic.Count; i++)
                {
                    var icItem = ic[i];
                    string instanceName = icItem.IndividualName;
                    string instanceCurrentRole = icItem.CurrentRole;
                    string instanceNextBestOpp = icItem.NextRole;
                    bool instanceIsAvaialble = false;
                    string instanceAvailability = "No";
                    instanceIsAvaialble = icItem.IsAvailable;
                    if (instanceIsAvaialble)
                    {
                        instanceAvailability = "Yes";
                    }
                    string instanceGetWork = icItem.Sales;
                    string instanceDoWork = icItem.Ops;
                    string instanceForPeople = icItem.People;

                    sheet.Cells[32 + i, 3] = instanceName;
                    sheet.Cells[32 + i, 4] = instanceCurrentRole;
                    sheet.Cells[32 + i, 5] = instanceNextBestOpp;
                    sheet.Cells[32 + i, 6] = instanceAvailability;
                    sheet.Cells[32 + i, 7] = instanceGetWork;
                    sheet.Cells[32 + i, 8] = instanceDoWork;
                    sheet.Cells[32 + i, 9] = instanceForPeople;

                    sheetIRNS.Cells[10 + irnsRowNo + i, 2] = irnsRowNo + 1;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 3] = instanceName;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 4] = instanceCurrentRole;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 5] = instanceNextBestOpp;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 6] = instanceAvailability;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 7] = instanceGetWork;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 8] = instanceDoWork;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 9] = instanceForPeople;
                    sheetIRNS.Cells[10 + irnsRowNo + i, 10] = dprGroupName;
                    
                }
                irnsRowNo = irnsRowNo + ic.Count;

                //STRATEGIC STAFFING NEEDS
                SsneedsController sn = new SsneedsController();
                List<Ssneed> snc = await sn.GetByBRId(BRID);
                for (i = 0; i < snc.Count; i++)
                {
                    sheet.Cells[39 + i, 3] = snc[i].Role;
                    sheet.Cells[39 + i, 4] = snc[i].Quantity;

                    sheetSSN.Cells[10 + ssnRowNo + i, 2] = ssnRowNo + 1;
                    sheetSSN.Cells[10 + ssnRowNo + i, 3] = snc[i].Role;
                    sheetSSN.Cells[10 + ssnRowNo + i, 4] = snc[i].Quantity;
                    sheetSSN.Cells[10 + ssnRowNo + i, 5] = dprGroupName;
                }
                ssnRowNo = ssnRowNo + snc.Count;

                //sheet.SaveAs(FileName); //TODO : Commenting for combined sheet. Need to uncomment for groupwise data
                sheetCS.SaveAs("CurrentSuccess2018.xlsx");
                sheetOTFS.SaveAs("OppToFinishStrong2018.xlsx");
                sheetGO.SaveAs("GoalsAndOpp.xlsx");
                sheetIRNS.SaveAs("IndividualReadyForNextOpp.xlsx");
                sheetSSN.SaveAs("StrategicStaffingNeeds.xlsx");

                //csBook = csApp.Workbooks.Open("C:\\Users\\saurabhsa\\Documents\\CurrentSuccess2018.xlsx");


            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                    csBook.Close();
                    goBook.Close();
                    irnsBook.Close();
                    otfsBook.Close();
                    ssnBook.Close();
                }
                if (app != null)
                {
                    app.Quit();
                    csApp.Quit();
                    goApp.Quit();
                    irnsApp.Quit();
                    otfsApp.Quit();
                    ssnApp.Quit();
                }
                //this.ReleaseObject(sheet);
                //this.ReleaseObject(book);
                //this.ReleaseObject(app);
            }
        }

        private async Task createPDFContentFromBRID(int BRID, Document pdfDoc, PdfWriter pdfWriter)
        {
            Chunk chunk;
            Paragraph line;

            BPMReferenceController bpm = new BPMReferenceController();
            Bpmreference bpmRef = bpm.GetById(BRID);
            string currentSuccessYear = bpmRef.Year;

            CurrentSuccessController cs = new CurrentSuccessController();
            List<CurrentSuccess> csc = await cs.GetByBRId(BRID);
            //string currentSuccessYear = csc[0].Year;
            int currentYear = 2019;

            Int32.TryParse(currentSuccessYear, out currentYear);
            int CurrentYearMinusOne = currentYear - 1;

            drawHeader(pdfDoc, out chunk, out line, currentSuccessYear, BRID);

            PdfContentByte cb = pdfWriter.DirectContent;
            BaseFont bf1 = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1257, BaseFont.NOT_EMBEDDED);
            
            cb.SaveState();
            cb.BeginText();
            cb.MoveText(15, pdfDoc.Top - 5);
            cb.SetFontAndSize(bf1, 20);
            cb.ShowText(" Business Planning Meeting Report For year : "+currentSuccessYear );
            cb.EndText();
            cb.RestoreState();

            cb.SetLineWidth(1.0f);   // Make a bit thicker than 1.0 default
            cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
            cb.MoveTo(0, pdfDoc.Top - 10);
            cb.LineTo(650, pdfDoc.Top - 10);
            cb.Stroke();
            line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 0F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
            pdfDoc.Add(line);

            //pdfDoc.Add(pr);
            RegionController rc = new RegionController();
            var dprGroupName = await rc.GetRegionNameByBRID(BRID);
            //Paragraph pr = new Paragraph();
            chunk = new Chunk(" Group : ", FontFactory.GetFont("Arial", 16, Font.BOLD, BaseColor.BLACK));

            pdfDoc.Add(chunk);
            chunk = new Chunk(dprGroupName[0].RegionName, FontFactory.GetFont("Arial", 16, Font.NORMAL, BaseColor.BLACK));
            //pr.Add(chunk);
            pdfDoc.Add(chunk);
            cb = pdfWriter.DirectContent;
            cb.SetLineWidth(1.0f);
            cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
            cb.MoveTo(0, pdfDoc.Top - 42);
            cb.LineTo(650, pdfDoc.Top - 42);
            cb.Stroke();
            //line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
            //pdfDoc.Add(line);

            //number of characters per line in pdf
            BaseFont bf = BaseFont.CreateFont(
            BaseFont.COURIER, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
            float charWidth = bf.GetWidth(" ");
            int charactersPerLine = 100;
            // float pageWidth = pdfDoc.Right() - pdfDoc.Left();
            float fontSize = (1000 * (pageWidth / (charWidth * charactersPerLine)));
            fontSize = ((int)(fontSize * 100)) / 100f;
            Font font = new Font(bf, fontSize);


            PdfPCell cell = new PdfPCell();
            cell.Border = 0;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            int noOfCells = 5;
            cell.Colspan = noOfCells;

            line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));

            List<string> dataValue = new List<string>();

            for (int xx = 0; xx < csc.Count; xx++)
            {
                dataValue.Add(csc[xx].CurrentSuccesses);
            }

            String tableHeaderValue = CurrentYearMinusOne + " SUCCESSES";

            int left = -10;
            int top = -60;
            PdfPTable table = null;
            int i;
            float[] sglTblHdWidths;
            drawTableList(pdfDoc, pdfWriter, out cell, dataValue, tableHeaderValue, left, top, out table, out sglTblHdWidths, out i, 1);


            //2019  goals/Opportunities
            NextOpportunitiesController no = new NextOpportunitiesController();
            List<NextOpportunity> noc = await no.GetByBRId(BRID);


            List<string> dataValue01 = new List<string>();
            //NextOpportunitiesController noo = new NextOpportunitiesController();
            //List<NextOpportunity> nocc = await noo.GetByBRId(BRID);
            for (int zz = 0; zz < noc.Count; zz++)
            {
                dataValue01.Add(noc[zz].NextOpportunities);
            }
            tableHeaderValue = CurrentYearMinusOne + " OPPORTUNITIES TO FINISH STRONG";
            left = 180;
            top = -60;
            drawTableList(pdfDoc, pdfWriter, out cell, dataValue01, tableHeaderValue, left, top, out table, out sglTblHdWidths, out i, 1);

            List<string> dataValue2 = new List<string>();
            CurrentOpportunitiesController co = new CurrentOpportunitiesController();
            List<CurrentOpportunity> coc = await co.GetByBRId(BRID);

            for (int yy = 0; yy < coc.Count; yy++)
            {
                dataValue2.Add(coc[yy].CurrentOpportunities);
            }
            //dataValue2.Add(NextOpportunity); dataValue2.Add(NextOpportunity); dataValue2.Add(NextOpportunity); dataValue2.Add(NextOpportunity); dataValue2.Add(NextOpportunity);
            tableHeaderValue = currentYear + " GOALS / OPPORTUNITIES";
            left = 370;
            top = -60;
            drawTableList(pdfDoc, pdfWriter, out cell, dataValue2, tableHeaderValue, left, top, out table, out sglTblHdWidths, out i, 1);



            //Individual ready for next opportunity
            table = new PdfPTable(8);
            table.WidthPercentage = 100;
            table.DefaultCell.FixedHeight = 35f;

            sglTblHdWidths = new float[8];
            sglTblHdWidths[0] = 120f;
            sglTblHdWidths[1] = 820f;
            sglTblHdWidths[2] = 820f;
            sglTblHdWidths[3] = 820f;
            sglTblHdWidths[4] = 400f;
            sglTblHdWidths[5] = 280f;
            sglTblHdWidths[6] = 280f;
            sglTblHdWidths[7] = 280f;
            table.SetWidths(sglTblHdWidths);
            table.TotalWidth = 570f;
            table.LockedWidth = true;
            cell = new PdfPCell(new Phrase(" INDIVIDUALS READY FOR THE NEXT OPPORTUNITY", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = new BaseColor(0, 90, 155);
            cell.Colspan = 8;
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Name", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Current Role", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase("Next Best Opportunity ", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Availability", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase("Right Who", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
            cell.Colspan = 3;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            table.AddCell(cell);
            table.AddCell(new Phrase("Get Work", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.BLACK)));
            table.AddCell(new Phrase(" Do Work", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.BLACK)));
            table.AddCell(new Phrase("Take Care Of People", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.BLACK)));

            //Individuals Ready For Next Opportunity
            IndividualController individualController = new IndividualController();
            List<Individual> ic = await individualController.GetByBRId(BRID);

            List<List<string>> gridData = new List<List<string>>();


            foreach (var icItem in ic)
            {
                string instanceName = icItem.IndividualName;
                string instanceCurrentRole = icItem.CurrentRole;
                string instanceNextBestOpp = icItem.NextRole;
                bool instanceIsAvaialble = false;
                string instanceAvailability = "No";
                instanceIsAvaialble = icItem.IsAvailable;
                if (instanceIsAvaialble)
                {
                    instanceAvailability = "Yes";
                }
                string instanceGetWork = icItem.Sales;
                string instanceDoWork = icItem.Ops;
                string instanceForPeople = icItem.People;
                dataValue = new List<string>();
                dataValue.Add(instanceName);
                dataValue.Add(instanceCurrentRole);
                dataValue.Add(instanceNextBestOpp);
                dataValue.Add(instanceAvailability);



                dataValue.Add(instanceGetWork);
                dataValue.Add(instanceDoWork);
                dataValue.Add(instanceForPeople);
                //dataValue.Add(" ");
                gridData.Add(dataValue);
            }


            Console.WriteLine();

            //gridData.Add(dataValue);
            int count = gridData.Count;
            if (count < 5)
            {
                int addEmptyRows = 5 - count;
                for (int y = 0; y < addEmptyRows; y++)
                {
                    //int sno = count + y + 1;
                    List<string> emptyList = new List<string>(8);
                    //emptyList.Add(sno.ToString());
                    for (int z = 0; z < 7; z++)
                    {
                        emptyList.Add(" ");
                    }
                    gridData.Add(emptyList);
                }
            }

            int x = 1;
            foreach (var gridItemList in gridData)
            {

                table.AddCell(x.ToString());
                x++;

                foreach (var item in gridItemList)
                {

                    cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));
                    //table.AddCell(pp);
                    if (item.Equals("Red"))
                    {
                        //BaseColor color = new BaseColor(red, green, blue);
                        // BaseColor color = new BaseColor(red, green, blue);
                        //cell.BackgroundColor(new BaseColor(226, 226, 226));
                        //cell.BackgroundColor(iTextSharp.text.html.WebColors.GetRGBColor("#E2E2E2"));
                        cell = new PdfPCell(new Phrase("")); cell.BackgroundColor = BaseColor.RED;

                    }
                    else if (item.Equals("Green"))
                    {
                        cell = new PdfPCell(new Phrase(""));
                        cell.BackgroundColor = BaseColor.GREEN;
                    }
                    else if (item.Equals("Yellow"))
                    {
                        cell = new PdfPCell(new Phrase("")); cell.BackgroundColor = BaseColor.YELLOW;
                    }
                    table.AddCell(cell);

                }
            }
            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 450, pdfWriter.DirectContent);


            //Strategic Needs
            SsneedsController sn = new SsneedsController();
            List<Ssneed> snc = await sn.GetByBRId(BRID);

            Console.WriteLine();

            List<string> dataValue4 = new List<string>();
            for (int dd = 0; dd < snc.Count; dd++)
            {
                dataValue4.Add(snc[dd].Role + ":" + snc[dd].Quantity);
            }
            //dataValue3.Add(StrategicNeeds); dataValue3.Add(StrategicNeeds); dataValue3.Add(StrategicNeeds); dataValue3.Add(StrategicNeeds); dataValue3.Add(StrategicNeeds);
            table = new PdfPTable(5);
            tableHeaderValue = " STRATEGIC STAFFING NEEDS";
            table.TotalWidth = 570f;
            left = -10;
            top = -750;
            //int i;
            //drawTableList(pdfDoc, pdfWriter, out cell, dataValue4, tableHeaderValue, left, top, out table, out sglTblHdWidths, out i, 1);
            //table.WriteSelectedRows(0, -1, pdfDoc.Left - left, pdfDoc.Top - top, pdfWriter.DirectContent);
            table.DefaultCell.FixedHeight = 20f;
            table.WidthPercentage = 100;
            sglTblHdWidths = new float[5];
            sglTblHdWidths[0] = 120f;
            sglTblHdWidths[1] = 120f;
            sglTblHdWidths[2] = 120f;
            sglTblHdWidths[3] = 120f;
            sglTblHdWidths[4] = 120f;
            // Set the column widths on table creation. Unlike HTML cells cannot be sized.
            //table.SetWidths(sglTblHdWidths);
            table.LockedWidth = true;
            //Header of the Table
            cell = new PdfPCell(new Phrase(tableHeaderValue, FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.CYAN;
            cell.BackgroundColor = new BaseColor(0, 90, 155);
            cell.Colspan = 5;
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);

            i = 1;
            int listCount = dataValue4.Count;
            if (listCount < 5)
            {
                for (int a = 0; a < 5 - listCount; a++)
                {
                    dataValue4.Add(" ");
                }
            }
            foreach (var item in dataValue4)
            {
                Phrase p = new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK));
                i++;
                table.AddCell(p);
            }
            table.WriteSelectedRows(0, -1, pdfDoc.Left + left, pdfDoc.Top + top, pdfWriter.DirectContent);


            table.SplitRows = false;
        }

        private static void drawTableList(Document pdfDoc, PdfWriter pdfWriter, out PdfPCell cell, List<string> dataValue, string tableHeaderValue, int left, int top, out PdfPTable table, out float[] sglTblHdWidths, out int i, int numberOfColumns)
        {
            //i = 10;
            table = new PdfPTable(numberOfColumns);
            table.DefaultCell.FixedHeight = 60f;
            table.WidthPercentage = 100;
            sglTblHdWidths = new float[1];
            sglTblHdWidths[0] = 35f;
            // Set the column widths on table creation. Unlike HTML cells cannot be sized.
            //table.SetWidths(sglTblHdWidths);

            table.TotalWidth = 185f;
            table.LockedWidth = true;
            //Header of the Table
            cell = new PdfPCell(new Phrase(tableHeaderValue, FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = new BaseColor(0, 90, 155);
            cell.Colspan = 3;
            cell.HorizontalAlignment = 1;
            cell.FixedHeight = 32f;
            table.AddCell(cell);
            i = 1;
            int listCount = dataValue.Count;
            if (listCount < 5)
            {
                for (int a = 0; a < 5 - listCount; a++)
                {
                    dataValue.Add(" ");
                }
            }
            foreach (var item in dataValue)
            {
                Phrase p = new Phrase(i + " " + item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK));
                i++;
                table.AddCell(p);
            }
            table.WriteSelectedRows(0, -1, pdfDoc.Left + left, pdfDoc.Top + top, pdfWriter.DirectContent);
        }

        private void drawHeader(Document pdfDoc, out Chunk chunk, out Paragraph line, string year, int BRID)
        {
            //BPMReferenceController br = new BPMReferenceController();
            //string year = await br.GetByYear(year);

            Paragraph pr = new Paragraph();
            chunk = new Chunk(" Business Planning Meeting Report For " + year, FontFactory.GetFont("Arial", 21, Font.BOLD, BaseColor.BLACK));
            //pdfDoc.Add(chunk);
            //Horizontal Line
            line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            Image ok = Image.GetInstance(Path.Combine(_env.ContentRootPath, "~/Content/Images/dpr logo.png"));
            ok.ScaleAbsoluteWidth(40);
            ok.ScaleAbsoluteHeight(20);
            //if(BRID == 2)
            //{
                ok.SetAbsolutePosition(pdfDoc.Right - 35, pdfDoc.Top - 5);
            //}
            //else
            //{
                //ok.SetAbsolutePosition(pdfDoc.Right - 35, pdfDoc.Top - 20);
            //}
            
            ok.SetDpi(300, 300);
            pdfDoc.Add(ok);
        }



        private PdfPCell GetCell(string text, int colSpan, int rowSpan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text));
            cell.HorizontalAlignment = 1;
            cell.Rowspan = rowSpan;
            cell.Colspan = colSpan;

            return cell;
        }


    }
}