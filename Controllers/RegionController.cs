﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
   // [Authorize]
    public class RegionController : ControllerBase
    {
        private readonly IGenericRepository<Region> _repository;

        private readonly bpmdbdevContext db;
        public RegionController()
       {
            db = new bpmdbdevContext();

            this._repository = new BPMRepository<Region>(db);
        }

        //public RegionController(IGenericRepository<Region> repository)
        //{
        //    this._repository = repository;
        //}

        [Route("api/Region/GetBpmWithRegionByYear/{year}")]
        public async Task<List<ViewRegion>> GetBpmWithRegionByYear(string year)
        {
            List<Region> yearListwithRegions = new List<Region>();
            try
            {
                yearListwithRegions = await _repository.GetModelWithEagrLoading("Bpmreferences");
                var ListwithRegions = yearListwithRegions.Where(x => x.Year == year & x.IsDeleted==false).Select(l => new ViewRegion
                {
                    BRID = l.Bpmreferences.Select(m => m.Brid).Any() ? l.Bpmreferences.Select(m => m.Brid).First() : 0,
                    RegionID = l.RegionId,
                    RegionName = l.RegionName,
                    GroupName = l.GroupName,
                    SerialNo=l.SerialNo
                }).OrderBy(m=>m.SerialNo).ToList(); 

                return ListwithRegions;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }

        [Route("api/Region/GetRegionNameByYear/{year}")]
        // GET api/<controller>
        public async Task<List<Region>> GetRegionNameByYear(string year)
        {
            try
            {
                var regionList= await _repository.GetModel();
                regionList = regionList.Where(u => u.Year == year & u.IsDeleted==false).OrderBy(c=>c.RegionId).ToList();
                
                return regionList;

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Region Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public Region GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Region GetByID method :", ex);
            }
        }

        
        //}
        // GET api/<controller>
        [Route("api/Region/GetRegionNameByBRID/{BRId}")]
        public async Task<List<Region>> GetRegionNameByBRID(int BRId)
        {
            try
            {
                BPMRepository<Bpmreference> BPMRepository = new BPMRepository<Bpmreference>(new bpmdbdevContext());
                List<Bpmreference> yearList = new List<Bpmreference>();
                yearList = await BPMRepository.GetModel();
                yearList = yearList.Where(x => x.Brid == BRId).ToList();
                int regionId = yearList.First().RegionId.Value;

                List<Region> brList = new List<Region>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.RegionId == regionId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Region GetByBRId method :", ex);
            }

        }

        
            public async Task<List<Region>> GetRegionByBRID(int BRId)
        {
            try
            {
                
                List<Region> brList = new List<Region>();
                brList = await _repository.GetModelByBPMId(BRId);
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Region GetByBRId method :", ex);
            }

        }


        [Route("api/Region/GetGroupIDByYearRegId/{year}/{RegionId}")]
        public async Task<string> GetGroupIDByYearRegId(int RegionId)
        {
            try
            {
                List<Region> brList = new List<Region>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.RegionId == RegionId ).ToList();
                return brList.First().GroupName;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Region GetByBRId method :", ex);
            }

        }
        

        [Route("api/Region/GetRegionNameByRespPer/{year}")]
        public async Task<List<Region>> GetRegionNameByRespPer(string year)
        {
            try
            {
                //(User.Identity as System.Security.Claims.ClaimsIdentity).Name.ToLower();
                String userName = "saurabhd@vconstruct.in";
                userName = userName.Trim();
                 var regionList =await _repository.GetModel();
                regionList = regionList.Where(u => (u.ResponsiblePerson.ToLower().Contains(userName) | u.Mcsponser == userName) & u.Year == year & u.IsDeleted==false).OrderBy(c=>c.RegionName).ToList();
                return regionList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Region GetByBRId method :", ex);
            }

        }


        // POST api/<controller>
        public void Post(Region collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Region Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void Put( Region collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Region put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Region Controller Delete method:", e);
            }
        }
    }
}