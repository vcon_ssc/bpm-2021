﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    [Authorize]
    public class NextBigIdeasController : ControllerBase
    {
        private IGenericRepository<NextBigIdea> _repository;
        public  NextBigIdeasController()
        {
            this._repository = new BPMRepository<NextBigIdea>(new bpmdbdevContext());
        }

        public  NextBigIdeasController(IGenericRepository<NextBigIdea> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async System.Threading.Tasks.Task<List<NextBigIdea>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextBigIdeas Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public NextBigIdea GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextBigIdeas GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/NextBigIdea/GetByYear/{year}")]
        public async Task<List<NextBigIdea>> GetByYear(string year)
        {
            try
            {
                List<NextBigIdea> yearList = new List<NextBigIdea>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextBigIdeas GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/NextBigIdea/GetByBRId/{BRId}")]
        public async Task<List<NextBigIdea>> GetByBRId(int BRId)
        {
            try
            {
                List<NextBigIdea> brList = new List<NextBigIdea>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextBigIdea GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(NextBigIdea collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextBigIdeas Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, NextBigIdea collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextBigIdeas put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextBigIdeas Controller Delete method:", e);
            }
        }
    }
}