﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    //[Authorize]
    public class CurrentChallengesController : ControllerBase
    {
        private IGenericRepository<CurrentChallenge> _repository;

        public CurrentChallengesController()
        {
            this._repository = new BPMRepository<CurrentChallenge>(new bpmdbdevContext());
        }

        public CurrentChallengesController(IGenericRepository<CurrentChallenge> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<CurrentChallenge>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentChallenges Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public CurrentChallenge GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentChallenges GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/CurrentChallenge/GetByYear/{year}")]
        public async Task<List<CurrentChallenge>> GetByYear(string year)
        {
            try
            {
                List<CurrentChallenge> yearList = new List<CurrentChallenge>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentChallenges GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/CurrentChallenge/GetByBRId/{BRId}")]
        public async Task<List<CurrentChallenge>> GetByBRId(int BRId)
        {
            try
            {
                List<CurrentChallenge> brList = new List<CurrentChallenge>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentChallenge GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(CurrentChallenge collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentChallenges Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, CurrentChallenge collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentChallenges put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentChallenges Controller Delete method:", e);
            }
        }
    }
}