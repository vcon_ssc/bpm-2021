﻿using System;
using System.Collections.Generic;
using BPM.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    public class PredefinedSupportITFController : ControllerBase
    {
        private readonly IGenericRepository<PredefinedSupportItf> _predefinedSupportITFrepository;
        public PredefinedSupportITFController()
        {
            this._predefinedSupportITFrepository = new BPMRepository<PredefinedSupportItf>(new bpmdbdevContext());
        }

        // GET api/<controller>
        [Route("api/PredefinedSupportITF/Get")]
        public async Task<List<PredefinedSupportItf>> Get()
        {
            try
            {
                var result = await _predefinedSupportITFrepository.GetModel();
                result.Select(v => new PredefinedSupportItf
                {
                    SupportItfId = v.SupportItfId,
                    Name = v.Name
                });
                return result;

            }
            catch (Exception ex)
            {

                throw new ApplicationException("Error in PredefinedSupportITF Get method :", ex);
            }

        }
    }

}