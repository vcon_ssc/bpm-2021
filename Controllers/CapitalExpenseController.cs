﻿using BPM.Data;
using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
  //  [Authorize]
    public class CapitalExpenseController : ControllerBase
    {

        private IGenericRepository<CapitalExpense> _expenserepository;
        public CapitalExpenseController()
        {
            this._expenserepository = new BPMRepository<CapitalExpense>(new bpmdbdevContext());
        }

        //public CapitalExpenseController(IGenericRepository<CapitalExpense> repository)
        //{
        //    this._expenserepository = repository;
        //}

        [Route("api/FocusAreas/GetByBRIdOnly/{BRId}")]
        public async Task<List<CapitalExpense>> GetByBRIdOnly(int BRId)
        {
            try
            {
                List<CapitalExpense> brList = new List<CapitalExpense>();
                brList = await _expenserepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in expenserepository GetByBRIdOnly method :", ex);
            }

        }

        // GET api/<controller>
        public async Task<List<CapitalExpense>> Get()
        {
            try
            {
                return await _expenserepository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in c Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public CapitalExpense GetById(int id)
        {
            try
            {
                return _expenserepository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CapitalExpense GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/CapitalExpense/GetByYear/{year}")]
        public async Task<List<CapitalExpense>> GetByYear(string year)
        {
            try
            {
                List<CapitalExpense> yearList = new List<CapitalExpense>();
                yearList = await _expenserepository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CapitalExpense GetByYear method :", ex);
            }

        }

        [Route("api/CapitalExpense/GetByBRId/{BRId}/{year}")]
        public async Task<List<CapitalExpense>> GetByBRId(int BRId, string year)
        {
            try
            {
                List<CapitalExpense> brList = new List<CapitalExpense>();
                brList = await _expenserepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId & X.Year == year).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CapitalExpense GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(CapitalExpense collection)
        {
            try
            {
                // TODO: Add insert logic here
                //collection.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.CreatedBy = "saurabhd@vconstruct.in";
                collection.ModifiedBy = collection.CreatedBy;
                collection.CreatedDate = DateTime.UtcNow;
                collection.ModifiedDate = collection.CreatedDate;


                _expenserepository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CapitalExpense Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public void put(int id, CapitalExpense collection)
        {
            try
            {
                // TODO: Add update logic here
                //collection.ModifiedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.ModifiedBy = "saurabhd@vconstruct.in";
                collection.ModifiedDate = DateTime.UtcNow;
                _expenserepository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CapitalExpense put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _expenserepository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CapitalExpense Controller Delete method:", e);
            }
        }
    }
}