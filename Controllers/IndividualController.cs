﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    //[Authorize]
    public class IndividualController : ControllerBase
    {
        private IGenericRepository<Individual> _repository;
        public IndividualController()
        {
            this._repository = new BPMRepository<Individual>(new bpmdbdevContext());
        }

        //public IndividualController(IGenericRepository<Individual> repository)
        //{
        //    this._repository = repository;
        //}

        // GET api/<controller>
        public async Task<List<Individual>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Individual Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public Individual GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Individual GetByID method :", ex);
            }
        }

        //// GET api/<controller>
        [Route("api/Individual/GetByBRId/{BRId}")]
        public async Task<List<Individual>> GetByBRId(int BRId)
        {
            try
            {
                List<Individual> brList = new List<Individual>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Individual GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(Individual collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Individual Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        //[HttpPost]
        public void put(int id, Individual collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Individual put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Individual Controller Delete method:", e);
            }
        }
    }
}