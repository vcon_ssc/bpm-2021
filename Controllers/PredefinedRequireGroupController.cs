﻿using System;
using System.Collections.Generic;
using BPM.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    public class PredefinedRequireGroupController : ControllerBase
    {
        private readonly IGenericRepository<PredefinedRequireGroup> _predefinedRequireGrouprepository;
        public PredefinedRequireGroupController()
        {
            this._predefinedRequireGrouprepository = new BPMRepository<PredefinedRequireGroup>(new bpmdbdevContext());
        }

        public PredefinedRequireGroupController(IGenericRepository<PredefinedRequireGroup> repository)
        {
            this._predefinedRequireGrouprepository = repository;
        }



        // GET api/<controller>
        [Route("api/PredefinedRequireGroup/Get")]
        public async Task<List<PredefinedRequireGroup>> Get()
        {
            try
            {
                var result = await _predefinedRequireGrouprepository.GetModel();
                result.Select(v => new PredefinedRequireGroup
                {
                    RequireGroupId = v.RequireGroupId,
                    Name = v.Name
                });
                return result;

            }
            catch (Exception ex)
            {
                
                throw new ApplicationException("Error in PredefinedRequireGroup Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public PredefinedRequireGroup GetById(int id)
        {
            try
            {
                return _predefinedRequireGrouprepository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in PredefinedRequireGroup GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/PredefinedRequireGroup/GetByYear/{year}")]
        public async Task<List<PredefinedRequireGroup>> GetByYear(string year)
        {
            try
            {
                List<PredefinedRequireGroup> yearList = new List<PredefinedRequireGroup>();
                yearList = await _predefinedRequireGrouprepository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in PredefinedRequireGroup GetByYear method :", ex);
            }

        }

       
    }
}