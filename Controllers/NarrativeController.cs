﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    //[Authorize]
    public class NarrativeController : ControllerBase
    {
        private IGenericRepository<Narrative> _narrativeRepository;
        public NarrativeController()
        {
            this._narrativeRepository = new BPMRepository<Narrative>(new bpmdbdevContext());
        }

        //public NarrativeController(IGenericRepository<Narrative> repository)
        //{
        //    this._narrativeRepository = repository;
        //}

        [Route("api/Narrative/GetByBRIdOnly/{BRId}")]
        public async Task<List<Narrative>> GetByBRIdOnly(int BRId)
        {
            try
            {
                List<Narrative> brList = new List<Narrative>();
                brList = await _narrativeRepository.GetModel();
                brList = brList.Any()? brList.Where(X => X.Brid == BRId).ToList():brList;
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Narrative GetByBRIdOnly method :", ex);
            }

        }

        // GET api/<controller>
        public async Task<List<Narrative>> Get()
        {
            try
            {
                return await _narrativeRepository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Narrative Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public Narrative GetById(int id)
        {
            try
            {
                return _narrativeRepository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Narrative GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/Narrative/GetByYear/{year}")]
        public async Task<List<Narrative>> GetByYear(string year)
        {
            try
            {
                List<Narrative> yearList = new List<Narrative>();
                yearList = await _narrativeRepository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Narrative GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/Narrative/GetByBRId/{BRId}/{year}")]
        public async Task<List<Narrative>> GetByBRId(int BRId, string year)
        {
            try
            {
                List<Narrative> brList = new List<Narrative>();

                brList = await _narrativeRepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId & X.Year == year).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Narrative GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        [HttpPost]
        [Route("api/Narrative")]
        public void Post(Narrative collection)
        {
            try
            {
                // TODO: Add insert logic here
                // (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.CreatedBy = "saurabhd@vconstruct.in";
                collection.ModifiedBy = collection.CreatedBy;
                collection.CreatedDate = DateTime.UtcNow;
                collection.ModifiedDate = collection.CreatedDate;


                _narrativeRepository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Narrative Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public void put(int id, Narrative collection)
        {
            try
            {
                // TODO: Add update logic here
                collection.ModifiedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.ModifiedDate = DateTime.UtcNow;

                _narrativeRepository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Narrative put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _narrativeRepository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in Narrative Controller Delete method:", e);
            }
        }
    }
}

