﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;
namespace BPM.Controllers.API
{
    [Authorize]
    public class StrategicPartnersController : ControllerBase
    {
        private IGenericRepository<StrategicPartner> _repository;
        public StrategicPartnersController()
        {
            this._repository = new BPMRepository<StrategicPartner>(new bpmdbdevContext());
        }

        public StrategicPartnersController(IGenericRepository<StrategicPartner> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<StrategicPartner>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in StrategicPartner Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public StrategicPartner GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in StrategicPartner GetByID method :", ex);
            }
        }

        ////// GET api/<controller>
        //[Route("api/StrategicPartner/GetByBRId/{BRId}")]
        //public async Task<List<StrategicPartner>> GetByBRId(int BRId)
        //{
        //    try
        //    {
        //        List<StrategicPartner> brList = new List<StrategicPartner>();
        //        brList = await _repository.GetModel();
        //        brList = brList.Where(X => X.BRID == BRId).ToList();
        //        return brList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Error in StrategicPartner GetByBRId  method :", ex);
        //    }

        //}

        // POST api/<controller>
        public void Post(StrategicPartner collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in StrategicPartner Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, StrategicPartner collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in StrategicPartner put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in  StrategicPartner Controller Delete method:", e);
            }
        }
    }
}