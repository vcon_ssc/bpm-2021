﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
   // [Authorize]
    public class IntegratedTopFiveController : ControllerBase
    {
        private IGenericRepository<IntegratedTopFive> _integratedrepository;
        public IntegratedTopFiveController()
        {
            this._integratedrepository = new BPMRepository<IntegratedTopFive>(new bpmdbdevContext());
        }

        //public IntegratedTopFiveController(IGenericRepository<IntegratedTopFive> repository)
        //{
        //    this._integratedrepository = repository;
        //}


        [Route("api/FocusAreas/GetByBRIdOnly/{BRId}")]
        public async Task<List<IntegratedTopFive>> GetByBRIdOnly(int BRId)
        {
            try
            {
                List<IntegratedTopFive> brList = new List<IntegratedTopFive>();
                brList = await _integratedrepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in IntegratedTopFive GetByBRIdOnly method :", ex);
            }

        }

        // GET api/<controller>
        public async Task<List<IntegratedTopFive>> Get()
        {
            try
            {
                return await _integratedrepository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in IntegratedTopFive Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public IntegratedTopFive GetById(int id)
        {
            try
            {
                return _integratedrepository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in IntegratedTopFive GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/IntegratedTopFive/GetByYear/{year}")]
        public async Task<List<IntegratedTopFive>> GetByYear(string year)
        {
            try
            {
                List<IntegratedTopFive> yearList = new List<IntegratedTopFive>();
                yearList = await _integratedrepository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in IntegratedTopFive GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/IntegratedTopFive/GetByBRId/{BRId}/{year}")]
        public async Task<List<IntegratedTopFive>> GetByBRId(int BRId, string year)
        {
            try
            {
                List<IntegratedTopFive> brList = new List<IntegratedTopFive>();
               
                brList = await _integratedrepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId & X.Year ==year).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in IntegratedTopFive GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(IntegratedTopFive collection)
        {
            try
            {
                // TODO: Add insert logic here
                collection.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.ModifiedBy = collection.CreatedBy;
                collection.CreatedDate = DateTime.UtcNow;
                collection.ModifiedDate = collection.CreatedDate;


                _integratedrepository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in IntegratedTopFive Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public void put(int id, IntegratedTopFive collection)
        {
            try
            {
                // TODO: Add update logic here
                collection.ModifiedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.ModifiedDate = DateTime.UtcNow;
                _integratedrepository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in IntegratedTopFive put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _integratedrepository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in IntegratedTopFive Controller Delete method:", e);
            }
        }
    }
}
