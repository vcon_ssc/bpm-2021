﻿using BPM.Data;
using BPM.Models;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BPM.Controllers.API
{
   // [Authorize]
    public class BPMReferenceController : ControllerBase
    {
        
        private IGenericRepository<Bpmreference> _repository;
        public BPMReferenceController()
        {
            this._repository = new BPMRepository<Bpmreference>(new bpmdbdevContext());
        }

       


        [Route("api/BPMReference/GetBpmWithRegionBySerialNo/{year}")]
        public async Task<List<ViewRegion>> GetBpmWithRegionBySerialNo(string year)
        {
            List<Bpmreference> yearListwithRegions = new List<Bpmreference>();
            try
            {
                yearListwithRegions = await _repository.GetModelWithEagrLoading("Region");
                var ListwithRegions = yearListwithRegions.Where(x => x.Year == year && x.IsDeleted == false).Select(l => new ViewRegion
                {
                    BRID = l.Brid,
                    RegionID = l.RegionId,
                    RegionName = l.Region.RegionName,
                    GroupName = l.Region.GroupName,
                    SerialNo = l.Region.SerialNo
                }).OrderBy(d => d.SerialNo);

                return ListwithRegions.ToList();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }

        public async Task<List<ViewRegion>> GetBpmWithRegion(int BRId)
        {
            List<Bpmreference> list = new List<Bpmreference>();
            try
            {
                list = await _repository.GetModelWithEagrLoading("Region");
                var ListwithRegions = list.Where(x => x.Year == "2021" && x.IsDeleted == false && x.Brid == BRId).Select(l => new ViewRegion
                {
                    BRID = l.Brid,
                    RegionID = l.RegionId,
                    RegionName = l.Region.RegionName,
                    GroupName = l.Region.GroupName,
                    Year=l.Year
                });

                return ListwithRegions.ToList();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }

        //[HttpGet]
        //[Route("api/BpmReference/CreatePDFAsync/{BRID}")]
        //public async Task CreatePDFAsync(int BRID)
        //{
        //    try
        //    {
        //        HttpResponse Response = HttpContext.Current.Response;
        //        MemoryStream ms = new MemoryStream();
        //        Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
        //        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, ms);

        //        // string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";

        //        pdfDoc.Open();
        //        await createPDFContentFromBRID(BRID, pdfDoc, pdfWriter);
        //        pdfWriter.CloseStream = false;
        //        pdfDoc.Close();
        //        Response.Buffer = true;
        //        Response.ContentType = "application/pdf";
        //        byte[] pdfBytes = ms.ToArray();
        //        Response.AppendHeader("Content-Length", pdfBytes.Length.ToString());
        //        Response.OutputStream.Write(pdfBytes, 0, (int)pdfBytes.Length);

        //        //Response.Write(pdfDoc);
        //        Response.End();
        //        return;

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}


        [HttpGet]
        //[Route("api/BpmReference/CreateMyPDFAsync/{year}")]
        //public async Task CreateMyPDFAsync(string year)
        //{
        //    HttpResponse Response = System.Web.HttpContext.Current.Response;
        //    Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
        //    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        //    //  string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";


        //    pdfDoc.Open();

        //    BPMReferenceController bpm = new BPMReferenceController();
        //    List<ViewRegion> bpmRef = await bpm.GetByResPerYear(year.ToString());
        //    int i = 0;
        //    foreach (var item in bpmRef)
        //    {
        //        int yearBRID = item.BRID;
        //        await createPDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
        //        i++;
        //        pdfDoc.NewPage();
        //    }

        //    //await 
        //    pdfWriter.CloseStream = false;
        //    pdfDoc.Close();


        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=2020 Top 5 by xxx Group.pdf");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Write(pdfDoc);
        //    Response.End();
        //}

       
        //[HttpGet]
        //[Route("api/BpmReference/CreateAllYearPDFAsync/{year}")]
        //public async Task CreateYearPDFAsync(string year)
        //{
        //    try
        //    {

        //        HttpResponse Response = HttpContext.Current.Response;
        //        Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
        //        MemoryStream ms = new MemoryStream();
        //        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, ms);

        //        //PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //        //string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";
        //        pdfDoc.Open();

        //        BPMReferenceController bpm = new BPMReferenceController();
        //        List<BPMReference> bpmRef = await bpm.GetByYear(year.ToString());
        //        int i = 0;
        //        foreach (var item in bpmRef)
        //        {
        //            int yearBRID = item.BRID;
        //            await createPDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
        //            i++;
        //            pdfDoc.NewPage();
        //        }

        //        //await 
        //        pdfWriter.CloseStream = false;
        //        pdfDoc.Close();
                
        //        Response.Buffer = true;
        //        Response.ContentType = "application/pdf";
        //        byte[] pdfBytes = ms.ToArray();
        //        Response.AppendHeader("Content-Length", pdfBytes.Length.ToString());
        //        Response.OutputStream.Write(pdfBytes, 0, (int)pdfBytes.Length);
        //        Response.End();
        //        return ;


        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}


       // [HttpGet]
       //// [AcceptVerbs("GET", "POST")]
       // [Route("api/BpmReference/CreateExcelAsyncForYear")]
        //public async Task CreateExcelAsyncForYear()
        //{

        //    try
        //    {
                
        //        string year = "2020";
                
        //        HttpResponse Response = HttpContext.Current.Response;
               
                
        //       // await CreateNewWorkbook(year);
                
        //        Response.Buffer = true;
        //        Response.ContentType = "application/vnd.ms-excel";
        //        Response.AddHeader("content-disposition", "attachment;filename=2020Top5.xlsx");
        //       // byte[] excelBytes = ms.ToArray();
        //        Response.AppendHeader("Content-Length", excelBytes.Length.ToString());
        //        Response.OutputStream.Write(excelBytes, 0, (int)excelBytes.Length);

        //       // ms.WriteTo(Response.OutputStream);

        //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //       // Response.Write(DestinationBook);
        //        Response.End();
        //        return ;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;

        //    }
        //    //finally
        //    //{

        //    //    app.Workbooks.Close();
        //    //    if (app != null)
        //    //    {
        //    //        app.Quit();
        //    //    }

        //    //}
        //}

        //public async Task CreateNewWorkbook(string year)
        //{

        //    //Excel.Application app = new Excel.Application();
        //    Excel.Application app = new Excel.Application();
        //    public MemoryStream  ms = new MemoryStream();
        //    //public Excel.Workbook DestinationBook = null;
        //    string FileName = "2020_BfghjPM" + ".xlsx";
        //    Excel.Workbook SourceBook = null;
        //    Excel.Worksheet SourceSheet = null;
        //Excel.Workbook DestinationBook = app.Workbooks.Add();
        //    Excel.Worksheet DestinationSheet = null;
        //    // CreateLog();
        //    try
        //    {
        //        RegionController region = new RegionController();
        //        List<ViewRegion> RegionList = await region.GetBpmWithRegionByYear(year);
        //        string startPath, filePath = "";
        //        int i = 1;
        //        foreach (var item in RegionList)
        //        {
        //            i++;
        //            // int yearBRID = item.BRID;

        //            string RegionName = item.RegionName;
        //            string GroupName = item.GroupName;
        //            if (RegionName.Contains("\n"))
        //            {
        //                RegionName = "Integrated" + i;
        //            }
        //            if (GroupName == "RBU")
        //            {
        //                startPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
        //                // filePath = System.IO.Path.Combine(startPath, "Group1Template.xlsx");
        //                filePath = HttpContext.Current.Server.MapPath("~/app/Resources/Group1Template.xlsx");
        //            }
        //            else if (GroupName == "CM" || GroupName == "CSG" || GroupName == "SP" || GroupName == "WVE")
        //            {
        //                if (GroupName == "CSG" && RegionName.Contains("Integrated"))
        //                {
        //                    startPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
        //                    //filePath = System.IO.Path.Combine(startPath, "Group3Template.xlsx");
        //                    filePath = HttpContext.Current.Server.MapPath("~/app/Resources/Group3Template.xlsx");

        //                }
        //                else
        //                {
        //                    startPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
        //                    //filePath = System.IO.Path.Combine(startPath, "Group2Template.xlsx");
        //                    filePath = HttpContext.Current.Server.MapPath("~/app/Resources/Group2Template.xlsx");
        //                }
        //            }

        //            SourceBook = app.Workbooks.Open(filePath);
        //            SourceSheet = (Excel.Worksheet)SourceBook.Worksheets.Item[3];
        //            //SourceSheet.Copy(After: DestinationBook.Worksheets.Item[j+1]);
        //            SourceSheet.Copy(Type.Missing, DestinationBook.Sheets[DestinationBook.Sheets.Count]);
        //            DestinationSheet = ((Excel.Worksheet)DestinationBook.Worksheets[i]);
        //            DestinationSheet.Name = RegionName;
        //            DestinationSheet.Cells[5, 3] = RegionName;
        //            if (item.BRID != 0)
        //            {
        //                if (GroupName == "RBU")
        //                {
        //                    RecessionPlanningController rpc = new RecessionPlanningController();
        //                    List<RecessionPlanning> rpList = await rpc.GetByBRId(item.BRID, year);
        //                    int h = 0;
        //                    //Recession Planning
        //                    foreach (var rp in rpList)
        //                    {
        //                        DestinationSheet.Cells[11 + h, 2] = h + 1;
        //                        DestinationSheet.Cells[11 + h, 3] = rp.Description;
        //                        DestinationSheet.Cells[11 + h, 4] = rp.Category;
        //                        h++;
        //                    }

        //                    //Focus Area
        //                    FocusAreasController fac = new FocusAreasController();
        //                    List<FocusArea> faList = await fac.GetByBRId(item.BRID, year);
        //                    int j = 0;
        //                    foreach (var fa in faList)
        //                    {
        //                        DestinationSheet.Cells[18 + j, 2] = j + 1;
        //                        DestinationSheet.Cells[18 + j, 3] = fa.StrategicActions;
        //                        j++;
        //                    }

        //                    //Lease Modification
        //                    LeaseModificationController lmc = new LeaseModificationController();
        //                    List<LeaseModification> lmList = await lmc.GetByBRId(item.BRID, year);
        //                    int k = 0;
        //                    foreach (var lm in lmList)
        //                    {
        //                        DestinationSheet.Cells[25 + k, 2] = k + 1;
        //                        DestinationSheet.Cells[25 + k, 3] = lm.Description;
        //                        DestinationSheet.Cells[25 + k, 4] = lm.Date;
        //                        DestinationSheet.Cells[25 + k, 5] = lm.PlanOfRenewal;
        //                        k++;
        //                    }
        //                    //Capital Expense
        //                    CapitalExpenseController cec = new CapitalExpenseController();
        //                    List<CapitalExpense> ceList = await cec.GetByBRId(item.BRID, year);
        //                    k = 0;
        //                    foreach (var cm in ceList)
        //                    {
        //                        DestinationSheet.Cells[33 + k, 2] = k + 1;
        //                        DestinationSheet.Cells[33 + k, 3] = cm.Description;
        //                        DestinationSheet.Cells[33 + k, 4] = string.Format("{0:#,###0}", cm.Amount);
        //                        k++;
        //                    }
        //                }

        //                else if (GroupName == "CSG" && RegionName.Contains("Integrated"))
        //                {
        //                    IntegratedTopFiveController itc = new IntegratedTopFiveController();
        //                    List<IntegratedTopFive> itList = await itc.GetByBRId(item.BRID, year);
        //                    int h = 0;
        //                    foreach (var fa in itList)
        //                    {
        //                        DestinationSheet.Cells[11 + h, 2] = h + 1;
        //                        DestinationSheet.Cells[11 + h, 3] = fa.StrategicActions;
        //                        h++;
        //                    }
        //                }

        //                else
        //                {
        //                    //Focus Area
        //                    FocusAreasController fac = new FocusAreasController();
        //                    List<FocusArea> faList = await fac.GetByBRId(item.BRID, year);
        //                    int j = 0;
        //                    foreach (var fa in faList)
        //                    {
        //                        DestinationSheet.Cells[11 + j, 2] = j + 1;
        //                        DestinationSheet.Cells[11 + j, 3] = fa.StrategicActions;
        //                        j++;
        //                    }

        //                    //Lease Modification
        //                    LeaseModificationController lmc = new LeaseModificationController();
        //                    List<LeaseModification> lmList = await lmc.GetByBRId(item.BRID, year);
        //                    int k = 0;
        //                    foreach (var lm in lmList)
        //                    {
        //                        DestinationSheet.Cells[19 + k, 2] = k + 1;
        //                        DestinationSheet.Cells[19 + k, 3] = lm.Description;
        //                        DestinationSheet.Cells[19 + k, 4] = lm.Date.Value.ToLocalTime();
        //                        DestinationSheet.Cells[19 + k, 5] = lm.PlanOfRenewal;
        //                        k++;
        //                    }
        //                    //Capital Expense
        //                    CapitalExpenseController cec = new CapitalExpenseController();
        //                    List<CapitalExpense> ceList = await cec.GetByBRId(item.BRID, year);
        //                    k = 0;
        //                    foreach (var cm in ceList)
        //                    {
        //                        DestinationSheet.Cells[27 + k, 2] = k + 1;
        //                        DestinationSheet.Cells[27 + k, 3] = cm.Description;
        //                        DestinationSheet.Cells[27 + k, 4] = string.Format("{0:#,###0}", cm.Amount);
        //                        k++;
        //                    }
        //                }
        //            }
        //            SourceBook.Close(false);
        //            // app.Workbooks[1].Close(false);
        //        }
        //        ((Excel.Worksheet)DestinationBook.Sheets[2]).Activate();
        //        //DestinationBook.SaveCopyAs(FileName);
        //        ((Excel.Worksheet)DestinationBook.Sheets[1]).Delete();
        //        //return DestinationBook;
        //        DestinationBook.SaveAs(ms);



        //    }
        //    catch (Exception e)
        //    {
        //        //ManageLogs("Error in exporting all to excel" + e);
        //        throw new ApplicationException(e.ToString());
        //    }



        //}




        public async Task<List<Bpmreference>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference Get method :", ex);
            }

        }

        //public async Task createPDFContentFromBRID(int BRID, Document pdfDoc, PdfWriter pdfWriter)
        //{
        //    try
        //    {
        //        var pageWidth = 0.0f;
        //        Chunk chunk;
        //        Paragraph line;

        //        BPMReferenceController bpm = new BPMReferenceController();
        //        BPMReference bpmRef = bpm.GetById(BRID);
        //        string nextYear = bpmRef.Year;

        //        //string nextYear = csc[0].Year;
        //        int currentYear = 2020;

        //        Int32.TryParse(nextYear, out currentYear);
        //        int CurrentYearMinusOne = currentYear - 1;

        //        drawHeader(pdfDoc, out chunk, out line, nextYear, BRID);

        //        PdfContentByte cb = pdfWriter.DirectContent;
        //        BaseFont bf1 = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1257, BaseFont.NOT_EMBEDDED);

        //        cb.SaveState();
        //        cb.BeginText();
        //        cb.MoveText(15, pdfDoc.Top - 5);
        //        cb.SetFontAndSize(bf1, 20);
        //        cb.ShowText(" Business Planning Meeting Report For " + nextYear);
        //        cb.EndText();
        //        cb.RestoreState();

        //        cb.SetLineWidth(1.0f);   // Make a bit thicker than 1.0 default
        //        cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
        //        cb.MoveTo(0, pdfDoc.Top - 10);
        //        cb.LineTo(650, pdfDoc.Top - 10);
        //        cb.Stroke();
        //        line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 0F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
        //        pdfDoc.Add(line);

        //        //pdfDoc.Add(pr);
        //        RegionController rc = new RegionController();
        //        List<Region> regionList = await rc.GetRegionNameByBRID(BRID);
        //        string dprGroupName = regionList[0].RegionName;
        //        string mainGroups = regionList[0].GroupName;
        //        //Paragraph pr = new Paragraph();
        //        chunk = new Chunk(" Group : ", FontFactory.GetFont("Arial", 16, Font.BOLD, BaseColor.BLACK));

        //        pdfDoc.Add(chunk);
        //        chunk = new Chunk(dprGroupName, FontFactory.GetFont("Arial", 16, Font.NORMAL, BaseColor.BLACK));
        //        //pr.Add(chunk);
        //        pdfDoc.Add(chunk);
        //        cb = pdfWriter.DirectContent;
        //        cb.SetLineWidth(1.0f);
        //        cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
        //        cb.MoveTo(0, pdfDoc.Top - 42);
        //        cb.LineTo(650, pdfDoc.Top - 42);
        //        cb.Stroke();
        //        //line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
        //        //pdfDoc.Add(line);

        //        //number of characters per line in pdf
        //        BaseFont bf = BaseFont.CreateFont(
        //        BaseFont.COURIER, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        //        float charWidth = bf.GetWidth(" ");
        //        int charactersPerLine = 100;
        //        // float pageWidth = pdfDoc.Right() - pdfDoc.Left();
        //        float fontSize = (1000 * (pageWidth / (charWidth * charactersPerLine)));
        //        fontSize = ((int)(fontSize * 100)) / 100f;
        //        Font font = new Font(bf, fontSize);


        //        PdfPCell cell = new PdfPCell();
        //        cell.Border = 0;
        //        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //        int noOfCells = 5;
        //        cell.Colspan = noOfCells;

        //        line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
        //        if (mainGroups == "RBU")
        //        {

        //            //recession planning

        //            PdfPTable table = new PdfPTable(3);
        //            table.WidthPercentage = 50;
        //            table.DefaultCell.FixedHeight = 35f;
        //            float[] sglTblHdWidths;
        //            sglTblHdWidths = new float[3];
        //            sglTblHdWidths[0] = 20f;
        //            sglTblHdWidths[1] = 40f;
        //            sglTblHdWidths[2] = 160f;
        //            table.SetWidths(sglTblHdWidths);
        //            table.TotalWidth = 310f;
        //            table.LockedWidth = true;
        //            cell = new PdfPCell(new Phrase("2020 Company Strategy and Recession Planning", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //            cell.BackgroundColor = new BaseColor(0, 90, 155);
        //            cell.Colspan = 6;
        //            cell.HorizontalAlignment = 1;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("Category", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            RecessionPlanningController rp = new RecessionPlanningController();
        //            List<RecessionPlanning> rpc = await rp.GetByBRIdOnly(BRID);
        //            List<List<string>> gridData1 = new List<List<string>>();

        //            foreach (var icItem in rpc)
        //            {
        //                string instanceDescription = icItem.Category;
        //                string instanceDate = icItem.Description;
        //                List<string> dataValue = new List<string>();
        //                dataValue.Add(instanceDescription);
        //                dataValue.Add(instanceDate);
        //                gridData1.Add(dataValue);
        //            }
        //            Console.WriteLine();

        //            //gridData.Add(dataValue);
        //            int count2 = gridData1.Count;
        //            if (count2 < 5)
        //            {
        //                int addEmptyRows = 5 - count2;
        //                for (int y = 0; y < addEmptyRows; y++)
        //                {
        //                    //int sno = count + y + 1;
        //                    List<string> emptyList = new List<string>(4);
        //                    //emptyList.Add(sno.ToString());
        //                    for (int z = 0; z < 2; z++)
        //                    {
        //                        emptyList.Add(" ");
        //                    }
        //                    gridData1.Add(emptyList);
        //                }
        //            }

        //            int x2 = 1;
        //            foreach (var gridItemList in gridData1)
        //            {

        //                table.AddCell(x2.ToString());
        //                x2++;

        //                foreach (var item in gridItemList)
        //                {

        //                    cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

        //                    table.AddCell(cell);

        //                }
        //            }

        //            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);

        //            table = new PdfPTable(2);
        //            table.WidthPercentage = 50;
        //            table.DefaultCell.FixedHeight = 35f;
        //            sglTblHdWidths = new float[2];
        //            sglTblHdWidths[0] = 20f;
        //            sglTblHdWidths[1] = 120f;

        //            table.SetWidths(sglTblHdWidths);
        //            table.TotalWidth = 240f;
        //            table.LockedWidth = true;
        //            cell = new PdfPCell(new Phrase("Strategic Action (Not Related To Company Strategy)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //            cell.BackgroundColor = new BaseColor(0, 90, 155);
        //            cell.Colspan = 8;
        //            cell.HorizontalAlignment = 1;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);


        //            //List<string> dataValue = new List<string>();
        //            FocusAreasController fa = new FocusAreasController();
        //            List<FocusArea> fac = await fa.GetByBRIdOnly(BRID);


        //            List<List<string>> gridData4 = new List<List<string>>();

        //            foreach (var icItem in fac)
        //            {
        //                string instanceDescription = icItem.StrategicActions;
        //                List<string> dataValue01 = new List<string>();
        //                dataValue01.Add(instanceDescription);

        //                gridData4.Add(dataValue01);
        //            }
        //            // Console.WriteLine();

        //            //gridData.Add(dataValue);
        //            int count4 = gridData4.Count;
        //            if (count4 < 5)
        //            {
        //                int addEmptyRows = 5 - count4;
        //                for (int y = 0; y < addEmptyRows; y++)
        //                {
        //                    //int sno = count + y + 1;
        //                    List<string> emptyList = new List<string>(4);
        //                    //emptyList.Add(sno.ToString());
        //                    for (int z = 0; z < 1; z++)
        //                    {
        //                        emptyList.Add(" ");
        //                    }
        //                    gridData4.Add(emptyList);
        //                }
        //            }

        //            int x5 = 1;
        //            foreach (var gridItemList in gridData4)
        //            {

        //                table.AddCell(x5.ToString());
        //                x5++;

        //                foreach (var item in gridItemList)
        //                {

        //                    cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

        //                    table.AddCell(cell);

        //                }
        //            }

        //            table.WriteSelectedRows(0, -1, pdfDoc.Left + 320, pdfDoc.Top - 60, pdfWriter.DirectContent);

        //            //Lease Modifications

        //            table = new PdfPTable(4);
        //            table.WidthPercentage = 100;
        //            table.DefaultCell.FixedHeight = 35f;

        //            sglTblHdWidths = new float[4];
        //            sglTblHdWidths[0] = 20f;
        //            sglTblHdWidths[1] = 220f;
        //            sglTblHdWidths[2] = 120f;
        //            sglTblHdWidths[3] = 220f;
        //            table.SetWidths(sglTblHdWidths);
        //            table.TotalWidth = 570f;
        //            table.LockedWidth = true;
        //            cell = new PdfPCell(new Phrase("2020 Lease Modifications", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //            cell.BackgroundColor = new BaseColor(0, 90, 155);
        //            cell.Colspan = 8;
        //            cell.HorizontalAlignment = 1;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("Lease Modification Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = new PdfPCell(new Phrase("Current Date Of Expiration", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("Plan Of Renewal", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);
        //            //List<string> dataValue = new List<string>();
        //            LeaseModificationController lm = new LeaseModificationController();
        //            List<LeaseModification> lmc = await lm.GetByBRIdOnly(BRID);
        //            List<List<string>> gridData2 = new List<List<string>>();

        //            foreach (var icItem in lmc)
        //            {
        //                string instanceDescription = icItem.Description;
        //                string instanceDate = icItem.Date.Value.ToShortDateString();
        //                string instancePlanOfRenewal = icItem.PlanOfRenewal;
        //                List<string> dataValue = new List<string>();
        //                dataValue.Add(instanceDescription);
        //                dataValue.Add(instanceDate);
        //                dataValue.Add(instancePlanOfRenewal);
        //                gridData2.Add(dataValue);
        //            }
        //            Console.WriteLine();

        //            //gridData.Add(dataValue);
        //            int count6 = gridData2.Count;
        //            if (count6 < 5)
        //            {
        //                int addEmptyRows = 5 - count6;
        //                for (int y = 0; y < addEmptyRows; y++)
        //                {
        //                    //int sno = count + y + 1;
        //                    List<string> emptyList = new List<string>(4);
        //                    //emptyList.Add(sno.ToString());
        //                    for (int z = 0; z < 3; z++)
        //                    {
        //                        emptyList.Add(" ");
        //                    }
        //                    gridData2.Add(emptyList);
        //                }
        //            }

        //            int x3 = 1;
        //            foreach (var gridItemList in gridData2)
        //            {

        //                table.AddCell(x3.ToString());
        //                x3++;

        //                foreach (var item in gridItemList)
        //                {

        //                    cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

        //                    table.AddCell(cell);

        //                }
        //            }

        //            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 300, pdfWriter.DirectContent);

        //            table = new PdfPTable(3);
        //            table.WidthPercentage = 100;
        //            table.DefaultCell.FixedHeight = 35f;

        //            sglTblHdWidths = new float[3];
        //            sglTblHdWidths[0] = 50f;
        //            sglTblHdWidths[1] = 320f;
        //            sglTblHdWidths[2] = 80f;
        //            table.SetWidths(sglTblHdWidths);
        //            table.TotalWidth = 570f;
        //            table.LockedWidth = true;
        //            cell = new PdfPCell(new Phrase("2020 Capital Expenses", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //            cell.BackgroundColor = new BaseColor(0, 90, 155);
        //            cell.Colspan = 6;
        //            cell.HorizontalAlignment = 1;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);
        //            cell = new PdfPCell(new Phrase("Capital Expense Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = new PdfPCell(new Phrase("Amount(in $)", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //            cell.Rowspan = 1;
        //            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            CapitalExpenseController ce = new CapitalExpenseController();
        //            List<CapitalExpense> cec = await ce.GetByBRIdOnly(BRID);
        //            List<List<string>> gridData3 = new List<List<string>>();

        //            foreach (var icItem in cec)
        //            {
        //                string instanceDescription = icItem.Description;
        //                string instanceAmount = icItem.Amount;
        //                List<string> dataValue5 = new List<string>();
        //                dataValue5.Add(instanceDescription);
        //                dataValue5.Add(instanceAmount);
        //                gridData3.Add(dataValue5);
        //            }
        //            Console.WriteLine();

        //            //gridData.Add(dataValue);
        //            int count3 = gridData3.Count;
        //            if (count3 < 5)
        //            {
        //                int addEmptyRows = 5 - count3;
        //                for (int y = 0; y < addEmptyRows; y++)
        //                {
        //                    //int sno = count + y + 1;
        //                    List<string> emptyList = new List<string>(4);
        //                    //emptyList.Add(sno.ToString());
        //                    for (int z = 0; z < 2; z++)
        //                    {
        //                        emptyList.Add(" ");
        //                    }
        //                    gridData3.Add(emptyList);
        //                }
        //            }

        //            int x6 = 1;
        //            foreach (var gridItemList in gridData3)
        //            {

        //                table.AddCell(x6.ToString());
        //                x6++;

        //                foreach (var item in gridItemList)
        //                {

        //                    cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

        //                    table.AddCell(cell);

        //                }
        //            }

        //            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 550, pdfWriter.DirectContent);


        //        }

        //        else if (mainGroups == "RBU" || mainGroups == "CM" || mainGroups == "CSG" || mainGroups == "SP" || mainGroups == "WVE")
        //        {
        //            if (mainGroups == "CSG" && dprGroupName.Contains("Integrated") == true)
        //            {
        //                //2020 IntegratedTopFive

        //                PdfPTable table = new PdfPTable(2);
        //                table.WidthPercentage = 100;
        //                table.DefaultCell.FixedHeight = 35f;
        //                float[] sglTblHdWidths;
        //                sglTblHdWidths = new float[2];
        //                sglTblHdWidths[0] = 120f;
        //                sglTblHdWidths[1] = 820f;

        //                table.SetWidths(sglTblHdWidths);
        //                table.TotalWidth = 570f;
        //                table.LockedWidth = true;
        //                cell = new PdfPCell(new Phrase("Integrated Top 5 (TCOP, Integrated Project Life Cycle, Get Work)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //                cell.BackgroundColor = new BaseColor(0, 90, 155);
        //                cell.Colspan = 8;
        //                cell.HorizontalAlignment = 1;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);


        //                //List<string> dataValue = new List<string>();
        //                IntegratedTopFiveController itf = new IntegratedTopFiveController();
        //                List<IntegratedTopFive> itfc = await itf.GetByBRIdOnly(BRID);


        //                List<List<string>> gridData1 = new List<List<string>>();

        //                foreach (var icItem in itfc)
        //                {
        //                    string instanceDescription = icItem.StrategicActions;
        //                    List<string> dataValue01 = new List<string>();
        //                    dataValue01.Add(instanceDescription);

        //                    gridData1.Add(dataValue01);
        //                }
        //                Console.WriteLine();

        //                //gridData.Add(dataValue);
        //                int count1 = gridData1.Count;
        //                if (count1 < 5)
        //                {
        //                    int addEmptyRows = 5 - count1;
        //                    for (int y = 0; y < addEmptyRows; y++)
        //                    {
        //                        //int sno = count + y + 1;
        //                        List<string> emptyList = new List<string>(4);
        //                        //emptyList.Add(sno.ToString());
        //                        //for (int z = 0; z < 3; z++)
        //                        //{
        //                        emptyList.Add(" ");
        //                        //}
        //                        gridData1.Add(emptyList);
        //                    }
        //                }

        //                int x1 = 1;
        //                foreach (var gridItemList in gridData1)
        //                {

        //                    table.AddCell(x1.ToString());
        //                    x1++;

        //                    foreach (var item in gridItemList)
        //                    {

        //                        cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

        //                        table.AddCell(cell);

        //                    }
        //                }

        //                table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 100, pdfWriter.DirectContent);



        //                //String tableHeaderValue = currentYear + " Integrated Top Five";
        //                //int left = 180;
        //                //int top = -60;
        //                //int i;
        //                //PdfPTable table = null;
        //                //float[] sglTblHdWidths;
        //                //drawTableList(pdfDoc, pdfWriter, out cell, dataValue01, tableHeaderValue, left, top, out table, out sglTblHdWidths, out i, 1);
        //            }
        //            else
        //            {

        //                PdfPTable table = new PdfPTable(2);
        //                table.WidthPercentage = 100;
        //                table.DefaultCell.FixedHeight = 35f;
        //                float[] sglTblHdWidths;
        //                sglTblHdWidths = new float[2];
        //                sglTblHdWidths[0] = 20f;
        //                sglTblHdWidths[1] = 500f;

        //                table.SetWidths(sglTblHdWidths);
        //                table.TotalWidth = 570f;
        //                table.LockedWidth = true;
        //                cell = new PdfPCell(new Phrase("Strategic Actions (Not Related to Company Strategy)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //                cell.BackgroundColor = new BaseColor(0, 90, 155);
        //                cell.Colspan = 2;
        //                cell.HorizontalAlignment = 1;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);


        //                //List<string> dataValue = new List<string>();
        //                FocusAreasController fa = new FocusAreasController();
        //                List<FocusArea> fac = await fa.GetByBRIdOnly(BRID);
        //                List<List<string>> gridData1 = new List<List<string>>();

        //                foreach (var icItem in fac)
        //                {
        //                    string instanceDescription = icItem.StrategicActions;
        //                    List<string> dataValue01 = new List<string>();
        //                    dataValue01.Add(instanceDescription);

        //                    gridData1.Add(dataValue01);
        //                }
        //                //gridData.Add(dataValue);
        //                int count1 = gridData1.Count;
        //                if (count1 < 5)
        //                {
        //                    int addEmptyRows = 5 - count1;
        //                    for (int y = 0; y < addEmptyRows; y++)
        //                    {
        //                        //int sno = count + y + 1;
        //                        List<string> emptyList = new List<string>(3);
        //                        emptyList.Add(" ");
        //                        // emptyList.Add(sno.ToString());
        //                        //for (int z = 0; z < 2; z++)
        //                        //{
        //                        //    
        //                        //}
        //                        gridData1.Add(emptyList);
        //                    }
        //                }

        //                int x1 = 1;
        //                foreach (var gridItemList in gridData1)
        //                {

        //                    table.AddCell(x1.ToString());
        //                    x1++;

        //                    foreach (var item in gridItemList)
        //                    {

        //                        cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

        //                        table.AddCell(cell);

        //                    }
        //                }

        //                table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);


        //                table = new PdfPTable(4);
        //                table.WidthPercentage = 100;
        //                table.DefaultCell.FixedHeight = 35f;

        //                sglTblHdWidths = new float[4];
        //                sglTblHdWidths[0] = 120f;
        //                sglTblHdWidths[1] = 820f;
        //                sglTblHdWidths[2] = 520f;
        //                sglTblHdWidths[3] = 820f;
        //                table.SetWidths(sglTblHdWidths);
        //                table.TotalWidth = 570f;
        //                table.LockedWidth = true;
        //                cell = new PdfPCell(new Phrase("2020 Lease Modifications", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //                cell.BackgroundColor = new BaseColor(0, 90, 155);
        //                cell.Colspan = 8;
        //                cell.HorizontalAlignment = 1;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("Lease Modification Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);

        //                cell = new PdfPCell(new Phrase("Current Date Of Expiration", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("Plan Of Renewal", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);
        //                LeaseModificationController lm = new LeaseModificationController();
        //                List<LeaseModification> lmc = await lm.GetByBRIdOnly(BRID);
        //                List<List<string>> gridData = new List<List<string>>();

        //                foreach (var icItem in lmc)
        //                {
        //                    string instanceDescription = icItem.Description;
        //                    string instanceDate = icItem.Date.Value.ToShortDateString();
        //                    string instancePlanOfRenewal = icItem.PlanOfRenewal;
        //                    List<string> dataValue = new List<string>();
        //                    dataValue.Add(instanceDescription);
        //                    dataValue.Add(instanceDate);
        //                    dataValue.Add(instancePlanOfRenewal);
        //                    gridData.Add(dataValue);
        //                }
        //                Console.WriteLine();

        //                //gridData.Add(dataValue);
        //                int count = gridData.Count;
        //                if (count < 5)
        //                {
        //                    int addEmptyRows = 5 - count;
        //                    for (int y = 0; y < addEmptyRows; y++)
        //                    {
        //                        //int sno = count + y + 1;
        //                        List<string> emptyList = new List<string>(4);
        //                        //emptyList.Add(sno.ToString());
        //                        for (int z = 0; z < 3; z++)
        //                        {
        //                            emptyList.Add(" ");
        //                        }
        //                        gridData.Add(emptyList);
        //                    }
        //                }

        //                int x = 1;
        //                foreach (var gridItemList in gridData)
        //                {

        //                    table.AddCell(x.ToString());
        //                    x++;

        //                    foreach (var item in gridItemList)
        //                    {

        //                        cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));

        //                        table.AddCell(cell);

        //                    }
        //                }

        //                table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 300, pdfWriter.DirectContent);

        //                //capital expenses

        //                table = new PdfPTable(3);
        //                table.WidthPercentage = 100;
        //                table.DefaultCell.FixedHeight = 35f;

        //                sglTblHdWidths = new float[3];
        //                sglTblHdWidths[0] = 50f;
        //                sglTblHdWidths[1] = 420f;
        //                sglTblHdWidths[2] = 80f;
        //                table.SetWidths(sglTblHdWidths);
        //                table.TotalWidth = 570f;
        //                table.LockedWidth = true;
        //                cell = new PdfPCell(new Phrase("2020 Capital Expenses", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
        //                cell.BackgroundColor = new BaseColor(0, 90, 155);
        //                cell.Colspan = 6;
        //                cell.HorizontalAlignment = 1;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);
        //                cell = new PdfPCell(new Phrase("Capital Expense Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);

        //                cell = new PdfPCell(new Phrase("Amount(in $)", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
        //                cell.Rowspan = 1;
        //                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //                table.AddCell(cell);

        //                CapitalExpenseController ce = new CapitalExpenseController();
        //                List<CapitalExpense> cec = await ce.GetByBRIdOnly(BRID);
        //                List<List<string>> gridData3 = new List<List<string>>();

        //                foreach (var icItem in cec)
        //                {
        //                    string instanceDescription = icItem.Description;
        //                    string instanceAmount = icItem.Amount;
        //                    List<string> dataValue5 = new List<string>();
        //                    dataValue5.Add(instanceDescription);
        //                    dataValue5.Add(instanceAmount);
        //                    gridData3.Add(dataValue5);
        //                }
        //                Console.WriteLine();

        //                //gridData.Add(dataValue);
        //                int count3 = gridData3.Count;
        //                if (count3 < 5)
        //                {
        //                    int addEmptyRows = 5 - count3;
        //                    for (int y = 0; y < addEmptyRows; y++)
        //                    {
        //                        //int sno = count + y + 1;
        //                        List<string> emptyList = new List<string>(4);


        //                        //emptyList.Add(sno.ToString());
        //                        for (int z = 0; z < 2; z++)
        //                        {
        //                            emptyList.Add(" ");
        //                        }
        //                        gridData3.Add(emptyList);
        //                    }
        //                }

        //                int x6 = 1;
        //                foreach (var gridItemList in gridData3)
        //                {

        //                    table.AddCell(x6.ToString());
        //                    x6++;

        //                    foreach (var item in gridItemList)
        //                    {

        //                        cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

        //                        table.AddCell(cell);

        //                    }
        //                }
        //                table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 550, pdfWriter.DirectContent);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Trace.TraceError("If you're seeing this, something bad happened sdfghfvjnv" + e);
        //        throw new ApplicationException(e.ToString());
        //        //ManageLogs("Error in PDFController converting all to pdf " + e);
        //    }
        //}

        //private void drawHeader(Document pdfDoc, out Chunk chunk, out Paragraph line, string year, int BRID)
        //{
        //    try
        //    {
        //        Paragraph pr = new Paragraph();
        //        chunk = new Chunk(" Business Planning Meeting Report For " + year, FontFactory.GetFont("Arial", 21, Font.BOLD, BaseColor.BLACK));

        //        //Horizontal Line
        //        line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
        //        Image ok = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/dpr-logo.jpg"));
        //        ok.ScaleAbsoluteWidth(40);
        //        ok.ScaleAbsoluteHeight(20);
        //        ok.SetAbsolutePosition(pdfDoc.Right - 35, pdfDoc.Top - 5);
        //        ok.SetDpi(300, 300);
        //        pdfDoc.Add(ok);
        //    }
        //    catch(Exception e)
        //    {
        //        throw e;
        //    }

        //}



        // GET api/<controller>/5
        public Bpmreference GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByID method :", ex);
            }
        }

       

        // GET api/<controller>
        [Route("api/BPMReference/GetByYear/{year}")]
        public async Task<List<Bpmreference>> GetByYear(string year)
        {
            try
            {


                List<Bpmreference> yearList = new List<Bpmreference>();
                string[] children = new string[] { "Region", "FocusAreas", "LeaseModifications", "CapitalExpenses", "RecessionPlannings", "IntegratedTopFives" };
                //yearList = await _repository.GetMultipleModelWithEagrLoading(x => x.Year == year,children);
                yearList = await _repository.GetModel();
                yearList = yearList.Where(c => c.Year == year).ToList();
                //yearList = yearList.Where(x => x.Year == year).ToList();
                //RegionController rc = new RegionController();
                //foreach (var item in yearList)
                //{
                //    var regID = item.RegionID;
                //    var regionItem = rc.GetById(Convert.ToInt32(regID));
                //    var regionName = regionItem.RegionName;

                //}
                //OrderBy(c => c.Region.SerialNo).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }


        //****************
        // GET api/<controller>
        [Route("api/BPMReference/GetBpmWithRegionByYear/{year}")]
        public async Task<List<ViewRegion>> GetBpmWithRegionByYear(string year)
        {
            List<Bpmreference> yearListwithRegions = new List<Bpmreference>();
            try
            {
                yearListwithRegions = await _repository.GetModelWithEagrLoading("Region");
                var ListwithRegions = yearListwithRegions.Where(x => x.Year == year).Select(l => new ViewRegion
                {
                    BRID=l.Brid,
                    RegionID = l.RegionId,
                    RegionName=l.Region.RegionName,
                    GroupName=l.Region.GroupName
                }).OrderBy(d=>d.GroupName);

                return ListwithRegions.ToList();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }

        [Route("api/BPMReference/getGroupByBRId/{BRID}/{year}")]
        public async Task<List<Region>> GetGroupByBRId(string BRID, string year)
        {
            var bpm = await _repository.GetModel();
            var regionId = bpm.Where(f=>f.Brid==int.Parse(BRID)).Select(x => x.RegionId).First();

            var regionrepository = new BPMRepository<Region>(new bpmdbdevContext());
            var regionList = await regionrepository.GetModel();
           var groupName= regionList.Where(x => x.RegionId == regionId).ToList();
            return groupName;


        }



        //[Route("api/BPMReference/GetAllByYear/{year}")]
        public Bpmreference GetAllByYear(string year,int BRID=0)
        {
            try
            {


                List<Bpmreference> Bpm = new List<Bpmreference>();
                string[] children = new string[] { "Region", "Narratives", "FocusAreas" };
                //"LeaseModifications", "CapitalExpenses", "IntegratedTopFives"
                Bpm =  _repository.GetMultipleModelWithEagrLoading(x => x.Year == year & (BRID != 0 ? x.Brid == BRID : true), children).Result;
                var finalList = Bpm.First();
                //OrderBy(c => c.Region.SerialNo).ToList(); ;
                //yearList = yearList.Where(x => x.Year == year).ToList();
                //RegionController rc = new RegionController();
                //foreach (var item in yearList)
                //{
                //    var regID = item.RegionID;
                //    var regionItem = rc.GetById(Convert.ToInt32(regID));
                //    var regionName = regionItem.RegionName;

                //}
                //OrderBy(c => c.Region.SerialNo).ToList();
                return finalList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }



        [Route("api/BPMReference/GetByResPerYear/{year}")]
        public async Task<List<ViewRegion>> GetByResPerYear(string year)
        {
            try
            {
                // String userName = (User.Identity as System.Security.Claims.ClaimsIdentity).Name.ToLower();
                string userName = "saurabhd@vconstruct.in";
                userName = userName.Trim();
                var regionrepository = new BPMRepository<Region>(new bpmdbdevContext());
                var regionList = await regionrepository.GetModel();
                var regionIdList = regionList.Where(s => (s.ResponsiblePerson.ToLower().Contains(userName) | s.Mcsponser == userName) & s.Year == year).Select(o => o.RegionId).ToList();
                ////regionList.Select(o => o.ResponsiblePerson.Contains(userName))).ToList();

                //List<BPMReference> yearList = new List<BPMReference>();
                //yearList = await _repository.GetModel();
                //yearList = yearList.Where(x => x.Year == year & regionList.Select(o => o.RegionID).Contains(x.RegionID)).ToList();
                List<Bpmreference> yearListwithRegions = new List<Bpmreference>();

                yearListwithRegions = await _repository.GetModelWithEagrLoading("Region");
                var ListwithRegions = yearListwithRegions.Where(x => x.Year == year & regionIdList.Contains(x.RegionId.Value) & (x.Region.ResponsiblePerson.ToLower().Contains(userName) | x.Region.Mcsponser == userName)).Select(l => new ViewRegion
                {
                    BRID = l.Brid,
                    RegionID = l.RegionId,
                    RegionName = l.Region.RegionName,
                    GroupName = l.Region.GroupName
                });

                return ListwithRegions.ToList();
               // return yearList;
            }
           
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }
          
        }

        [Route("api/BPMReference/GetBRIDByYearRegId/{year}/{regionId}")]
        public async Task<string> GetBRIDByYearRegId(string year, int regionId)
        {
            try
            {


                List<Bpmreference> yearList = new List<Bpmreference>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year && x.RegionId == regionId).ToList();

                //List<Region> regionList = new List<Region>();
                //var regionrepository = new BPMRepository<Region>(new bpmdbdevContext());
                //regionList = await regionrepository.GetModel();
                //var regionIdList = regionList.Where(x => x.RegionID == regionId ).Select(o=>o.RegionID).ToList();
                //yearList = yearList.Where(x => x.Year == year && regionIdList.Contains(x.RegionID)).ToList();

                if (yearList.Count == 0)
                {
                    Bpmreference bpmRef = new Bpmreference();
                    bpmRef.Year = year;
                    bpmRef.RegionId = regionId;
                    //Post(bpmRef);
                    await _repository.InsertModel(bpmRef);
                    yearList = new List<Bpmreference>();
                    yearList = await _repository.GetModel();
                    yearList = yearList.Where(x => x.Year == year && x.RegionId == regionId).ToList();
                   
                }

                return yearList.First().Brid.ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(Bpmreference collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in BPM Reference Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        //[HttpPut]
        
        public void put(int id, Bpmreference collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in BPM Reference put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in BPM Reference Controller Delete method:", e);
            }
        }

        public async Task<string> GetAccByReg(string year, int regionId)
        {
            try
            {
                List<Bpmreference> BRTable = new List<Bpmreference>();
                BRTable = await _repository.GetModel();
                BRTable = BRTable.Where(x => x.Year == year && x.RegionId == regionId).ToList();
               if( BRTable.Any())
                {
                   // BRTable.Select(c=>c.SPID)

                }
                return BRTable.First().Brid.ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BPM Reference GetByYear method :", ex);
            }

        }

    }

    public  class ViewRegion
    {
        public int BRID { get; set; }
        public int? RegionID { get; set; }
        public string RegionName { get; set; }
        public string GroupName { get; set; }
        public int? SerialNo { get; set; }

        public string Year { get; set; }
    }
}