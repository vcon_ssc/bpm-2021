﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    //[Authorize]
    public class CurrentSuccessController : ControllerBase
    {
        private readonly IGenericRepository<CurrentSuccess> _repository;
        public CurrentSuccessController()
        {
            this._repository = new BPMRepository<CurrentSuccess>(new bpmdbdevContext());
        }

        //public CurrentSuccessController(IGenericRepository<CurrentSuccess> repository)
        //{
        //    this._repository = repository;
        //}

        // GET api/<controller>
        public async Task<List<CurrentSuccess>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentSuccess Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public CurrentSuccess GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentSuccess GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/CurrentSuccess/GetByYear/{year}")]
        public async Task<List<CurrentSuccess>> GetByYear(string year)
        {
            try
            {
                List<CurrentSuccess> yearList = new List<CurrentSuccess>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentSuccess GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/CurrentSuccess/GetByBRId/{BRId}")]
        public async Task<List<CurrentSuccess>> GetByBRId(int BRId)
        {
            try
            {
                List<CurrentSuccess> brList = new List<CurrentSuccess>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentSuccess GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(CurrentSuccess collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentSuccess Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public void Put(CurrentSuccess collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentSuccess put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentSuccess Controller Delete method:", e);
            }
        }
    }
}