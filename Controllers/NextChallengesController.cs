﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    [Authorize]
    public class NextChallengesController : ControllerBase
    {
        private IGenericRepository<NextChallenge> _repository;
        public NextChallengesController()
        {
            this._repository = new BPMRepository<NextChallenge>(new bpmdbdevContext());
        }

        public NextChallengesController(IGenericRepository<NextChallenge> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<NextChallenge>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextChallenges Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public NextChallenge GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextChallenges GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/BPMReference/NextChallenge/{year}")]
        public async Task<List<NextChallenge>> GetByYear(string year)
        {
            try
            {
                List<NextChallenge> yearList = new List<NextChallenge>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextChallenges GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/NextChallenge/GetByBRId/{BRId}")]
        public async Task<List<NextChallenge>> GetByBRId(int BRId)
        {
            try
            {
                List<NextChallenge> brList = new List<NextChallenge>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextChallenge GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(NextChallenge collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextChallenges Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, NextChallenge collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextChallenges put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextChallenges Controller Delete method:", e);
            }
        }
    }
}