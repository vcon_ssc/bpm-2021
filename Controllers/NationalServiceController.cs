﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    [Authorize]
    public class NationalServiceController : ControllerBase
    {
        private IGenericRepository<NationalService> _repository;
        public NationalServiceController()
        {
            this._repository = new BPMRepository<NationalService>(new bpmdbdevContext());
        }

        public NationalServiceController(IGenericRepository<NationalService> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<NationalService>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NationalService Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public NationalService GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NationalService GetByID method :", ex);
            }
        }

        ////// GET api/<controller>
        //[Route("api/NationalService/GetByBRId/{BRId}")]
        //public async Task<List<NationalService>> GetByBRId(int BRId)
        //{
        //    try
        //    {
        //        List<NationalService> brList = new List<NationalService>();
        //        brList = await _repository.GetModel();
        //        brList = brList.Where(X => X.BRID == BRId).ToList();
        //        return brList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Error in NationalService GetByBRId method :", ex);
        //    }

        //}

        // POST api/<controller>
        public void Post(NationalService collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NationalService Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, NationalService collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NationalService put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NationalService Controller Delete method:", e);
            }
        }
    }
}