﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using BPM.Models;
using System.Threading.Tasks;
using System.Web;
using Font = iTextSharp.text.Font;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using ClosedXML.Excel;
using ClosedXML.Extensions;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.Entity;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using RouteAttribute = System.Web.Http.RouteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;

namespace BPM.Controllers.API
{
    public class ExportController : ControllerBase

    {
        
        private readonly IHostEnvironment _env;

        
        public ExportController(IHostEnvironment env)
        {
            _env = env;
        }
        #region PDF Convert
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Export/CreatePDFAsync/{BRID}")]
        public async Task CreatePDFAsync(int BRID)
        {

            try
            {
                HttpResponse Response = HttpContext.Response;
                MemoryStream ms = new MemoryStream();
                Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 10);
                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, ms);

                // string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";

                int i = 0;
                pdfDoc.Open();
                //int yearBRID = nextYear + ";
                await CreatePDFContentFromBRID(BRID, pdfDoc, pdfWriter);
                pdfWriter.CloseStream = false;
                pdfDoc.Close();



                //Response.Body. = true;
                Response.ContentType = "application/pdf";
                byte[] pdfBytes = ms.ToArray();
                Response.Headers.Append("Content-Length", pdfBytes.Length.ToString());
                Response.Body.Write(pdfBytes, 0, (int)pdfBytes.Length);
                //Response.AddHeader("content-disposition", "attachment;filename=nextYear + " Top 5 by xxx Group.pdf");
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Write(pdfDoc);
                Response.StatusCode = StatusCodes.Status200OK;
                return;
            }
            catch (Exception e)
            {
                throw e;

            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Export/CreateAllYearPDFAsync/{year}")]
        public async Task CreateYearPDFAsync(string year)
        {
            try
            {

                HttpResponse Response = HttpContext.Response;
                Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
                MemoryStream ms = new MemoryStream();
                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, ms);

                //PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";
                pdfDoc.Open();

                BPMReferenceController bpm = new BPMReferenceController();
                List<ViewRegion> bpmRef = await bpm.GetBpmWithRegionBySerialNo(year.ToString());
                int i = 0;
                foreach (var item in bpmRef)
                {
                    int yearBRID = item.BRID;
                    await CreatePDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
                    i++;
                    pdfDoc.NewPage();
                }

                //await 
                pdfWriter.CloseStream = false;
                pdfDoc.Close();

                //Response.Buffer = true;
                Response.ContentType = "application/pdf";
                byte[] pdfBytes = ms.ToArray();
                Response.Headers.Append("Content-Length", pdfBytes.Length.ToString());
                Response.Body.Write(pdfBytes, 0, (int)pdfBytes.Length);
                Response.StatusCode = StatusCodes.Status200OK;
                return;


            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Export/CreateMyPDFAsync/{year}")]
        public async Task CreateMyPDFAsync(string year)
        {
            HttpResponse Response =HttpContext.Response;

            MemoryStream ms = new MemoryStream();
            Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, ms);

            //  string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";


            pdfDoc.Open();

            BPMReferenceController bpm = new BPMReferenceController();
            List<ViewRegion> bpmRef = await bpm.GetByResPerYear(year.ToString());
            int i = 0;
            foreach (var item in bpmRef)
            {
                int yearBRID = item.BRID;
                await CreatePDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
                i++;
                pdfDoc.NewPage();
            }

            //await 
            pdfWriter.CloseStream = false;
            pdfDoc.Close();


            Response.Clear();
            //Response.Buffer = true;
            byte[] pdfBytes = ms.ToArray();
            Response.Headers.Append("Content-Length", pdfBytes.Length.ToString());
            Response.Body.Write(pdfBytes, 0, (int)pdfBytes.Length);
            //Response.AddHeader("content-disposition", "attachment;filename=nextYear + " Top 5 by xxx Group.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Write(pdfDoc);
            Response.StatusCode = StatusCodes.Status200OK;
            return;

        }

        public async Task CreatePDFContentFromBRID(int BRID, Document pdfDoc, PdfWriter pdfWriter)
        {
            try
            {
                var pageWidth = 0.0f;

                BPMReferenceController bpm = new BPMReferenceController();
                Bpmreference bpmRef = bpm.GetById(BRID);
                string nextYear = bpmRef.Year;
                Chunk chunk;
                Paragraph line;

                DrawHeader(pdfDoc,out chunk,out line, nextYear);

                PdfContentByte cb = pdfWriter.DirectContent;
                BaseFont bf1 = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1257, BaseFont.NOT_EMBEDDED);

                cb.SaveState();
                cb.BeginText();
                cb.MoveText(15, pdfDoc.Top - 5);
                cb.SetFontAndSize(bf1, 20);
                cb.ShowText(" Business Planning Meeting Report For " + nextYear);
                cb.EndText();
                cb.RestoreState();

                cb.SetLineWidth(1.0f);   // Make a bit thicker than 1.0 default
                cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
                cb.MoveTo(0, pdfDoc.Top - 10);
                cb.LineTo(650, pdfDoc.Top - 10);
                cb.Stroke();
                line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 0F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
                pdfDoc.Add(line);

                //pdfDoc.Add(pr);
                RegionController rc = new RegionController();
                List<Region> regionList = await rc.GetRegionByBRID(BRID);
                string dprGroupName = regionList[0].RegionName;
                string mainGroups = regionList[0].GroupName;
                //Paragraph pr = new Paragraph();
                chunk = new Chunk(" Group : ", FontFactory.GetFont("Arial", 16, Font.BOLD, BaseColor.BLACK));

                pdfDoc.Add(chunk);
                chunk = new Chunk(dprGroupName, FontFactory.GetFont("Arial", 16, Font.NORMAL, BaseColor.BLACK));
                //pr.Add(chunk);
                pdfDoc.Add(chunk);
                cb = pdfWriter.DirectContent;
                cb.SetLineWidth(1.0f);
                cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
                cb.MoveTo(0, pdfDoc.Top - 42);
                cb.LineTo(650, pdfDoc.Top - 42);
                cb.Stroke();
                //line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
                //pdfDoc.Add(line);

                //number of characters per line in pdf
                BaseFont bf = BaseFont.CreateFont(
                BaseFont.COURIER, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
                float charWidth = bf.GetWidth(" ");
                int charactersPerLine = 100;
                // float pageWidth = pdfDoc.Right() - pdfDoc.Left();
                float fontSize = (1000 * (pageWidth / (charWidth * charactersPerLine)));
                fontSize = ((int)(fontSize * 100)) / 100f;
                Font font = new Font(bf, fontSize);


                PdfPCell cell = new PdfPCell
                {
                    Border = 0,
                    HorizontalAlignment = PdfPCell.ALIGN_CENTER
                };
                int noOfCells = 5;
                cell.Colspan = noOfCells;

                line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                if (mainGroups == "RBU"|| mainGroups == "SP" || mainGroups == "WVE"|| mainGroups == "CM" )
                {
                    int count2=0;
                    #region Narrative
                    //2021 Narrative 


                    PdfPTable table = new PdfPTable(2)
                    {
                        WidthPercentage = 100
                    };
                    table.DefaultCell.FixedHeight = 35f;
                    float[] sglTblHdWidths;
                    sglTblHdWidths = new float[2];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 500f;

                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase(nextYear + " Narrative", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)))
                    {
                        BackgroundColor = new BaseColor(0, 90, 155),
                        Colspan = 2,
                        HorizontalAlignment = 1
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);


                    List<string> dataValue = new List<string>();
                    NarrativeController nc = new NarrativeController();
                    List<Narrative> nac = await nc.GetByBRIdOnly(BRID);

                    List<List<string>> gridData1 = new List<List<string>>();

                    foreach (var icItem in nac)
                    {
                        string instanceDescription = icItem.Description;
                        List<string> dataValue01 = new List<string>();
                        dataValue01.Add(instanceDescription);
                        //if(instanceDescription.Length>500)
                        gridData1.Add(dataValue01);
                    }

                    int count1 = gridData1.Count;
                    if (count1 < 1)
                    {
                        int addEmptyRows = 1;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(3)
                            {
                                " "
                            };
                            //for (int z = 0; z < 1; z++)
                            //{
                            //    emptyList.Add(" ");
                            //}
                            gridData1.Add(emptyList);
                        }
                    }


                    int x1 = 1;
                    foreach (var gridItemList in gridData1)
                    {

                        table.AddCell(x1.ToString());
                        x1++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }
                   
                    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);


                    #endregion

                    #region Recession planning
                    ////recession planning

                    //table = new PdfPTable(3);
                    //table.WidthPercentage = 100;
                    //table.DefaultCell.FixedHeight = 35f;
                    ////float[] sglTblHdWidths;
                    //sglTblHdWidths = new float[3];
                    //sglTblHdWidths[0] = 20f;
                    //sglTblHdWidths[1] = 40f;
                    //sglTblHdWidths[2] = 160f;
                    //table.SetWidths(sglTblHdWidths);
                    //table.TotalWidth = 310f;
                    //table.LockedWidth = true;
                    //cell = new PdfPCell(new Phrase(nextYear + " Company Strategy and Recession Planning", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    //cell.BackgroundColor = new BaseColor(0, 90, 155);
                    //cell.Colspan = 6;
                    //cell.HorizontalAlignment = 1;
                    //table.AddCell(cell);
                    //cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);
                    //cell = new PdfPCell(new Phrase("Category", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);

                    //RecessionPlanningController rp = new RecessionPlanningController();
                    //List<RecessionPlanning> rpc = await rp.GetByBRIdOnly(BRID);
                    //List<List<string>> gridData1 = new List<List<string>>();

                    //foreach (var icItem in rpc)
                    //{
                    //    string instanceDescription = icItem.Category;
                    //    string instanceDate = icItem.Description;
                    //    List<string> dataValue = new List<string>();
                    //    dataValue.Add(instanceDescription);
                    //    dataValue.Add(instanceDate);
                    //    gridData1.Add(dataValue);
                    //}
                    //Console.WriteLine();

                    ////gridData.Add(dataValue);
                    //int count2 = gridData1.Count;
                    //if (count2 < 5)
                    //{
                    //     addEmptyRows = 5 - count2;
                    //    for (int y = 0; y < addEmptyRows; y++)
                    //    {
                    //        //int sno = count + y + 1;
                    //        List<string> emptyList = new List<string>(4);
                    //        //emptyList.Add(sno.ToString());
                    //        for (int z = 0; z < 2; z++)
                    //        {
                    //            emptyList.Add(" ");
                    //        }
                    //        gridData1.Add(emptyList);
                    //    }
                    //}

                    //int x2 = 1;
                    //foreach (var gridItemList in gridData1)
                    //{

                    //    table.AddCell(x2.ToString());
                    //    x2++;

                    //    foreach (var item in gridItemList)
                    //    {

                    //        cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                    //        table.AddCell(cell);

                    //    }
                    //}

                    //table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);

                    #endregion

                    #region Top Priorities
                    if (mainGroups != "RBU")
                    {
                        table = new PdfPTable(6);
                        table.WidthPercentage = 100;
                        table.DefaultCell.FixedHeight = 35f;
                        //float[] sglTblHdWidths;
                        sglTblHdWidths = new float[6];
                        sglTblHdWidths[0] = 20f;
                        sglTblHdWidths[1] = 250f;
                        sglTblHdWidths[2] = 80f;
                        sglTblHdWidths[3] = 80f;
                        sglTblHdWidths[4] = 70f;
                        sglTblHdWidths[5] = 60f;

                        table.SetWidths(sglTblHdWidths);
                        table.TotalWidth = 570f;
                        table.LockedWidth = true;
                        cell = new PdfPCell(new Phrase("Top Priorities (Enhance Your Own Function)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)))
                        {
                            BackgroundColor = new BaseColor(0, 90, 155),
                            Colspan = 8,
                            HorizontalAlignment = 1
                        };
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Is this an impact on the frontline?", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                        {
                            Rowspan = 1,
                            HorizontalAlignment = Element.ALIGN_CENTER
                        };
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("The groups you need help from", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                        {
                            Rowspan = 1,
                            HorizontalAlignment = Element.ALIGN_CENTER
                        };
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Is it an ongoing effort from last year?", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                        {
                            Rowspan = 1,
                            HorizontalAlignment = Element.ALIGN_CENTER
                        };
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Estimated incremental costs", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                        {
                            Rowspan = 1,
                            HorizontalAlignment = Element.ALIGN_CENTER
                        };
                        table.AddCell(cell);
                        //List<string> dataValue = new List<string>();
                        FocusAreasController fa = new FocusAreasController();
                        List<FocusArea> fac = await fa.GetByBRId(BRID, nextYear);
                        List<List<string>> gridData2 = new List<List<string>>();
                        var requireGroups = string.Empty;
                        foreach (var icItem in fac)
                        {
                            for (var group = 0; group < icItem.FocusAreaRequireGroups.ToArray().Length; group++)
                            {
                                if (group == 1)
                                {
                                    requireGroups += icItem.FocusAreaRequireGroups.ElementAt(group).Name;
                                }
                                else
                                {
                                    requireGroups += ", " + icItem.FocusAreaRequireGroups.ElementAt(group).Name;
                                }
                            }
                            string instanceDescription = icItem.StrategicActions;
                            string instanceFirstChk = icItem.IsImpact==false|| icItem.IsImpact == null ? "" : "\u2713";
                            string instanceSecondChk = requireGroups;
                            string instanceThirdChk = icItem.OngoingEffort == false || icItem.OngoingEffort == null ? "" : "\u2713";
                            decimal instanceCosts = icItem.IncrementalCosts.Any() ?0:Convert.ToDecimal(icItem.IncrementalCosts);
                            List<string> dataValue2 = new List<string>();
                            dataValue2.Add(instanceDescription);
                            dataValue2.Add(instanceFirstChk);
                            dataValue2.Add(instanceSecondChk);
                            dataValue2.Add(instanceThirdChk);
                            dataValue2.Add(instanceCosts.ToString("C"));
                            gridData2.Add(dataValue2);
                        }
                        //gridData.Add(dataValue);
                         count2 = gridData2.Count;
                        if (count2 < 5)
                        {
                            int addEmptyRows = 5 - count2;
                            for (int y = 0; y < addEmptyRows; y++)
                            {
                                //int sno = count + y + 1;
                                List<string> emptyList = new List<string>(4);
                                //emptyList.Add(sno.ToString());
                                for (int z = 0; z < 5; z++)
                                {
                                    emptyList.Add("");
                                }
                                gridData2.Add(emptyList);
                            }
                        }



                        int x6 = 1;
                        foreach (var gridItemList in gridData2)
                        {

                            table.AddCell(x6.ToString());
                            x6++;

                            //   foreach (var item in gridItemList)
                            for (var i = 0; i < gridItemList.Count; i++)
                            {
                                if (i == 1 ||i==2 || i==3)
                                {
                                    if (gridItemList[i] != "")
                                    {
                                        Font zapfdingbats = new Font(Font.FontFamily.ZAPFDINGBATS);
                                        Paragraph p = new Paragraph("\u0033", zapfdingbats);
                                        p.Alignment = Element.ALIGN_CENTER;
                                        cell = new PdfPCell();
                                        cell.AddElement(p);
                                        //cell = new PdfPCell(new Phrase(new Chunk("\u0033", zapfdingbats)));
                                    }
                                    else
                                    {
                                        cell = new PdfPCell(new Phrase(gridItemList[i], FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));
                                    }
                                }
                                else
                                {
                                    cell = new PdfPCell(new Phrase(gridItemList[i], FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));
                                    // new Phrase(new Chunk("\u0033", new Font(Font.FontFamily.ZAPFDINGBATS))));
                                }
                                table.AddCell(cell);

                            }
                        }
                        
                    }

                    else
                    {
                        table = new PdfPTable(2);
                        table.WidthPercentage = 100;
                        table.DefaultCell.FixedHeight = 35f;
                        //float[] sglTblHdWidths;
                        sglTblHdWidths = new float[2];
                        sglTblHdWidths[0] = 20f;
                        sglTblHdWidths[1] = 500f;

                        table.SetWidths(sglTblHdWidths);
                        table.TotalWidth = 570f;
                        table.LockedWidth = true;
                        cell = new PdfPCell(new Phrase("Top Priorities (Enhance Your Own Function)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)))
                        {
                            BackgroundColor = new BaseColor(0, 90, 155),
                            Colspan = 2,
                            HorizontalAlignment = 1
                        };
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                        {
                            Rowspan = 1,
                            HorizontalAlignment = Element.ALIGN_CENTER
                        };
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                        {
                            Rowspan = 1,
                            HorizontalAlignment = Element.ALIGN_CENTER
                        };
                        table.AddCell(cell);


                        //List<string> dataValue = new List<string>();
                        FocusAreasController fa = new FocusAreasController();
                        List<FocusArea> fac = await fa.GetByBRIdOnly(BRID);
                        List<List<string>> gridData2 = new List<List<string>>();

                        foreach (var icItem in fac)
                        {
                            string instanceDescription = icItem.StrategicActions;
                            List<string> dataValue2 = new List<string>();
                            dataValue2.Add(instanceDescription);

                            gridData2.Add(dataValue2);
                        }
                        //gridData.Add(dataValue);
                         count2 = gridData2.Count;
                        if (count2 < 5)
                        {
                            int addEmptyRows = 5 - count2;
                            for (int y = 0; y < addEmptyRows; y++)
                            {
                                //int sno = count + y + 1;
                                List<string> emptyList = new List<string>(4);
                                //emptyList.Add(sno.ToString());
                                for (int z = 0; z < 1; z++)
                                {
                                    emptyList.Add(" ");
                                }
                                gridData2.Add(emptyList);
                            }
                        }



                        int x6 = 1;
                        foreach (var gridItemList in gridData2)
                        {

                            table.AddCell(x6.ToString());
                            x6++;

                            foreach (var item in gridItemList)
                            {

                                cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                                table.AddCell(cell);

                            }
                        }

                        //table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 220, pdfWriter.DirectContent);
                        //if (count2 >= 9)
                        //{
                        //    pdfDoc.NewPage();
                        //}
                       

                    }

                    if (gridData1[0][0].Length > 500 && gridData1[0][0].Length<2000)
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 260, pdfWriter.DirectContent);
                    }
                    else if (gridData1[0][0].Length > 2000)
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 350, pdfWriter.DirectContent);
                    }
                    else
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 180, pdfWriter.DirectContent);

                    }
                    if (count2 >= 7)
                    {
                        pdfDoc.NewPage();
                    }
                    #endregion

                    #region Lease Modification
                    //Lease Modifications

                    table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;

                    sglTblHdWidths = new float[4];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 220f;
                    sglTblHdWidths[2] = 120f;
                    sglTblHdWidths[3] = 220f;
                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase(nextYear + " Lease Modifications", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)))
                    {
                        BackgroundColor = new BaseColor(0, 90, 155),
                        Colspan = 8,
                        HorizontalAlignment = 1
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Lease Modification Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Current Date Of Expiration", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Plan Of Renewal", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    //List<string> dataValue = new List<string>();
                    LeaseModificationController lm = new LeaseModificationController();
                    List<LeaseModification> lmc = await lm.GetByBRIdOnly(BRID);
                    List<List<string>> gridDat = new List<List<string>>();

                    foreach (var icItem in lmc)
                    {
                        string instanceDescription = icItem.Description;
                        string instanceDate = icItem.Date.HasValue ? icItem.Date.Value.ToShortDateString():"";
                        string instancePlanOfRenewal = icItem.PlanOfRenewal;
                        List<string> Value = new List<string>();
                        Value.Add(instanceDescription);
                        Value.Add(instanceDate);
                        Value.Add(instancePlanOfRenewal);
                        gridDat.Add(Value);
                    }
                    Console.WriteLine();

                    //gridData.Add(dataValue);
                    int count6 = gridDat.Count;
                    if (count6 < 5)
                    {
                        var addEmptyRows = 5 - count6;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4);
                            //emptyList.Add(sno.ToString());
                            for (int z = 0; z < 3; z++)
                            {
                                emptyList.Add(" ");
                            }
                            gridDat.Add(emptyList);
                        }
                    }

                    int x3 = 1;
                    foreach (var gridItemList in gridDat)
                    {

                        table.AddCell(x3.ToString());
                        x3++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }
                    if (count2 <= 5)
                    {
                        if( gridData1[0][0].Length > 500 ) {
                            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 540, pdfWriter.DirectContent);
                            pdfDoc.NewPage();
                        }
                       
                        else {
                            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 450, pdfWriter.DirectContent);
                            pdfDoc.NewPage();
                        }
                        
                    }
                    else if (count2 <7)
                    {
                        if (gridData1[0][0].Length > 500 && gridData1[0][0].Length < 2000)
                        {
                            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 540, pdfWriter.DirectContent);
                            pdfDoc.NewPage();
                        }
                        else if (gridData1[0][0].Length > 2000)
                        {
                            pdfDoc.NewPage();
                            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 30, pdfWriter.DirectContent);
                        }
                        else
                        {
                            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 450, pdfWriter.DirectContent);
                            pdfDoc.NewPage();
                        }
                       // table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 540, pdfWriter.DirectContent);
                        //pdfDoc.NewPage();
                    }
                    else
                    {

                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 30, pdfWriter.DirectContent);

                    }
                    

                    #endregion

                    #region Capital Expense
                    table = new PdfPTable(3);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;

                    sglTblHdWidths = new float[3];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 320f;
                    sglTblHdWidths[2] = 80f;
                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase(nextYear + " Capital Expenses", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)))
                    {
                        BackgroundColor = new BaseColor(0, 90, 155),
                        Colspan = 6,
                        HorizontalAlignment = 1
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Capital Expense Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Amount(in $)", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = PdfPCell.ALIGN_CENTER
                    };
                    table.AddCell(cell);

                    CapitalExpenseController ce = new CapitalExpenseController();
                    List<CapitalExpense> cec = await ce.GetByBRIdOnly(BRID);
                    List<List<string>> gridData3 = new List<List<string>>();

                    foreach (var icItem in cec)
                    {
                        string instanceDescription = icItem.Description;
                        decimal instanceAmount =Convert.ToDecimal(icItem.Amount);
                        List<string> dataValue5 = new List<string>();
                        dataValue5.Add(instanceDescription);
                        dataValue5.Add(instanceAmount.ToString("C"));
                        gridData3.Add(dataValue5);
                    }
                    Console.WriteLine();

                    //gridData.Add(dataValue);
                    int count3 = gridData3.Count;
                    if (count3 < 5)
                    {
                        var addEmptyRows = 5 - count3;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4);
                            //emptyList.Add(sno.ToString());
                            for (int z = 0; z < 2; z++)
                            {
                                emptyList.Add(" ");
                            }
                            gridData3.Add(emptyList);
                        }
                    }

                    int x = 1;
                    foreach (var gridItemList in gridData3)
                    {

                        table.AddCell(x.ToString());
                        x++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }
                    //if (BRID == 98 || BRID == 87 || BRID == 61 || BRID == 85 || BRID == 111)
                    //{
                    //    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 620, pdfWriter.DirectContent);
                    //}
                    //else if (BRID == 117 || BRID == 100 || BRID == 122 || BRID == 129 || BRID == 101 || BRID == 94)
                    //{
                    //    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 650, pdfWriter.DirectContent);
                    //}
                    //else if (BRID == 122)
                    //{
                    //    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 670, pdfWriter.DirectContent);
                    //}
                    //else if (BRID == 112 || BRID == 100)
                    //{
                    //    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 700, pdfWriter.DirectContent);
                    //}
                    //else
                    //{
                    //    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 550, pdfWriter.DirectContent);

                    //}
                    if (count2 < 7)
                    {
                        if (gridData1[0][0].Length > 2000)
                        {
                            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 300, pdfWriter.DirectContent);
                        }
                        else
                        {
                            table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 30, pdfWriter.DirectContent);
                        }
                        
                    }
                    else
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 300, pdfWriter.DirectContent);

                    }
                    #endregion

                }

                else if (mainGroups == "CSG" )
                {
                    #region Narrative
                    //2021 Narrative 

                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;
                    float[] sglTblHdWidths;
                    sglTblHdWidths = new float[2];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 500f;

                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase(nextYear + " Narrative", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    cell.BackgroundColor = new BaseColor(0, 90, 155);
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);


                    List<string> dataValue = new List<string>();
                    NarrativeController nc = new NarrativeController();
                    List<Narrative> nac = await nc.GetByBRIdOnly(BRID);

                    List<List<string>> gridData1 = new List<List<string>>();

                    foreach (var icItem in nac)
                    {
                        string instanceDescription = icItem.Description;
                        List<string> dataValue01 = new List<string>();
                        dataValue01.Add(instanceDescription);

                        gridData1.Add(dataValue01);
                    }

                    int count1 = gridData1.Count;
                    if (count1 < 1)
                    {
                        int addEmptyRows = 1;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(3);
                            emptyList.Add(" ");
                            //for (int z = 0; z < 1; z++)
                            //{
                            //    emptyList.Add(" ");
                            //}
                            gridData1.Add(emptyList);
                        }
                    }


                    int x1 = 1;
                    foreach (var gridItemList in gridData1)
                    {

                        table.AddCell(x1.ToString());
                        x1++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }

                    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);


                    #endregion
                    #region Top Priorities
                    table = new PdfPTable(6);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;
                    //float[] sglTblHdWidths;
                    sglTblHdWidths = new float[6];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 250f;
                    sglTblHdWidths[2] = 80f;
                    sglTblHdWidths[3] = 80f;
                    sglTblHdWidths[4] = 70f;
                    sglTblHdWidths[5] = 60f;

                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase("Top Priorities (Enhance Your Own Function )", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)))
                    {
                        BackgroundColor = new BaseColor(0, 90, 155),
                        Colspan = 8,
                        HorizontalAlignment = 1
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Is this an impact on frontLine?", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("The groups you need help from", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Is it an ongoing effort from last year?", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    };
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Estimated incremental costs", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)))
                    {
                        Rowspan = 1,
                        HorizontalAlignment = Element.ALIGN_CENTER
                    };
                    table.AddCell(cell);

                    //List<string> dataValue = new List<string>();
                    FocusAreasController fa = new FocusAreasController();
                    List<FocusArea> fac = await fa.GetByBRIdOnly(BRID);
                    List<List<string>> gridData2 = new List<List<string>>();
                    var requireGroups = string.Empty;
                    foreach (var icItem in fac)
                    {
                        for (var group = 0; group < icItem.FocusAreaRequireGroups.ToArray().Length; group++)
                        {
                            if (group == 1)
                            {
                                requireGroups += icItem.FocusAreaRequireGroups.ElementAt(group).Name;
                            }
                            else
                            {
                                requireGroups += ", " + icItem.FocusAreaRequireGroups.ElementAt(group).Name;
                            }
                        }
                        string instanceDescription = icItem.StrategicActions;
                        string instanceFirstChk = icItem.IsImpact == false || icItem.IsImpact == null ? "" : "\u2713";
                        string instanceSecondChk = requireGroups;
                        string instanceThirdChk = icItem.OngoingEffort == false || icItem.OngoingEffort == null ? "" : "\u2713";
                        decimal instanceCosts = icItem.IncrementalCosts.Any() ? 0 : Convert.ToDecimal(icItem.IncrementalCosts);
                        List<string> dataValue2 = new List<string>();
                        dataValue2.Add(instanceDescription);
                        dataValue2.Add(instanceFirstChk);
                        dataValue2.Add(instanceSecondChk);
                        dataValue2.Add(instanceThirdChk);
                        dataValue2.Add(instanceCosts.ToString("C"));
                        gridData2.Add(dataValue2);
                    }
                    //gridData.Add(dataValue);
                    int count2 = gridData2.Count;
                    if (count2 < 5)
                    {
                        int addEmptyRows = 5 - count2;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4);
                            //emptyList.Add(sno.ToString());
                            for (int z = 0; z < 5; z++)
                            {
                                emptyList.Add("");
                            }
                            gridData2.Add(emptyList);
                        }
                    }



                    int x6 = 1;
                    foreach (var gridItemList in gridData2)
                    {

                        table.AddCell(x6.ToString());
                        x6++;

                        //   foreach (var item in gridItemList)
                        for (var i = 0; i < gridItemList.Count; i++)
                        {
                            if (i == 1 || i == 2 || i == 3)
                            {
                                if (gridItemList[i] != "")
                                {
                                    Font zapfdingbats = new Font(Font.FontFamily.ZAPFDINGBATS);
                                    Paragraph p = new Paragraph("\u0033", zapfdingbats);
                                    p.Alignment = Element.ALIGN_CENTER;
                                    cell = new PdfPCell();
                                    cell.AddElement(p);
                                    // cell = new PdfPCell(new Phrase(new Chunk("\u0033", zapfdingbats)));
                                }
                                else
                                {
                                    cell = new PdfPCell(new Phrase(gridItemList[i], FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));
                                }
                            }
                            else
                            {
                                cell = new PdfPCell(new Phrase(gridItemList[i], FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));
                                // new Phrase(new Chunk("\u0033", new Font(Font.FontFamily.ZAPFDINGBATS))));
                            }
                            table.AddCell(cell);

                        }
                    }

                    if (gridData1[0][0].Length > 700)
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 220, pdfWriter.DirectContent);
                    }
                    else
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 180, pdfWriter.DirectContent);

                    }
                    #endregion
                    #region Lease Modification
                    //table = new PdfPTable(4);
                    //table.WidthPercentage = 100;
                    //table.DefaultCell.FixedHeight = 35f;

                    //sglTblHdWidths = new float[4];
                    //sglTblHdWidths[0] = 120f;
                    //sglTblHdWidths[1] = 820f;
                    //sglTblHdWidths[2] = 520f;
                    //sglTblHdWidths[3] = 820f;
                    //table.SetWidths(sglTblHdWidths);
                    //table.TotalWidth = 570f;
                    //table.LockedWidth = true;
                    //cell = new PdfPCell(new Phrase(nextYear + " Lease Modifications", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    //cell.BackgroundColor = new BaseColor(0, 90, 155);
                    //cell.Colspan = 8;
                    //cell.HorizontalAlignment = 1;
                    //table.AddCell(cell);
                    //cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);
                    //cell = new PdfPCell(new Phrase("Lease Modification Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Current Date Of Expiration", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);
                    //cell = new PdfPCell(new Phrase("Plan Of Renewal", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);
                    //LeaseModificationController lm = new LeaseModificationController();
                    //List<LeaseModification> lmc = await lm.GetByBRIdOnly(BRID);
                    //List<List<string>> gridData = new List<List<string>>();

                    //foreach (var icItem in lmc)
                    //{
                    //    string instanceDescription = icItem.Description;
                    //    string instanceDate = icItem.Date.Value.ToShortDateString();
                    //    string instancePlanOfRenewal = icItem.PlanOfRenewal;
                    //    List<string> dataValue = new List<string>();
                    //    dataValue.Add(instanceDescription);
                    //    dataValue.Add(instanceDate);
                    //    dataValue.Add(instancePlanOfRenewal);
                    //    gridData.Add(dataValue);
                    //}
                    //Console.WriteLine();

                    ////gridData.Add(dataValue);
                    //int count = gridData.Count;
                    //if (count < 5)
                    //{
                    //     addEmptyRows = 5 - count;
                    //    for (int y = 0; y < addEmptyRows; y++)
                    //    {
                    //        //int sno = count + y + 1;
                    //        List<string> emptyList = new List<string>(4);
                    //        //emptyList.Add(sno.ToString());
                    //        for (int z = 0; z < 3; z++)
                    //        {
                    //            emptyList.Add(" ");
                    //        }
                    //        gridData.Add(emptyList);
                    //    }
                    //}

                    //int x = 1;
                    //foreach (var gridItemList in gridData)
                    //{

                    //    table.AddCell(x.ToString());
                    //    x++;

                    //    foreach (var item in gridItemList)
                    //    {

                    //        cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));

                    //        table.AddCell(cell);

                    //    }
                    //}

                    //table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 300, pdfWriter.DirectContent);
                    #endregion
                    #region capital expenses

                    //table = new PdfPTable(3);
                    //table.WidthPercentage = 100;
                    //table.DefaultCell.FixedHeight = 35f;

                    //sglTblHdWidths = new float[3];
                    //sglTblHdWidths[0] = 50f;
                    //sglTblHdWidths[1] = 420f;
                    //sglTblHdWidths[2] = 80f;
                    //table.SetWidths(sglTblHdWidths);
                    //table.TotalWidth = 570f;
                    //table.LockedWidth = true;
                    //cell = new PdfPCell(new Phrase(nextYear + " Capital Expenses", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    //cell.BackgroundColor = new BaseColor(0, 90, 155);
                    //cell.Colspan = 6;
                    //cell.HorizontalAlignment = 1;
                    //table.AddCell(cell);
                    //cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);
                    //cell = new PdfPCell(new Phrase("Capital Expense Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Amount(in $)", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                    //cell.Rowspan = 1;
                    //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    //table.AddCell(cell);

                    //CapitalExpenseController ce = new CapitalExpenseController();
                    //List<CapitalExpense> cec = await ce.GetByBRIdOnly(BRID);
                    //List<List<string>> gridData3 = new List<List<string>>();

                    //foreach (var icItem in cec)
                    //{
                    //    string instanceDescription = icItem.Description;
                    //    string instanceAmount = icItem.Amount;
                    //    List<string> dataValue5 = new List<string>();
                    //    dataValue5.Add(instanceDescription);
                    //    dataValue5.Add(instanceAmount);
                    //    gridData3.Add(dataValue5);
                    //}
                    //Console.WriteLine();

                    ////gridData.Add(dataValue);
                    //int count3 = gridData3.Count;
                    //if (count3 < 5)
                    //{
                    //    var addEmptyRows = 5 - count3;
                    //    for (int y = 0; y < addEmptyRows; y++)
                    //    {
                    //        //int sno = count + y + 1;
                    //        List<string> emptyList = new List<string>(4);


                    //        //emptyList.Add(sno.ToString());
                    //        for (int z = 0; z < 2; z++)
                    //        {
                    //            emptyList.Add(" ");
                    //        }
                    //        gridData3.Add(emptyList);
                    //    }
                    //}

                    //int x7 = 1;
                    //foreach (var gridItemList in gridData3)
                    //{

                    //    table.AddCell(x7.ToString());
                    //    x7++;

                    //    foreach (var item in gridItemList)
                    //    {

                    //        cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                    //        table.AddCell(cell);

                    //    }
                    //}
                    //if (count2 < 9)
                    //{
                    //    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 560, pdfWriter.DirectContent);
                    //}
                    //else
                    //{
                    //    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 150, pdfWriter.DirectContent);

                    //}
                    #endregion
                   
                   
                }

                else if (mainGroups == "IP")
                {
                    #region Narrative
                    //2021 Narrative 

                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;
                    float[] sglTblHdWidths;
                    sglTblHdWidths = new float[2];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 500f;

                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase(nextYear + " Narrative", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    cell.BackgroundColor = new BaseColor(0, 90, 155);
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);


                    List<string> dataValue = new List<string>();
                    NarrativeController nc = new NarrativeController();
                    List<Narrative> nac = await nc.GetByBRIdOnly(BRID);

                    List<List<string>> gridData1 = new List<List<string>>();

                    foreach (var icItem in nac)
                    {
                        string instanceDescription = icItem.Description;
                        List<string> dataValue01 = new List<string>();
                        dataValue01.Add(instanceDescription);

                        gridData1.Add(dataValue01);
                    }

                    int count1 = gridData1.Count;
                    if (count1 < 1)
                    {
                        int addEmptyRows = 1;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(3);
                            emptyList.Add(" ");
                            //for (int z = 0; z < 1; z++)
                            //{
                            //    emptyList.Add(" ");
                            //}
                            gridData1.Add(emptyList);
                        }
                    }


                    int x1 = 1;
                    foreach (var gridItemList in gridData1)
                    {

                        table.AddCell(x1.ToString());
                        x1++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }

                    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);


                    #endregion

                    #region Integrated
                    //nextYear + " IntegratedTopFive

                    table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;
                    //float[] sglTblHdWidths;
                    sglTblHdWidths = new float[2];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 820f;

                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase("Top Priorities That Impact others", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    cell.BackgroundColor = new BaseColor(0, 90, 155);
                    cell.Colspan = 8;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);


                    //List<string> dataValue = new List<string>();
                    IntegratedTopFiveController itf = new IntegratedTopFiveController();
                    List<IntegratedTopFive> itfc = await itf.GetByBRIdOnly(BRID);


                    List<List<string>> gridData2 = new List<List<string>>();

                    foreach (var icItem in itfc)
                    {
                        string instanceDescription = icItem.StrategicActions;
                        List<string> dataValue2 = new List<string>();
                        dataValue2.Add(instanceDescription);

                        gridData1.Add(dataValue2);
                    }
                    Console.WriteLine();

                    //gridData.Add(dataValue);
                    int count2 = gridData2.Count;
                    if (count2 < 5)
                    {
                        var addEmptyRows = 5 - count2;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4)
                            {
                                //emptyList.Add(sno.ToString());
                                //for (int z = 0; z < 3; z++)
                                //{
                                " "
                            };
                            //}
                            gridData2.Add(emptyList);
                        }
                    }

                    int x2 = 1;
                    foreach (var gridItemList in gridData1)
                    {

                        table.AddCell(x2.ToString());
                        x2++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }
                    if (gridData1[0][0].Length > 500)
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 220, pdfWriter.DirectContent);
                    }
                    else
                    {
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 180, pdfWriter.DirectContent);

                    }
                    #endregion
                }
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.ToString());
            }
        }

        private void DrawHeader(Document pdfDoc, out Chunk chunk, out Paragraph line, string year)
        {
            try
            {
                Paragraph pr = new Paragraph();
                chunk = new Chunk(" Business Planning Meeting Report For " + year, FontFactory.GetFont("Arial", 21, Font.BOLD, BaseColor.BLACK));

                //Horizontal Line
                line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                Image ok = Image.GetInstance(Path.Combine(_env.ContentRootPath,"~/Content/Images/dpr-logo.jpg"));
                ok.ScaleAbsoluteWidth(40);
                ok.ScaleAbsoluteHeight(20);
                ok.SetAbsolutePosition(pdfDoc.Right - 35, pdfDoc.Top - 5);
                ok.SetDpi(300, 300);
                pdfDoc.Add(ok);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        #endregion

        #region Excel Convert
        
       
        [Route("api/Export/CreateExcelAsyncForYear/{year}")]
        public async Task<HttpResponseMessage> CreateExcelAsyncForYear([FromRoute] string year)
        {
            try
            {
                HttpResponse Response = HttpContext.Response;
                var wob = await CreateWorkbook(year);
                return wob.Deliver("excelfile.xlsx");
            }
            catch (Exception e)
            {
                throw e; ;

            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Export/CreateExcelGroupWise/{year}")]
        public async Task<HttpResponseMessage> CreateExcelGroupWise([FromUri] string year)
        {
            try
            {
                HttpResponse Response = HttpContext.Response;
                var wob = await CreateWorkbookGroupWise(year);
                return wob.Deliver("excelfile.xlsx");
            }
            catch (Exception e)
            {
                throw e; ;

            }
        }

        #region Single page workbook
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Export/CreateExcel/{BRID}")]
        public async Task<HttpResponseMessage> CreateExcelByBRID([FromUri] int BRID)
        {
            try
            {
                HttpResponse Response = HttpContext.Response;
                var wob = await CreateSingleWorkbook(BRID);
                return wob.Deliver("excelfile.xlsx");
            }
            catch (Exception e)
            {
                throw e; ;

            }
        }

        public async Task<XLWorkbook> CreateSingleWorkbook(int BRID)
        {
            try
            {
                XLWorkbook SourceBook = null;
                var DestinationBook = new XLWorkbook();
                //BPMReferenceController region = new BPMReferenceController();
               using( var dbContext = new bpmdbdevContext()) {
                    var Region = dbContext.Bpmreferences
                          .Where(b => b.Brid == BRID)
                          .Include(b => b.Region).Include(v => v.FocusAreas).Include(c => c.Narratives).Include(m => m.LeaseModifications)
                          .Include(k => k.CapitalExpenses).Include(w => w.IntegratedTopFives)
                      .FirstOrDefault();
                //var Region = region.GetAllByYear("2020",BRID);
                string startPath, filePath = "";
                int i = 1;
                //foreach (var item in RegionList)
                //{

                //    i++;
                    // int yearBRID = item.BRID;
                    string RegionName = Region.Region.RegionName;
                    string GroupName = Region.Region.GroupName;
                    filePath = Path.Combine(_env.ContentRootPath, "~/Content/Group2Template.xlsx");
                    SourceBook = new XLWorkbook(filePath);
                    var a = SourceBook.Worksheets.Count;
                    IXLWorksheet SourceSheet;
                    if (GroupName == "RBU" || GroupName == "SP" || GroupName == "WVE" || GroupName == "CM")
                    {
                        SourceSheet = SourceBook.Worksheet(5);
                    }
                    else if (GroupName == "IP")
                    {
                        SourceSheet = SourceBook.Worksheet(4);
                        if (RegionName.Contains("Project Life Cycle"))
                        {
                            RegionName = RegionName.Split('/')[0];
                        }
                        RegionName = RegionName.Replace(" ", "");
                        //if (RegionName.Contains("Integrated"))
                        //{
                        //    SourceSheet = SourceBook.Worksheet(4);
                        //    if (RegionName.Contains("Project Life Cycle"))
                        //    {
                        //        RegionName = RegionName.Split('/')[0];
                        //    }
                        //    //else { }
                        //    RegionName = RegionName.Replace(" ", "");
                        //}
                    }
                    else if (GroupName == "CSG")
                    {
                        SourceSheet = SourceBook.Worksheet(3);
                    }

                    else
                    {
                        SourceSheet = SourceBook.Worksheet(5);
                    }
                    SourceSheet.CopyTo(DestinationBook, RegionName);
                    var DestinationSheet = DestinationBook.Worksheet(i);
                    DestinationSheet.Cell(5, 3).Value = RegionName;
                    if (BRID != 0)
                    {
                        if (GroupName == "RBU" || GroupName == "SP" || GroupName == "WVE" || GroupName == "CM")
                        {
                            //NarrativeController nc = new NarrativeController();
                            //List<Narrative> naList = await nc.GetByBRId(BRID, Region.Year);
                            int m = 0;
                            foreach (var n in Region.Narratives)
                            {
                                DestinationSheet.Cell(10 + m, 2).Value = m + 1;
                                DestinationSheet.Cell(10 + m, 3).Value = n.Description;
                                m++;
                            }

                            //RecessionPlanningController rpc = new RecessionPlanningController();
                            //List<RecessionPlanning> rpList = await rpc.GetByBRId(item.BRID, year);
                            //int h = 0;
                            ////Recession Planning
                            //foreach (var rp in rpList)
                            //{
                            //    DestinationSheet.Cell(11 + h, 2).Value = h + 1;
                            //    DestinationSheet.Cell(11 + h, 3).Value = rp.Description;
                            //    DestinationSheet.Cell(11 + h, 4).Value = rp.Category;
                            //    h++;
                            //}

                            //Focus Area
                            //FocusAreasController fac = new FocusAreasController();
                            //List<FocusArea> faList = await fac.GetByBRId(BRID, Region.Year);
                            if (GroupName != "RBU")
                            {
                                int j = 0;


                                foreach (var fa in Region.FocusAreas)
                                {
                                    string requireGroups = "";
                                    string supportItf = "";
                                    for (var group = 0; group < fa.FocusAreaRequireGroups.ToArray().Length; group++)
                                    {
                                        if (group == 1)
                                        {
                                            requireGroups += fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                        }
                                        else
                                        {
                                            requireGroups += ", " + fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                        }
                                    }
                                    for (var option = 0; option < fa.FocusAreaSupportItfs.ToArray().Length; option++)
                                    {
                                        if (option == 1)
                                        {
                                            supportItf += fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                        }
                                        else
                                        {
                                            supportItf += ", " + fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                        }
                                    }
                                    DestinationSheet.Cell(15 + j, 2).Value = j + 1;
                                    DestinationSheet.Cell(15 + j, 3).Value = fa.StrategicActions;
                                    DestinationSheet.Cell(15 + j, 4).Value = fa.IsImpact == null || fa.IsImpact == false ? "" : char.ConvertFromUtf32(0x00002713);
                                    DestinationSheet.Cell(15 + j, 5).Value = requireGroups;
                                    DestinationSheet.Cell(15 + j, 6).Value = supportItf;
                                    DestinationSheet.Cell(15 + j, 7).Value = fa.OngoingEffort == null || fa.OngoingEffort == false ? "" : char.ConvertFromUtf32(0x00002713);
                                    DestinationSheet.Cell(15 + j, 8).Value = fa.IncrementalCosts != null || fa.IncrementalCosts != "" ? string.Format("{0:#,###0}", fa.IncrementalCosts) : fa.IncrementalCosts;
                                    DestinationSheet.Cell(15 + j, 8).Style.NumberFormat.Format = "[$$-en-US] #,##0.00"; ;
                                    DestinationSheet.Cell(15 + j, 8).DataType = (XLDataType)CellValues.Number;
                                    //fa.RequireHelp.HasValue ? fa.RequireHelp.Value.ToString() : "";
                                    DestinationSheet.Row(15 + j).InsertRowsBelow(1);
                                    j++;
                                }
                            }
                            else
                            {
                                int j = 0;
                                foreach (var fa in Region.FocusAreas)
                                {
                                    DestinationSheet.Cell(15 + j, 2).Value = j + 1;
                                    DestinationSheet.Cell(15 + j, 3).Value = fa.StrategicActions;
                                    DestinationSheet.Row(15 + j).InsertRowsBelow(1);
                                    j++;
                                }
                            }

                            //Lease Modification
                            //LeaseModificationController lmc = new LeaseModificationController();
                            //List<LeaseModification> lmList = await lmc.GetByBRId(BRID, Region.Year);
                            int k = 0;
                            foreach (var lm in Region.LeaseModifications)
                            {
                                DestinationSheet.Cell(15 + (Region.FocusAreas.Count - 1) + 7 + k, 2).Value = k + 1;
                                DestinationSheet.Cell(15 + (Region.FocusAreas.Count - 1) + 7 + k, 3).Value = lm.Description;
                                DestinationSheet.Cell(15 + (Region.FocusAreas.Count - 1) + 7 + k, 4).Value = lm.Date.HasValue ? lm.Date.Value.ToString("MM/dd/yyyy") : "";
                                DestinationSheet.Cell(15 + (Region.FocusAreas.Count - 1) + 7 + k, 5).Value = lm.PlanOfRenewal;
                                k++;
                            }
                            var leaseLastRow = 15 + (Region.FocusAreas.Count - 1) + 7 + k;
                            //Capital Expense
                            //CapitalExpenseController cec = new CapitalExpenseController();
                            //List<CapitalExpense> ceList = await cec.GetByBRId(BRID, Region.Year);
                            int t = 0;
                            foreach (var cm in Region.CapitalExpenses)
                            {
                                DestinationSheet.Cell(leaseLastRow + 9 + t, 2).Value = t + 1;
                                DestinationSheet.Cell(leaseLastRow + 9 + t, 3).Value = cm.Description;
                                DestinationSheet.Cell(leaseLastRow + 9 + t, 4).Value = string.Format("{0:#,###0}", cm.Amount);
                                DestinationSheet.Cell(leaseLastRow + 9 + t, 4).Style.NumberFormat.Format = "[$$-en-US] #,##0.00";
                                DestinationSheet.Cell(leaseLastRow + 9 + t, 4).DataType = (XLDataType)CellValues.Number;
                                t++;
                            }
                        }

                        else if (GroupName == "IP")
                        {
                            NarrativeController nc = new NarrativeController();
                            List<Narrative> naList = await nc.GetByBRId(BRID, Region.Year);
                            int m = 0;
                            foreach (var n in naList)
                            {
                                DestinationSheet.Cell(10 + m, 2).Value = m + 1;
                                DestinationSheet.Cell(10 + m, 3).Value = n.Description;
                                m++;
                            }

                            //IntegratedTopFiveController itc = new IntegratedTopFiveController();
                            //List<IntegratedTopFive> itList = await itc.GetByBRId(BRID, Region.Year);
                            int h = 0;
                            foreach (var fa in Region.IntegratedTopFives)
                            {
                                DestinationSheet.Cell(15 + h, 2).Value = h + 1;
                                DestinationSheet.Cell(15 + h, 3).Value = fa.StrategicActions;
                                h++;
                            }
                        }
                        else
                        {
                            //NarrativeController nc = new NarrativeController();
                            //List<Narrative> naList = await nc.GetByBRId(BRID, Region.Year);
                            int m = 0;
                            foreach (var n in Region.Narratives)
                            {
                                DestinationSheet.Cell(10 + m, 2).Value = m + 1;
                                DestinationSheet.Cell(10 + m, 3).Value = n.Description;
                                m++;
                            }

                            //Focus Area
                            //FocusAreasController fac = new FocusAreasController();
                            //List<FocusArea> faList = await fac.GetByBRId(BRID, Region.Year);
                            int j = 0;

                            foreach (var fa in Region.FocusAreas)
                            {
                                string requireGroups = string.Empty;
                                string supportItf = string.Empty;
                                for (var group = 0; group < fa.FocusAreaRequireGroups.ToArray().Length; group++)
                                {
                                    if (group == 1)
                                    {
                                        requireGroups += fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                    }
                                    else
                                    {
                                        requireGroups += ", " + fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                    }
                                }

                                for (var option = 0; option < fa.FocusAreaSupportItfs.ToArray().Length; option++)
                                {
                                    if (option == 1)
                                    {
                                        supportItf += fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                    }
                                    else
                                    {
                                        supportItf += ", " + fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                    }
                                }
                                DestinationSheet.Cell(15 + j, 2).Value = j + 1;
                                DestinationSheet.Cell(15 + j, 3).Value = fa.StrategicActions;
                                DestinationSheet.Cell(15 + j, 4).Value = fa.IsImpact == null || fa.IsImpact == false ? "" : char.ConvertFromUtf32(0x00002713);
                                DestinationSheet.Cell(15 + j, 5).Value = requireGroups;
                                DestinationSheet.Cell(15 + j, 6).Value = supportItf;
                                DestinationSheet.Cell(15 + j, 7).Value = fa.OngoingEffort == null || fa.OngoingEffort == false ? "" : char.ConvertFromUtf32(0x00002713);
                                DestinationSheet.Cell(15 + j, 8).Value = fa.IncrementalCosts != null || fa.IncrementalCosts != "" ? string.Format("{0:#,###0}", fa.IncrementalCosts) : fa.IncrementalCosts; ;
                                DestinationSheet.Cell(15 + j, 8).Style.NumberFormat.Format = "[$$-en-US] #,##0.00";
                                DestinationSheet.Cell(15 + j, 8).DataType = (XLDataType)CellValues.Number;
                                j++;
                            }

                            //Lease Modification
                            //LeaseModificationController lmc = new LeaseModificationController();
                            //List<LeaseModification> lmList = await lmc.GetByBRId(item.BRID, year);
                            //int k = 0;
                            //foreach (var lm in lmList)
                            //{
                            //    DestinationSheet.Cell(21 + k, 2).Value = k + 1;
                            //    DestinationSheet.Cell(21 + k, 3).Value = lm.Description;
                            //    DestinationSheet.Cell(21 + k, 4).Value = lm.Date.Value.ToString("MM/dd/yyyy");
                            //    DestinationSheet.Cell(21 + k, 5).Value = lm.PlanOfRenewal;
                            //    k++;
                            //}
                            ////Capital Expense
                            //CapitalExpenseController cec = new CapitalExpenseController();
                            //List<CapitalExpense> ceList = await cec.GetByBRId(item.BRID, year);
                            //k = 0;
                            //foreach (var cm in ceList)
                            //{
                            //    DestinationSheet.Cell(30 + k, 2).Value = k + 1;
                            //    DestinationSheet.Cell(30 + k, 3).Value = cm.Description;
                            //    DestinationSheet.Cell(30 + k, 4).Value = string.Format("{0:#,###0}", cm.Amount);
                            //    k++;
                            //}
                            //}
                        }
                    }
                }
                return DestinationBook;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        #endregion

        #region old format
        public async Task<XLWorkbook> CreateWorkbook(string year)
        {
            try
            {
                XLWorkbook SourceBook = null;
                var DestinationBook = new XLWorkbook();
                RegionController region = new RegionController();
                List<ViewRegion> RegionList = await region.GetBpmWithRegionByYear(year);
                string startPath, filePath = "";
                int i = 0;
                foreach (var item in RegionList)
                {

                    i++;
                    // int yearBRID = item.BRID;
                    string RegionName = item.RegionName;
                    string GroupName = item.GroupName;
                    filePath = Path.Combine(_env.ContentRootPath, "~/Content/Group2Template.xlsx");
                    SourceBook = new XLWorkbook(filePath);
                    var a = SourceBook.Worksheets.Count;
                    IXLWorksheet SourceSheet;
                    if (GroupName == "RBU" || GroupName == "SP" || GroupName == "WVE" || GroupName == "CM")
                    {
                        SourceSheet = SourceBook.Worksheet(5);
                    }
                    else if (GroupName == "IP")
                    {
                        SourceSheet = SourceBook.Worksheet(4);
                        if (RegionName.Contains("Project Life Cycle"))
                        {
                            RegionName = RegionName.Split('/')[0];
                        }
                        RegionName = RegionName.Replace(" ", "");
                        //if (RegionName.Contains("Integrated"))
                        //{
                        //    SourceSheet = SourceBook.Worksheet(4);
                        //    if (RegionName.Contains("Project Life Cycle"))
                        //    {
                        //        RegionName = RegionName.Split('/')[0];
                        //    }
                        //    //else { }
                        //    RegionName = RegionName.Replace(" ", "");
                        //}
                    }
                    else if (GroupName == "CSG")
                    {
                        SourceSheet = SourceBook.Worksheet(3);
                    }
                    
                    else
                    {
                        SourceSheet = SourceBook.Worksheet(5);
                    }
                    SourceSheet.CopyTo(DestinationBook, RegionName);
                        var DestinationSheet = DestinationBook.Worksheet(i);
                        DestinationSheet.Cell(5, 3).Value = RegionName;
                        if (item.BRID != 0)
                        {
                            if (GroupName == "RBU" || GroupName == "SP"||GroupName == "WVE"|| GroupName == "CM")
                            {
                                NarrativeController nc = new NarrativeController();
                                List<Narrative> naList = await nc.GetByBRId(item.BRID, year);
                                int m = 0;
                                foreach (var n in naList)
                                {
                                    DestinationSheet.Cell(10 + m, 2).Value = m + 1;
                                    DestinationSheet.Cell(10 + m, 3).Value = n.Description;
                                    m++;
                                }

                            //RecessionPlanningController rpc = new RecessionPlanningController();
                            //List<RecessionPlanning> rpList = await rpc.GetByBRId(item.BRID, year);
                            //int h = 0;
                            ////Recession Planning
                            //foreach (var rp in rpList)
                            //{
                            //    DestinationSheet.Cell(11 + h, 2).Value = h + 1;
                            //    DestinationSheet.Cell(11 + h, 3).Value = rp.Description;
                            //    DestinationSheet.Cell(11 + h, 4).Value = rp.Category;
                            //    h++;
                            //}

                            //Focus Area
                            FocusAreasController fac = new FocusAreasController();
                            List<FocusArea> faList = await fac.GetByBRId(item.BRID, year);
                            if (GroupName != "RBU")
                            {
                                int j = 0;
                                string requireGroups = "";
                                string supportItf = "";

                                foreach (var fa in faList)
                                {
                                    for (var group=0;group< fa.FocusAreaRequireGroups.ToArray().Length;group++)
                                    {
                                        if (group == 1)
                                        {
                                            requireGroups += fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                        }
                                        else
                                        {
                                            requireGroups += ", " + fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                        }
                                    }

                                    for (var option = 0; option < fa.FocusAreaSupportItfs.ToArray().Length; option++)
                                    {
                                        if (option == 1)
                                        {
                                            supportItf += fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                        }
                                        else
                                        {
                                            supportItf += ", " + fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                        }
                                    }
                                    DestinationSheet.Cell(15 + j, 2).Value = j + 1;
                                    DestinationSheet.Cell(15 + j, 3).Value = fa.StrategicActions;
                                    DestinationSheet.Cell(15 + j, 4).Value = fa.IsImpact == null || fa.IsImpact == false ? "" : char.ConvertFromUtf32(0x00002713);
                                    DestinationSheet.Cell(15 + j, 5).Value = requireGroups;
                                    DestinationSheet.Cell(15 + j, 6).Value = supportItf;
                                    DestinationSheet.Cell(15 + j, 7).Value = fa.OngoingEffort == null || fa.OngoingEffort == false ? "" : char.ConvertFromUtf32(0x00002713);
                                    DestinationSheet.Cell(15 + j, 8).Value = fa.IncrementalCosts != null || fa.IncrementalCosts != "" ? string.Format("{0:#,###0}", fa.IncrementalCosts) : fa.IncrementalCosts; ;
                                    DestinationSheet.Cell(15 + j, 8).Style.NumberFormat.Format = "[$$-en-US] #,##0.00";
                                    DestinationSheet.Cell(15 + j, 8).DataType = (XLDataType)CellValues.Number;
                                    j++;
                                }
                            }
                            else
                            {
                                int j = 0;
                                foreach (var fa in faList)
                                {
                                    DestinationSheet.Cell(15 + j, 2).Value = j + 1;
                                    DestinationSheet.Cell(15 + j, 3).Value = fa.StrategicActions;
                                    DestinationSheet.Row(15 + j).InsertRowsBelow(1);
                                    j++;
                                }
                            }

                            //Lease Modification
                            LeaseModificationController lmc = new LeaseModificationController();
                            List<LeaseModification> lmList = await lmc.GetByBRId(item.BRID, year);
                            int k = 0;
                            foreach (var lm in lmList)
                            {
                                DestinationSheet.Cell(15 + (faList.Count - 1) + 7 + k, 2).Value = k + 1;
                                DestinationSheet.Cell(15 + (faList.Count - 1) + 7 + k, 3).Value = lm.Description;
                                DestinationSheet.Cell(15 + (faList.Count - 1) + 7 + k, 4).Value = lm.Date.HasValue ? lm.Date.Value.ToString("MM/dd/yyyy") : "";
                                DestinationSheet.Cell(15 + (faList.Count - 1) + 7 + k, 5).Value = lm.PlanOfRenewal;
                                k++;
                            }
                            var leaseLastRow = 15 + (faList.Count - 1) + 7 + k;
                            //Capital Expense
                            CapitalExpenseController cec = new CapitalExpenseController();
                                List<CapitalExpense> ceList = await cec.GetByBRId(item.BRID, year);
                                int t = 0;
                                foreach (var cm in ceList)
                                {
                                    DestinationSheet.Cell(leaseLastRow+9 + t, 2).Value = t + 1;
                                    DestinationSheet.Cell(leaseLastRow +9+ t, 3).Value = cm.Description;
                                    DestinationSheet.Cell(leaseLastRow +9 + t, 4).Value = string.Format("{0:#,###0}", cm.Amount);
                                DestinationSheet.Cell(leaseLastRow + 9 + t, 4).Style.NumberFormat.Format= "[$$-en-US] #,##0.00";
                                DestinationSheet.Cell(leaseLastRow + 9 + t, 4).DataType = (XLDataType)CellValues.Number;
                                t++;
                                }
                            }

                            else if (GroupName == "IP")
                            {
                                NarrativeController nc = new NarrativeController();
                                List<Narrative> naList = await nc.GetByBRId(item.BRID, year);
                                int m = 0;
                                foreach (var n in naList)
                                {
                                    DestinationSheet.Cell(10 + m, 2).Value = m + 1;
                                    DestinationSheet.Cell(10 + m, 3).Value = n.Description;
                                    m++;
                                }

                                IntegratedTopFiveController itc = new IntegratedTopFiveController();
                                List<IntegratedTopFive> itList = await itc.GetByBRId(item.BRID, year);
                                int h = 0;
                                foreach (var fa in itList)
                                {
                                    DestinationSheet.Cell(15 + h, 2).Value = h + 1;
                                    DestinationSheet.Cell(15 + h, 3).Value = fa.StrategicActions;
                                    h++;
                                }
                            }
                            else
                            {
                                NarrativeController nc = new NarrativeController();
                                List<Narrative> naList = await nc.GetByBRId(item.BRID, year);
                                int m = 0;
                                foreach (var n in naList)
                                {
                                    DestinationSheet.Cell(10 + m, 2).Value = m + 1;
                                    DestinationSheet.Cell(10 + m, 3).Value = n.Description;
                                    m++;
                                }

                                //Focus Area
                                FocusAreasController fac = new FocusAreasController();
                                List<FocusArea> faList = await fac.GetByBRId(item.BRID, year);
                                int j = 0;
                                string requireGroups = string.Empty;
                            string supportItf = string.Empty;
                                foreach (var fa in faList)
                                {
                                for (var group = 0; group < fa.FocusAreaRequireGroups.ToArray().Length; group++)
                                {
                                    if (group == 1)
                                    {
                                        requireGroups += fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                    }
                                    else
                                    {
                                        requireGroups += ", " + fa.FocusAreaRequireGroups.ElementAt(group).Name;
                                    }
                                }
                                for (var option = 0; option < fa.FocusAreaSupportItfs.ToArray().Length; option++)
                                {
                                    if (option == 1)
                                    {
                                        supportItf += fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                    }
                                    else
                                    {
                                        supportItf += ", " + fa.FocusAreaSupportItfs.ElementAt(option).Name;
                                    }
                                }
                                DestinationSheet.Cell(15 + j, 2).Value = j + 1;
                                DestinationSheet.Cell(15 + j, 3).Value = fa.StrategicActions;
                                DestinationSheet.Cell(15 + j, 4).Value = fa.IsImpact == null || fa.IsImpact == false ? "" : char.ConvertFromUtf32(0x00002713);
                                DestinationSheet.Cell(15 + j, 5).Value = requireGroups;
                                DestinationSheet.Cell(15 + j, 6).Value = supportItf;
                                DestinationSheet.Cell(15 + j, 7).Value = fa.OngoingEffort == null || fa.OngoingEffort == false ? "" : char.ConvertFromUtf32(0x00002713);
                                DestinationSheet.Cell(15 + j, 8).Value = fa.IncrementalCosts != null || fa.IncrementalCosts != "" ? string.Format("{0:#,###0}", fa.IncrementalCosts) : fa.IncrementalCosts; ;
                                DestinationSheet.Cell(15 + j, 8).Style.NumberFormat.Format = "[$$-en-US] #,##0.00";
                                DestinationSheet.Cell(15 + j, 8).DataType = (XLDataType)CellValues.Number;
                                j++;
                            }

                                //Lease Modification
                                //LeaseModificationController lmc = new LeaseModificationController();
                                //List<LeaseModification> lmList = await lmc.GetByBRId(item.BRID, year);
                                //int k = 0;
                                //foreach (var lm in lmList)
                                //{
                                //    DestinationSheet.Cell(21 + k, 2).Value = k + 1;
                                //    DestinationSheet.Cell(21 + k, 3).Value = lm.Description;
                                //    DestinationSheet.Cell(21 + k, 4).Value = lm.Date.Value.ToString("MM/dd/yyyy");
                                //    DestinationSheet.Cell(21 + k, 5).Value = lm.PlanOfRenewal;
                                //    k++;
                                //}
                                ////Capital Expense
                                //CapitalExpenseController cec = new CapitalExpenseController();
                                //List<CapitalExpense> ceList = await cec.GetByBRId(item.BRID, year);
                                //k = 0;
                                //foreach (var cm in ceList)
                                //{
                                //    DestinationSheet.Cell(30 + k, 2).Value = k + 1;
                                //    DestinationSheet.Cell(30 + k, 3).Value = cm.Description;
                                //    DestinationSheet.Cell(30 + k, 4).Value = string.Format("{0:#,###0}", cm.Amount);
                                //    k++;
                                //}
                            }
                        }
                    }
                return DestinationBook;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        #endregion

        #region new format of excel
        public async Task<XLWorkbook> CreateWorkbookGroupWise(string year)
        {
            try
            {
                var opts = new Dictionary<string, int>
                    {
                        { "IP", 1 },
                        { "CSG",2 },
                        { "CM", 3 },
                        { "SP", 4 },
                        { "WVE", 5 },
                        { "RBU", 6 }
                    };

                var requiredGroupsPosition = new Dictionary<string, int>
                    {
                        { "None", 5 },
                        { "Integrated - Project Life Cycle/Do Work", 6 },
                        { "Integrated - Get Work",7 },
                        { "Integrated - Take Care of People", 8 },
                        { "Human Resources", 9 },
                        { "Learning & Development", 10 },
                        { "Talent", 11 },
                        { "Recruiting", 12 },
                        { "Global Social Responsibility",13 },
                        { "Diversity & Inclusion", 14 },
                        { "Sustainability", 15 },
                        { "Operations",16 },
                        { "Preconstruction", 17 },
                        { "Community Initiatives", 18 },
                        { "Construction Technologies",19 },
                        { "Innovation", 20 },
                        { "Information Technology", 21 },
                        { "Large Projects Group", 22 },
                        { "Self Perform Work", 23 },
                        { "MEP", 24 },
                        { "PSPP",25 },
                        { "Risk Management", 26 },
                        { "Safety", 27 },
                        { "Insurance", 28 },
                        { "Quality", 29 },
                        { "SSG", 30 },
                        { "Finance & Accounting",31 },
                        { "Pre-fabrication", 32},
                        { "VDC", 33 },
                        { "Data and Development", 34 },
                        { "Sales & Marketing", 35 },
                        { "Communications & Brand", 36 },
                        { "Supply Chain",37 },
                        { "Commercial Core Market Group", 38 },
                        { "Healthcare Core Market Group", 39 },
                        { "Life Sciences Core Market Group", 40 },
                        { "Advance Technology Core Market Group", 41 },
                        { "Facebook Account Group", 42 },
                        { "Higher Education Core Market Group",43 },
                        { "OES", 44 },
                        { "EIG", 45 },
                        { "vConstruct", 46 },
                        { "WND Ventures", 47 },
                        { "VueOps", 48 },
                        { "DBC",49 },
                        { "SurePods", 50 },
                        { "Northwest Region", 51 },
                        { "Southwest Region", 52 },
                        { "Central Region", 53 },
                        { "Northeast Region", 54 },
                        { "Southeast Region", 55 },
                        { "International Region", 56 },
                        { "Supplier Diversity", 57 },
                        { "Craft TCOP", 58 }

                    };

                var supportITFPosition = new Dictionary<string, int>
                    {
                        { "None", 61 },
                        { "GW - Stay Customer Focused", 62 },
                        { "GW - Win the Right Work",63 },
                        { "GW - Leverage Get Work Data", 64},
                        { "DW - Individual commitment to EHS", 65},
                        { "DW - Quality Challenge Roll-out", 66 },
                        { "DW - Planning It Right", 67 },
                        { "DW - Production Management", 68 },
                        { "TCOP - Onboarding",69 },
                        { "TCOP - Builder Mindset and Skillset", 70 },
                        { "TCOP - Team Development", 71 },
                    };

                XLWorkbook SourceBook = null;
                var DestinationBook = new XLWorkbook();
                using (var dbContext = new bpmdbdevContext())
                {
                    var Region = dbContext.Bpmreferences
                             .Where(b => b.Year == year)
                            .Include(v => v.FocusAreas).Include(c => c.Narratives).Include(m => m.LeaseModifications)
                             .Include(k => k.CapitalExpenses).Include(w => w.IntegratedTopFives)
                              .Include(b => b.Region).Where(c => c.Year == year && c.IsDeleted != true).ToList();
                    var RegionList = Region.OrderBy(m => m.Region.SerialNo).GroupBy(v => v.Region.GroupName).ToList();
                   
                    string startPath, filePath = "";
                    int i = 1;
                    filePath = Path.Combine(_env.ContentRootPath,"~/Content/Group1Template.xlsx");
                    SourceBook = new XLWorkbook(filePath);
                    IXLWorksheet SourceSheet;
                    IXLWorksheet CombinedSheet;
                    CombinedSheet = SourceBook.Worksheet(7);
                    CombinedSheet.CopyTo(DestinationBook, "Combined", 1);
                    var destinationCombinedSheet = DestinationBook.Worksheet(1);
                    var combinedRowIndex = 3;
                    foreach (var item in RegionList)
                    {
                        i++;
                        SourceSheet = SourceBook.Worksheet(opts[item.Key]);
                        SourceSheet.CopyTo(DestinationBook, item.Key);
                        var DestinationSheet = DestinationBook.Worksheet(i);
                        var regionIndex = 3;

                        foreach (var groupedItem in item)
                        {

                            string RegionName = groupedItem.Region.RegionName;

                            if (groupedItem.Brid != 0)
                            {
                                DestinationSheet.Cell(regionIndex, 2).Value = RegionName;
                                destinationCombinedSheet.Cell(combinedRowIndex, 2).Value = RegionName;
                                //NarrativeController nc = new NarrativeController();
                                //List<Narrative> naList = await nc.GetByBRId(groupedItem.BRID, year);
                                Narrative nalist = groupedItem.Narratives.FirstOrDefault();
                                //dbContext.Narratives.Where(v => v.Year == year & v.BRID == groupedItem.BRID).FirstOrDefault();
                                int m = 0;
                                if (nalist != null)
                                {
                                    DestinationSheet.Cell(regionIndex + m, 3).Value = nalist.Description;
                                    destinationCombinedSheet.Cell(combinedRowIndex + m, 3).Value = nalist.Description;
                                }

                                //Focus Area
                                int j = 1;
                                if (groupedItem.Region.GroupName != "IP")
                                {
                                    List<FocusArea> faList = groupedItem.FocusAreas.ToList();
                                    //dbContext.FocusAreas.Where(v => v.Year == year & v.BRID == groupedItem.BRID).ToList();

                                    j = 1;
                                    foreach (var fa in faList)
                                    {

                                        foreach (var rg in fa.FocusAreaRequireGroups)
                                        {
                                            if (requiredGroupsPosition.ContainsKey(rg.Name))
                                            {
                                                DestinationSheet.Cell(regionIndex + j, requiredGroupsPosition[rg.Name]).Value = char.ConvertFromUtf32(0x00002713);
                                                destinationCombinedSheet.Cell(combinedRowIndex + j, requiredGroupsPosition[rg.Name]).Value = char.ConvertFromUtf32(0x00002713);
                                            }
                                        }

                                        foreach (var st in fa.FocusAreaSupportItfs)
                                        {
                                            if (supportITFPosition.ContainsKey(st.Name))
                                            {
                                                DestinationSheet.Cell(regionIndex + j, supportITFPosition[st.Name]).Value = char.ConvertFromUtf32(0x00002713);
                                                destinationCombinedSheet.Cell(combinedRowIndex + j, supportITFPosition[st.Name]).Value = char.ConvertFromUtf32(0x00002713);
                                            }
                                        }
                                        DestinationSheet.Cell(regionIndex + j, 2).Value = RegionName;
                                        DestinationSheet.Cell(regionIndex + j, 4).Value = fa.StrategicActions;
                                        DestinationSheet.Cell(regionIndex + j, 59).Value = fa.IsImpact == null || fa.IsImpact == false ? "" : char.ConvertFromUtf32(0x00002713);
                                        DestinationSheet.Cell(regionIndex + j, 60).Value = fa.OngoingEffort == null || fa.OngoingEffort == false ? "" : char.ConvertFromUtf32(0x00002713);
                                        DestinationSheet.Cell(regionIndex + j, 72).Value = fa.IncrementalCosts != null || fa.IncrementalCosts != "" ? string.Format("{0:#,###0}", fa.IncrementalCosts) : fa.IncrementalCosts;
                                        DestinationSheet.Cell(regionIndex + j, 72).Style.NumberFormat.Format = "[$$-en-US] #,##0.00"; ;
                                        DestinationSheet.Cell(regionIndex + j, 72).DataType = (XLDataType)CellValues.Number;
                                        DestinationSheet.Row(regionIndex + j).InsertRowsBelow(1);

                                        destinationCombinedSheet.Cell(combinedRowIndex + j, 2).Value = RegionName;
                                        destinationCombinedSheet.Cell(combinedRowIndex + j, 4).Value = fa.StrategicActions;
                                        destinationCombinedSheet.Cell(combinedRowIndex + j, 59).Value = fa.IsImpact == null || fa.IsImpact == false ? "" : char.ConvertFromUtf32(0x00002713);
                                        destinationCombinedSheet.Cell(combinedRowIndex + j, 60).Value = fa.OngoingEffort == null || fa.OngoingEffort == false ? "" : char.ConvertFromUtf32(0x00002713);
                                        destinationCombinedSheet.Cell(combinedRowIndex + j, 72).Value = fa.IncrementalCosts != null || fa.IncrementalCosts != "" ? string.Format("{0:#,###0}", fa.IncrementalCosts) : fa.IncrementalCosts;
                                        destinationCombinedSheet.Cell(combinedRowIndex + j, 72).Style.NumberFormat.Format = "[$$-en-US] #,##0.00"; ;
                                        destinationCombinedSheet.Cell(combinedRowIndex + j, 72).DataType = (XLDataType)CellValues.Number;
                                        destinationCombinedSheet.Row(combinedRowIndex + j).InsertRowsBelow(1);
                                        j++;
                                    }
                                }
                                else if (groupedItem.Region.GroupName == "IP")
                                {
                                    IntegratedTopFiveController itc = new IntegratedTopFiveController();
                                    List<IntegratedTopFive> itList = groupedItem.IntegratedTopFives.ToList();
                                    //await itc.GetByBRId(groupedItem.BRID, year);
                                    int h = 1;
                                    foreach (var fa in itList)
                                    {
                                        DestinationSheet.Cell(regionIndex + h, 4).Value = fa.StrategicActions;
                                        destinationCombinedSheet.Cell(combinedRowIndex + h, 4).Value = fa.StrategicActions;
                                        h++;
                                    }
                                }
                                if (groupedItem.Region.GroupName != "IP" && groupedItem.Region.GroupName != "CSG")
                                {

                                    //Lease Modification
                                    //LeaseModificationController lmc = new LeaseModificationController();
                                    List<LeaseModification> lmList = groupedItem.LeaseModifications.ToList();
                                    //await lmc.GetByBRId(groupedItem.BRID, year);
                                    int k = 1;
                                    foreach (var lm in lmList)
                                    {
                                        DestinationSheet.Cell(regionIndex + k, 73).Value = lm.Description;
                                        DestinationSheet.Cell(regionIndex + k, 74).Value = lm.Date.HasValue? lm.Date.Value.ToString("MM/dd/yyyy"):"";
                                        DestinationSheet.Cell(regionIndex + k, 75).Value = lm.PlanOfRenewal;

                                        destinationCombinedSheet.Cell(combinedRowIndex + k, 73).Value = lm.Description;
                                        destinationCombinedSheet.Cell(combinedRowIndex + k, 74).Value = lm.Date.HasValue ? lm.Date.Value.ToString("MM/dd/yyyy"):"";
                                        destinationCombinedSheet.Cell(combinedRowIndex + k, 75).Value = lm.PlanOfRenewal;
                                        k++;
                                    }
                                    ////Capital Expense
                                    //CapitalExpenseController cec = new CapitalExpenseController();
                                    List<CapitalExpense> ceList = groupedItem.CapitalExpenses.ToList();
                                    //await cec.GetByBRId(groupedItem.BRID, year);
                                    int t = 1;
                                    foreach (var cm in ceList)
                                    {
                                        DestinationSheet.Cell(regionIndex + t, 76).Value = cm.Description;
                                        DestinationSheet.Cell(regionIndex + t, 77).Value = string.Format("{0:#,###0}", cm.Amount);
                                        DestinationSheet.Cell(regionIndex + t, 78).Style.NumberFormat.Format = "[$$-en-US] #,##0.00";
                                        DestinationSheet.Cell(regionIndex + t, 79).DataType = (XLDataType)CellValues.Number;

                                        destinationCombinedSheet.Cell(combinedRowIndex + t, 76).Value = cm.Description;
                                        destinationCombinedSheet.Cell(combinedRowIndex + t, 77).Value = string.Format("{0:#,###0}", cm.Amount);
                                        destinationCombinedSheet.Cell(combinedRowIndex + t, 78).Style.NumberFormat.Format = "[$$-en-US] #,##0.00";
                                        destinationCombinedSheet.Cell(combinedRowIndex + t, 79).DataType = (XLDataType)CellValues.Number;
                                        t++;
                                    }
                                }

                                regionIndex += j;
                                combinedRowIndex += j;
                            }
                        }
                    }
                }
                return DestinationBook;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        #endregion
        #endregion
    }
}
