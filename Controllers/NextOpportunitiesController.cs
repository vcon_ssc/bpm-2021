﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    //[Authorize]
    public class NextOpportunitiesController : ControllerBase
    {
        private IGenericRepository<NextOpportunity> _repository;
        public NextOpportunitiesController()
        {
            this._repository = new BPMRepository<NextOpportunity>(new bpmdbdevContext());
        }

        //public NextOpportunitiesController(IGenericRepository<NextOpportunity> repository)
        //{
        //    this._repository = repository;
        //}

        // GET api/<controller>
        public async Task<List<NextOpportunity>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextOpportunity Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public NextOpportunity GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextOpportunity GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/NextOpportunity/GetByYear/{year}")]
        public async Task<List<NextOpportunity>> GetByYear(string year)
        {
            try
            {
                List<NextOpportunity> yearList = new List<NextOpportunity>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextOpportunity GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/NextOpportunities/GetByBRId/{BRId}")]
        public async Task<List<NextOpportunity>> GetByBRId(int BRId)
        {
            try
            {
                List<NextOpportunity> brList = new List<NextOpportunity>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in NextOpportunity GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(NextOpportunity collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextOpportunity Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        //[HttpPost]
        public void put(int id, NextOpportunity collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextOpportunity put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in NextOpportunity Controller Delete method:", e);
            }
        }
    }
}