﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    //[Authorize]
    public class BusinessUnitController : ControllerBase
    {
        private IGenericRepository<BusinessUnit> _repository;

        public BusinessUnitController()
        {
            this._repository = new BPMRepository<BusinessUnit>(new bpmdbdevContext());
        }

        public BusinessUnitController(IGenericRepository<BusinessUnit> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<BusinessUnit>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BusinessUnit Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public BusinessUnit GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BusinessUnit GetByID method :", ex);
            }
        }

        //// GET api/<controller>
        [Route("api/BusinessUnit/GetBUByRegion/{Region_Id}")]
        public async Task<List<BusinessUnit>> GetBUByRegion(int Region_Id)
        {
            try
            {
                List<BusinessUnit> buList = new List<BusinessUnit>();
                buList = await _repository.GetModel();
                buList = buList.Where(X => X.RegionId == Region_Id).ToList();
                return buList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in BusinessUnit GetBUByRegion method :", ex);
            }

        }

        //// GET api/<controller>
        //[Route("api/BusinessUnit/GetByBRId/{BRId}")]
        //public async Task<List<BusinessUnit>> GetByBRId(int BRId)
        //{
        //    try
        //    {
        //        List<BusinessUnit> brList = new List<BusinessUnit>();
        //        brList = await _repository.GetModel();
        //        brList = brList.Where(X => X.BRID == BRId).ToList();
        //        return brList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Error in Business Unit GetByBRId method :", ex);
        //    }

        //}

        // POST api/<controller>
        public void Post(BusinessUnit collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in BusinessUnit Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, BusinessUnit collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in BusinessUnit put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in BusinessUnit Controller Delete method:", e);
            }
        }
    }
}