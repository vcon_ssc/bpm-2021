﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;
namespace BPM.Controllers.API
{
    public class FocusAreaSupportItfController : ControllerBase
    {
        private readonly IGenericRepository<FocusAreaSupportItf> _FocusAreaSupportItfRepository;
        private readonly bpmdbdevContext dbContext;
        public FocusAreaSupportItfController()
        {
            dbContext = new bpmdbdevContext();
            this._FocusAreaSupportItfRepository = new BPMRepository<FocusAreaSupportItf>(dbContext);
        }

        //public FocusAreaSupportItfController(IGenericRepository<FocusAreaSupportItf> repository)
        //{
        //    this._FocusAreaSupportItfRepository = repository;
        //}


        // POST api/<controller>
        public void Post(List<FocusAreaSupportItf> collection)
        {
            try
            {
                dbContext.FocusAreaSupportItfs.AddRange(collection);
                dbContext.SaveChanges();

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusAreaSupportItf Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public void Put(List<FocusAreaSupportItf> collection)
        {
            try
            {
                var firstRecord = collection[0];

                var Records = dbContext.FocusAreaSupportItfs.Where(x => x.Faid == firstRecord.Faid).ToList();
                dbContext.FocusAreaSupportItfs.RemoveRange(Records);
                dbContext.SaveChanges();
                dbContext.FocusAreaSupportItfs.AddRange(collection);
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusAreaSupportItf put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _FocusAreaSupportItfRepository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusAreaSupportItf Controller Delete method:", e);
            }
        }
    }
}