﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
   // [Authorize]
    public class RecessionPlanningController : ControllerBase
    {

        private IGenericRepository<RecessionPlanning> _planningrepository;
        public RecessionPlanningController()
        {
            this._planningrepository = new BPMRepository<RecessionPlanning>(new bpmdbdevContext());
        }

        //public RecessionPlanningController(IGenericRepository<RecessionPlanning> repository)
        //{
        //    this._planningrepository = repository;
        //}


        [Route("api/FocusAreas/GetByBRIdOnly/{BRId}")]
        public async Task<List<RecessionPlanning>> GetByBRIdOnly(int BRId)
        {
            try
            {
                List<RecessionPlanning> brList = new List<RecessionPlanning>();
                brList = await _planningrepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Recession Planning GetByBRIdOnly method :", ex);
            }

        }


        // GET api/<controller>
        public async Task<List<RecessionPlanning>> Get()
        {
            try
            {
                return await _planningrepository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in RecessionPlanning Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public RecessionPlanning GetById(int id)
        {
            try
            {
                return _planningrepository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in RecessionPlanning GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/RecessionPlanning/GetByYear/{year}")]
        public async Task<List<RecessionPlanning>> GetByYear(string year)
        {
            try
            {
                List<RecessionPlanning> yearList = new List<RecessionPlanning>();
                yearList = await _planningrepository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in RecessionPlanning GetByYear method :", ex);
            }

        }

        [Route("api/RecessionPlanning/GetByBRId/{BRId}/{year}")]
        public async Task<List<RecessionPlanning>> GetByBRId(int BRId, string year)
        {
            try
            {
                List<RecessionPlanning> brList = new List<RecessionPlanning>();
                brList = await _planningrepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId & X.Year == year).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in RecessionPlanning GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(RecessionPlanning collection)
        {
            try
            {
                // TODO: Add insert logic here
                _planningrepository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in RecessionPlanning Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public void put(int id, RecessionPlanning collection)
        {
            try
            {
                // TODO: Add update logic here
                _planningrepository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in RecessionPlanning put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _planningrepository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in RecessionPlanning Controller Delete method:", e);
            }
        }
    }
}