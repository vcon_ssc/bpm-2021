﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
   // [Authorize]

    public class LeaseModificationController : ControllerBase
    {
        private IGenericRepository<LeaseModification> _leaserepository;
        public LeaseModificationController()
        {
            this._leaserepository = new BPMRepository<LeaseModification>(new bpmdbdevContext());
        }

        //public LeaseModificationController(IGenericRepository<LeaseModification> repository)
        //{
        //    this._leaserepository = repository;
        //}

        [Route("api/FocusAreas/GetByBRIdOnly/{BRId}")]
        public async Task<List<LeaseModification>> GetByBRIdOnly(int BRId)
        {
            try
            {
                List<LeaseModification> brList = new List<LeaseModification>();
                brList = await _leaserepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in leaserepository GetByBRIdOnly method :", ex);
            }

        }

        // GET api/<controller>
        [Route("api/LeaseModification/")]
        public async Task<List<LeaseModification>> Get()
        {
            try
            {
                var a= await _leaserepository.GetModel();
                a.ForEach(c => c.Date = c.Date.Value.ToLocalTime());
                return a;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in LeaseModification Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public LeaseModification GetById(int id)
        {
            try
            {
               // return _leaserepository.GetModelById(id);
                var a =  _leaserepository.GetModelById(id);
                a.Date=a.Date.Value.ToLocalTime();
                return a;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in LeaseModification GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/LeaseModification/GetByYear/{year}")]
        public async Task<List<LeaseModification>> GetByYear(string year)
        {
            try
            {
                List<LeaseModification> yearList = new List<LeaseModification>();
                yearList = await _leaserepository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in LeaseModification GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/LeaseModification/GetByBRId/{BRId}/{year}")]
        public async Task<List<LeaseModification>> GetByBRId(int BRId, string year)
        {
            try
            {
                List<LeaseModification> brList = new List<LeaseModification>();
                //IGenericRepository<LeaseModification> _Arearepository = new BPMRepository<LeaseModification>(new bpmdbdevContext()); ;
               // var aList = await _Arearepository.GetModel();
                brList = await _leaserepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId & X.Year == year).ToList();
                brList.ForEach(c => c.Date =c.Date.HasValue? c.Date.Value.ToLocalTime():c.Date);
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in LeaseModification GetByBRId method :", ex);
            }

        }
        [Route("api/LeaseModification")]
        [HttpPost]
        // POST api/<controller>
        public void Post(LeaseModification collection)
        {
            try
            {
                // TODO: Add insert logic here
                //collection.Date=new DateTime(collection.Date).
                //collection.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.CreatedBy = "saurabhd@vconstruct.in";
                collection.ModifiedBy = collection.CreatedBy;
                collection.CreatedDate = DateTime.UtcNow;
                collection.ModifiedDate = collection.CreatedDate;


                _leaserepository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in LeaseModification Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPut]
        public void put(int id, LeaseModification collection)
        {
            try
            {
                // TODO: Add update logic here
                //collection.ModifiedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.ModifiedBy = "saurabhd@vconstruct.in";
                collection.ModifiedDate = DateTime.UtcNow;
                _leaserepository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in LeaseModification put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _leaserepository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in LeaseModification Controller Delete method:", e);
            }
        }
    }

}


