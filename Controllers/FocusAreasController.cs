﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
   // [Authorize]
    public class FocusAreasController : ControllerBase
    {
        private readonly IGenericRepository<FocusArea> _focusArearepository;
        private readonly bpmdbdevContext dbContext;
        public FocusAreasController()
        {
            this.dbContext = new bpmdbdevContext();
            this._focusArearepository = new BPMRepository<FocusArea>(dbContext);
        }

        //public FocusAreasController(IGenericRepository<FocusArea> repository)
        //{
        //    this._focusArearepository = repository;
        //}

        [Route("api/FocusAreas/GetByBRIdOnly/{BRId}")]
        public async Task<List<FocusArea>> GetByBRIdOnly(int BRId)
        {
            try
            {
                List<FocusArea> brList = new List<FocusArea>();
                brList = await _focusArearepository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in FocusArea GetByBRIdOnly method :", ex);
            }

        }

        // GET api/<controller>
        public async Task<List<FocusArea>> Get()
        {
            try
            {
                return await _focusArearepository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in FocusArea Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public FocusArea GetById(int id)
        {
            try
            {
                return _focusArearepository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in FocusArea GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/FocusArea/GetByYear/{year}")]
        public async Task<List<FocusArea>> GetByYear(string year)
        {
            try
            {
                List<FocusArea> yearList = new List<FocusArea>();
                yearList = await _focusArearepository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in FocusArea GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/FocusAreas/GetByBRId/{BRId}/{year}")]
        public async Task<List<FocusArea>> GetByBRId(int BRId, string year)
        {
            try
            {
                List<FocusArea> brList = new List<FocusArea>();
                string[] children = { "FocusAreaRequireGroups", "FocusAreaSupportItfs" };
                brList = await _focusArearepository.GetMultipleModelWithEagrLoading(X => X.Brid == BRId & X.Year == year,children);
                //brList = brList.Where(X => X.BRID == BRId & X.Year == year).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in FocusArea GetByBRId method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(FocusArea collection)
        {
            try
            {

                //collection.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.CreatedBy = "saurabhd@vconstruct.in";
                collection.ModifiedBy = collection.CreatedBy;
                collection.CreatedDate = DateTime.UtcNow;
                collection.ModifiedDate = collection.CreatedDate;
                foreach (var item in collection.FocusAreaRequireGroups)
                {
                   
                    item.Faid = collection.Faid;
                    //item.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                    collection.CreatedBy = "saurabhd@vconstruct.in";
                    item.ModifiedBy = collection.CreatedBy;
                    item.CreatedAt = DateTime.UtcNow;
                    item.ModifiedAt = item.CreatedAt;
                }

                foreach (var item in collection.FocusAreaSupportItfs)
                {

                    item.Faid = collection.Faid;
                    //item.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                    collection.CreatedBy = "saurabhd@vconstruct.in"; 
                    item.ModifiedBy = collection.CreatedBy;
                    item.CreatedAt = DateTime.UtcNow;
                    item.ModifiedAt = item.CreatedAt;
                }

               
                using (var ctx = new bpmdbdevContext())
                {

                    ctx.FocusAreas.Add(collection);
                
                    ctx.SaveChanges();
                }


            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusArea Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public async void Put(FocusArea collection)
        {
            try
            {
                // TODO: Add update logic here
               
                if (collection.FocusAreaRequireGroups.Count > 0)
                {
                    foreach (var item in collection.FocusAreaRequireGroups)
                    {
                        item.Faid = collection.Faid;
                        //item.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                        collection.CreatedBy = "saurabhd@vconstruct.in"; 
                        item.ModifiedBy = collection.CreatedBy;
                        item.CreatedAt = DateTime.UtcNow;
                        item.ModifiedAt = item.CreatedAt;
                    }
                    var Records = dbContext.FocusAreaRequireGroups.Where(x => x.Faid == collection.Faid).ToList();
                    dbContext.FocusAreaRequireGroups.RemoveRange(Records);
                }
                if (collection.FocusAreaSupportItfs.Count > 0)
                {
                    foreach (var item in collection.FocusAreaSupportItfs)
                    {
                        item.Faid = collection.Faid;
                        //item.CreatedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                        collection.CreatedBy = "saurabhd@vconstruct.in"; 
                        item.ModifiedBy = collection.CreatedBy;
                        item.CreatedAt = DateTime.UtcNow;
                        item.ModifiedAt = item.CreatedAt;
                    }
                    var Records = dbContext.FocusAreaSupportItfs.Where(x => x.Faid == collection.Faid).ToList();
                    dbContext.FocusAreaSupportItfs.RemoveRange(Records);
                }
                 //dbContext.SaveChanges();
                dbContext.FocusAreaRequireGroups.AddRange(collection.FocusAreaRequireGroups);
                dbContext.FocusAreaSupportItfs.AddRange(collection.FocusAreaSupportItfs);
                //dbContext.SaveChanges();
                //collection.ModifiedBy = (User.Identity as System.Security.Claims.ClaimsIdentity).FindFirst("name").Value;
                collection.ModifiedBy = "saurabhd@vconstruct.in";
                collection.ModifiedDate = DateTime.UtcNow;
                dbContext.FocusAreas.Attach(collection);
                dbContext.Entry(collection).Property(x => x.ModifiedBy).IsModified = true;
                dbContext.Entry(collection).Property(x => x.ModifiedDate).CurrentValue = DateTime.Now;
                dbContext.Entry(collection).Property(x => x.StrategicActions).IsModified = true;
                dbContext.Entry(collection).Property(x => x.IsImpact).IsModified = true;
                dbContext.Entry(collection).Property(x => x.OngoingEffort).IsModified = true;
                dbContext.Entry(collection).Property(x => x.IncrementalCosts).IsModified = true;
                dbContext.SaveChanges();
                //await _focusArearepository.UpdateModel(collection);






            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusArea put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public async void Delete(int id)
        {
            try
            {
                

                var relatedGroups = dbContext.FocusAreaRequireGroups.Where(x => x.Faid == id);

                var supportGroups = dbContext.FocusAreaSupportItfs.Where(x => x.Faid == id);
                    if (relatedGroups.Any())
                    {
                    dbContext.FocusAreaRequireGroups.RemoveRange(relatedGroups);
                        //await dbContext.SaveChangesAsync();
                    }
                    if (supportGroups.Any())
                    {
                    dbContext.FocusAreaSupportItfs.RemoveRange(supportGroups);
                        //await dbContext.SaveChangesAsync();
                    }
                var focusArea = dbContext.FocusAreas.Where(x => x.Faid == id).FirstOrDefault();
                dbContext.FocusAreas.Remove(focusArea);
                dbContext.SaveChanges();
                // TODO: Add delete logic here
                //await _focusArearepository.DeleteModel(id);

                // }



            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusArea Controller Delete method:", e);
            }
        }
    }
}
