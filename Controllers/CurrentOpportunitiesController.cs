﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
   // [Authorize]
    public class CurrentOpportunitiesController : ControllerBase
    {
        private IGenericRepository<CurrentOpportunity> _repository;
        public CurrentOpportunitiesController()
        {
            this._repository = new BPMRepository<CurrentOpportunity>(new bpmdbdevContext());
        }

        public CurrentOpportunitiesController(IGenericRepository<CurrentOpportunity> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<CurrentOpportunity>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentOpportunities Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public CurrentOpportunity GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentOpportunities GetByID method :", ex);
            }
        }

        // GET api/<controller>
        [Route("api/CurrentOpportunity/GetByYear/{year}")]
        public async Task<List<CurrentOpportunity>> GetByYear(string year)
        {
            try
            {
                List<CurrentOpportunity> yearList = new List<CurrentOpportunity>();
                yearList = await _repository.GetModel();
                yearList = yearList.Where(x => x.Year == year).ToList();
                return yearList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentOpportunities GetByYear method :", ex);
            }

        }

        //// GET api/<controller>
        [Route("api/CurrentOpportunity/GetByBRId/{BRId}")]
        public async Task<List<CurrentOpportunity>> GetByBRId(int BRId)
        {
            try
            {
                List<CurrentOpportunity> brList = new List<CurrentOpportunity>();
                brList = await _repository.GetModel();
                brList = brList.Where(X => X.Brid == BRId).ToList();
                return brList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CurrentOpportunity GetByBRId  method :", ex);
            }

        }

        // POST api/<controller>
        public void Post(CurrentOpportunity collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentOpportunities Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        //[HttpPost]
        public void put(int id, CurrentOpportunity collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentOpportunities put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CurrentOpportunities Controller Delete method:", e);
            }
        }
    }
}