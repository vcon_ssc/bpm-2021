﻿using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    //[Authorize]
    public class CoreMarketController : ControllerBase
    {
        private IGenericRepository<CoreMarket> _repository;

        public CoreMarketController()
        {
            this._repository = new BPMRepository<CoreMarket>(new bpmdbdevContext());
        }

        public CoreMarketController(IGenericRepository<CoreMarket> repository)
        {
            this._repository = repository;
        }

        // GET api/<controller>
        public async Task<List<CoreMarket>> Get()
        {
            try
            {
                return await _repository.GetModel();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CoreMarket Get method :", ex);
            }

        }

        // GET api/<controller>/5
        public CoreMarket GetById(int id)
        {
            try
            {
                return _repository.GetModelById(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in CoreMarket GetByID method :", ex);
            }
        }

        //// GET api/<controller>
        //[Route("api/CoreMarket/GetByBRId/{BRId}")]
        //public async Task<List<CoreMarket>> GetByBRId(int BRId)
        //{
        //    try
        //    {
        //        List<CoreMarket> brList = new List<CoreMarket>();
        //        brList = await _repository.GetModel();
        //        brList = brList.Where(X => X.BRID == BRId).ToList();
        //        return brList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Error in CoreMarket GetByBRId method :", ex);
        //    }

        //}   

        // POST api/<controller>
        public void Post(CoreMarket collection)
        {
            try
            {
                // TODO: Add insert logic here
                _repository.InsertModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CoreMarket Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        [HttpPost]
        public void put(int id, CoreMarket collection)
        {
            try
            {
                // TODO: Add update logic here
                _repository.UpdateModel(collection);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CoreMarket put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in CoreMarket Controller Delete method:", e);
            }
        }
    }
}