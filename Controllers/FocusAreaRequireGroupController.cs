﻿using BPM.Data;
using BPM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BPM.Data;

namespace BPM.Controllers.API
{
    public class FocusAreaRequireGroupController : ControllerBase
    {
        private readonly IGenericRepository<FocusAreaRequireGroup> _focusAreaRequireGroupRepository;
        private readonly bpmdbdevContext dbContext;
        public FocusAreaRequireGroupController()
        {
            dbContext = new bpmdbdevContext();
            this._focusAreaRequireGroupRepository = new BPMRepository<FocusAreaRequireGroup>(dbContext);
        }

        //public FocusAreaRequireGroupController(IGenericRepository<FocusAreaRequireGroup> repository)
        //{
        //    this._focusAreaRequireGroupRepository = repository;
        //}
        

        // POST api/<controller>
        public void Post(List<FocusAreaRequireGroup> collection)
        {
            try
            {
                dbContext.FocusAreaRequireGroups.AddRange(collection);
                dbContext.SaveChanges();

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusAreaRequireGroup Controller Post Method :", e);
                //return View();
            }
        }

        // PUT api/<controller>/5
        // [HttpPost]
        public async void Put(List<FocusAreaRequireGroup> collection)
        {
            try
            {
                var firstRecord = collection[0];

                var Records = dbContext.FocusAreaRequireGroups.Where(x => x.Faid == firstRecord.Faid).ToList();
                dbContext.FocusAreaRequireGroups.RemoveRange(Records);
                //dbContext.SaveChanges();
                dbContext.FocusAreaRequireGroups.AddRange(collection);
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusAreaRequireGroup put method :", e);
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _focusAreaRequireGroupRepository.DeleteModel(id);

            }
            catch (Exception e)
            {
                throw new ApplicationException("Error in FocusAreaRequireGroup Controller Delete method:", e);
            }
        }
    }
}