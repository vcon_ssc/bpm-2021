﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using BPM.Models;
using System.Threading.Tasks;
using System.Web;
using Font = iTextSharp.text.Font;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Net.Http.Headers;
using ClosedXML.Excel;
using ClosedXML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

namespace BPM.Controllers.API
{
    [Authorize]
    public class Export2020Controller : ControllerBase
    {
        #region Pdf For 2020
        private readonly float pageWidth=0F;
        private readonly IHostEnvironment _env;
        public Export2020Controller(IHostEnvironment env)
        {
            _env = env;
        }

        [HttpGet]
        [Route("api/Export2020/CreateAllYearPDFAsync/{year}")]
        public async Task CreateYearPDFAsync(string year)
        {
            try
            {

                HttpResponse Response = HttpContext.Response;
                Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
                MemoryStream ms = new MemoryStream();
                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, ms);

                //PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";
                pdfDoc.Open();

                BPMReferenceController bpm = new BPMReferenceController();
                List<Bpmreference> bpmRef = await bpm.GetByYear(year.ToString());
                int i = 0;
                foreach (var item in bpmRef)
                {
                    int yearBRID = item.Brid;
                    await createPDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
                    i++;
                    pdfDoc.NewPage();
                }

                //await 
                pdfWriter.CloseStream = false;
                pdfDoc.Close();

               // Response.Buffer = true;
                Response.ContentType = "application/pdf";
                byte[] pdfBytes = ms.ToArray();
                Response.Headers.Append("Content-Length", pdfBytes.Length.ToString());
                Response.Body.Write(pdfBytes, 0, (int)pdfBytes.Length);
                Response.StatusCode = StatusCodes.Status200OK;
                return;


            }
            catch (Exception e)
            {
                throw new ApplicationException("Err in All Pdf async " + e);
            }
        }

        [HttpGet]
        [AcceptVerbs("GET", "POST")]
        [Route("api/Export2020/CreatePDFAsync/{BRID}")]
        public async Task CreatePDFAsync(int BRID)
        {

            try
            {
                HttpResponse Response = HttpContext.Response;
                MemoryStream ms = new MemoryStream();
                Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, ms);

                // string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";

                int i = 0;
                pdfDoc.Open();
                //int yearBRID = 2020;
                await createPDFContentFromBRID(BRID, pdfDoc, pdfWriter);
                pdfWriter.CloseStream = false;
                pdfDoc.Close();



                //Response.Buffer = true;
                Response.ContentType = "application/pdf";
                byte[] pdfBytes = ms.ToArray();
                Response.Headers.Append("Content-Length", pdfBytes.Length.ToString());
                Response.Body.Write(pdfBytes, 0, (int)pdfBytes.Length);
                //Response.AddHeader("content-disposition", "attachment;filename=2020 Top 5 by xxx Group.pdf");
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Write(pdfDoc);
                Response.StatusCode = StatusCodes.Status200OK;
                return;
            }
            catch (Exception e)
            {
                throw e;

            }
        }


        public async Task createPDFContentFromBRID(int BRID, Document pdfDoc, PdfWriter pdfWriter)
        {
            try
            {
                Chunk chunk;
                Paragraph line;

                BPMReferenceController bpm = new BPMReferenceController();
                Bpmreference bpmRef = bpm.GetById(BRID);
                string nextYear = bpmRef.Year;

                //string nextYear = csc[0].Year;
                int currentYear = 2020;

                Int32.TryParse(nextYear, out currentYear);
                int CurrentYearMinusOne = currentYear - 1;

                drawHeader(pdfDoc, out chunk, out line, nextYear, BRID);

                PdfContentByte cb = pdfWriter.DirectContent;
                BaseFont bf1 = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1257, BaseFont.NOT_EMBEDDED);

                cb.SaveState();
                cb.BeginText();
                cb.MoveText(15, pdfDoc.Top - 5);
                cb.SetFontAndSize(bf1, 20);
                cb.ShowText(" Business Planning Meeting Report For " + nextYear);
                cb.EndText();
                cb.RestoreState();

                cb.SetLineWidth(1.0f);   // Make a bit thicker than 1.0 default
                cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
                cb.MoveTo(0, pdfDoc.Top - 10);
                cb.LineTo(650, pdfDoc.Top - 10);
                cb.Stroke();
                line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 0F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
                pdfDoc.Add(line);

                //pdfDoc.Add(pr);
                RegionController rc = new RegionController();
                List<Region> regionList = await rc.GetRegionNameByBRID(BRID);
                string dprGroupName = regionList[0].RegionName;
                string mainGroups = regionList[0].GroupName;
                //Paragraph pr = new Paragraph();
                chunk = new Chunk(" Group : ", FontFactory.GetFont("Arial", 16, Font.BOLD, BaseColor.BLACK));

                pdfDoc.Add(chunk);
                chunk = new Chunk(dprGroupName, FontFactory.GetFont("Arial", 16, Font.NORMAL, BaseColor.BLACK));
                //pr.Add(chunk);
                pdfDoc.Add(chunk);
                cb = pdfWriter.DirectContent;
                cb.SetLineWidth(1.0f);
                cb.SetGrayStroke(0.10f); // 1 = black, 0 = white
                cb.MoveTo(0, pdfDoc.Top - 42);
                cb.LineTo(650, pdfDoc.Top - 42);
                cb.Stroke();
                //line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100F, BaseColor.BLACK, Element.ALIGN_LEFT, 0.5f)));
                //pdfDoc.Add(line);

                //number of characters per line in pdf
                BaseFont bf = BaseFont.CreateFont(
                BaseFont.COURIER, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
                float charWidth = bf.GetWidth(" ");
                int charactersPerLine = 100;
                // float pageWidth = pdfDoc.Right() - pdfDoc.Left();
                float fontSize = (1000 * (pageWidth / (charWidth * charactersPerLine)));
                fontSize = ((int)(fontSize * 100)) / 100f;
                Font font = new Font(bf, fontSize);


                PdfPCell cell = new PdfPCell();
                cell.Border = 0;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                int noOfCells = 5;
                cell.Colspan = noOfCells;

                line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                if (mainGroups == "RBU")
                {

                    //recession planning

                    PdfPTable table = new PdfPTable(3);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;
                    float[] sglTblHdWidths;
                    sglTblHdWidths = new float[3];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 40f;
                    sglTblHdWidths[2] = 400f;
                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase("2020 Company Strategy and Recession Planning", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    cell.BackgroundColor = new BaseColor(0, 90, 155);
                    cell.Colspan = 6;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Category", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    RecessionPlanningController rp = new RecessionPlanningController();
                    List<RecessionPlanning> rpc = await rp.GetByBRIdOnly(BRID);
                    List<List<string>> gridData1 = new List<List<string>>();

                    foreach (var icItem in rpc)
                    {
                        string instanceDescription = icItem.Category;
                        string instanceDate = icItem.Description;
                        List<string> dataValue = new List<string>();
                        dataValue.Add(instanceDescription);
                        dataValue.Add(instanceDate);
                        gridData1.Add(dataValue);
                    }
                    Console.WriteLine();

                    //gridData.Add(dataValue);
                    int count2 = gridData1.Count;
                    if (count2 < 5)
                    {
                        int addEmptyRows = 5 - count2;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4);
                            //emptyList.Add(sno.ToString());
                            for (int z = 0; z < 2; z++)
                            {
                                emptyList.Add(" ");
                            }
                            gridData1.Add(emptyList);
                        }
                    }

                    int x2 = 1;
                    foreach (var gridItemList in gridData1)
                    {

                        table.AddCell(x2.ToString());
                        x2++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }

                    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);

                    table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;
                    sglTblHdWidths = new float[2];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 500f;

                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase("Strategic Action (Not Related To Company Strategy)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    cell.BackgroundColor = new BaseColor(0, 90, 155);
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);


                    //List<string> dataValue = new List<string>();
                    FocusAreasController fa = new FocusAreasController();
                    List<FocusArea> fac = await fa.GetByBRIdOnly(BRID);


                    List<List<string>> gridData4 = new List<List<string>>();

                    foreach (var icItem in fac)
                    {
                        string instanceDescription = icItem.StrategicActions;
                        List<string> dataValue01 = new List<string>();
                        dataValue01.Add(instanceDescription);

                        gridData4.Add(dataValue01);
                    }
                    // Console.WriteLine();

                    //gridData.Add(dataValue);
                    int count4 = gridData4.Count;
                    if (count4 < 5)
                    {
                        int addEmptyRows = 5 - count4;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4);
                            //emptyList.Add(sno.ToString());
                            for (int z = 0; z < 1; z++)
                            {
                                emptyList.Add(" ");
                            }
                            gridData4.Add(emptyList);
                        }
                    }

                    int x5 = 1;
                    foreach (var gridItemList in gridData4)
                    {

                        table.AddCell(x5.ToString());
                        x5++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }

                    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 320, pdfWriter.DirectContent);

                    //Lease Modifications

                    table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;

                    sglTblHdWidths = new float[4];
                    sglTblHdWidths[0] = 20f;
                    sglTblHdWidths[1] = 220f;
                    sglTblHdWidths[2] = 120f;
                    sglTblHdWidths[3] = 220f;
                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase("2020 Lease Modifications", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    cell.BackgroundColor = new BaseColor(0, 90, 155);
                    cell.Colspan = 8;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Lease Modification Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Current Date Of Expiration", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Plan Of Renewal", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    //List<string> dataValue = new List<string>();
                    LeaseModificationController lm = new LeaseModificationController();
                    List<LeaseModification> lmc = await lm.GetByBRIdOnly(BRID);
                    List<List<string>> gridData2 = new List<List<string>>();

                    foreach (var icItem in lmc)
                    {
                        string instanceDescription = icItem.Description;
                        string instanceDate = icItem.Date.Value.ToShortDateString();
                        string instancePlanOfRenewal = icItem.PlanOfRenewal;
                        List<string> dataValue = new List<string>();
                        dataValue.Add(instanceDescription);
                        dataValue.Add(instanceDate);
                        dataValue.Add(instancePlanOfRenewal);
                        gridData2.Add(dataValue);
                    }
                    Console.WriteLine();

                    //gridData.Add(dataValue);
                    int count6 = gridData2.Count;
                    if (count6 < 5)
                    {
                        int addEmptyRows = 5 - count6;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4);
                            //emptyList.Add(sno.ToString());
                            for (int z = 0; z < 3; z++)
                            {
                                emptyList.Add(" ");
                            }
                            gridData2.Add(emptyList);
                        }
                    }

                    int x3 = 1;
                    foreach (var gridItemList in gridData2)
                    {

                        table.AddCell(x3.ToString());
                        x3++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }

                    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 550, pdfWriter.DirectContent);

                    pdfDoc.NewPage();
                    table = new PdfPTable(3);
                    table.WidthPercentage = 100;
                    table.DefaultCell.FixedHeight = 35f;

                    sglTblHdWidths = new float[3];
                    sglTblHdWidths[0] = 50f;
                    sglTblHdWidths[1] = 320f;
                    sglTblHdWidths[2] = 80f;
                    table.SetWidths(sglTblHdWidths);
                    table.TotalWidth = 570f;
                    table.LockedWidth = true;
                    cell = new PdfPCell(new Phrase("2020 Capital Expenses", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                    cell.BackgroundColor = new BaseColor(0, 90, 155);
                    cell.Colspan = 6;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase("Capital Expense Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Amount(in $)", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                    cell.Rowspan = 1;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    CapitalExpenseController ce = new CapitalExpenseController();
                    List<CapitalExpense> cec = await ce.GetByBRIdOnly(BRID);
                    List<List<string>> gridData3 = new List<List<string>>();

                    foreach (var icItem in cec)
                    {
                        string instanceDescription = icItem.Description;
                        string instanceAmount = icItem.Amount;
                        List<string> dataValue5 = new List<string>();
                        dataValue5.Add(instanceDescription);
                        dataValue5.Add(instanceAmount);
                        gridData3.Add(dataValue5);
                    }
                    Console.WriteLine();

                    //gridData.Add(dataValue);
                    int count3 = gridData3.Count;
                    if (count3 < 5)
                    {
                        int addEmptyRows = 5 - count3;
                        for (int y = 0; y < addEmptyRows; y++)
                        {
                            //int sno = count + y + 1;
                            List<string> emptyList = new List<string>(4);
                            //emptyList.Add(sno.ToString());
                            for (int z = 0; z < 2; z++)
                            {
                                emptyList.Add(" ");
                            }
                            gridData3.Add(emptyList);
                        }
                    }

                    int x6 = 1;
                    foreach (var gridItemList in gridData3)
                    {

                        table.AddCell(x6.ToString());
                        x6++;

                        foreach (var item in gridItemList)
                        {

                            cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                            table.AddCell(cell);

                        }
                    }

                    table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 30, pdfWriter.DirectContent);


                }

                else if (mainGroups == "RBU" || mainGroups == "CM" || mainGroups == "CSG" || mainGroups == "SP" || mainGroups == "WVE")
                {
                    if (mainGroups == "CSG" && dprGroupName.Contains("Integrated") == true)
                    {
                        //2020 IntegratedTopFive

                        PdfPTable table = new PdfPTable(2);
                        table.WidthPercentage = 100;
                        table.DefaultCell.FixedHeight = 35f;
                        float[] sglTblHdWidths;
                        sglTblHdWidths = new float[2];
                        sglTblHdWidths[0] = 120f;
                        sglTblHdWidths[1] = 820f;

                        table.SetWidths(sglTblHdWidths);
                        table.TotalWidth = 570f;
                        table.LockedWidth = true;
                        cell = new PdfPCell(new Phrase("Integrated Top 5 (TCOP, Integrated Project Life Cycle, Get Work)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                        cell.BackgroundColor = new BaseColor(0, 90, 155);
                        cell.Colspan = 8;
                        cell.HorizontalAlignment = 1;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);


                        //List<string> dataValue = new List<string>();
                        IntegratedTopFiveController itf = new IntegratedTopFiveController();
                        List<IntegratedTopFive> itfc = await itf.GetByBRIdOnly(BRID);


                        List<List<string>> gridData1 = new List<List<string>>();

                        foreach (var icItem in itfc)
                        {
                            string instanceDescription = icItem.StrategicActions;
                            List<string> dataValue01 = new List<string>();
                            dataValue01.Add(instanceDescription);

                            gridData1.Add(dataValue01);
                        }
                        Console.WriteLine();

                        //gridData.Add(dataValue);
                        int count1 = gridData1.Count;
                        if (count1 < 5)
                        {
                            int addEmptyRows = 5 - count1;
                            for (int y = 0; y < addEmptyRows; y++)
                            {
                                //int sno = count + y + 1;
                                List<string> emptyList = new List<string>(4);
                                //emptyList.Add(sno.ToString());
                                //for (int z = 0; z < 3; z++)
                                //{
                                emptyList.Add(" ");
                                //}
                                gridData1.Add(emptyList);
                            }
                        }

                        int x1 = 1;
                        foreach (var gridItemList in gridData1)
                        {

                            table.AddCell(x1.ToString());
                            x1++;

                            foreach (var item in gridItemList)
                            {

                                cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                                table.AddCell(cell);

                            }
                        }

                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 100, pdfWriter.DirectContent);



                        //String tableHeaderValue = currentYear + " Integrated Top Five";
                        //int left = 180;
                        //int top = -60;
                        //int i;
                        //PdfPTable table = null;
                        //float[] sglTblHdWidths;
                        //drawTableList(pdfDoc, pdfWriter, out cell, dataValue01, tableHeaderValue, left, top, out table, out sglTblHdWidths, out i, 1);
                    }
                    else
                    {

                        PdfPTable table = new PdfPTable(2);
                        table.WidthPercentage = 100;
                        table.DefaultCell.FixedHeight = 35f;
                        float[] sglTblHdWidths;
                        sglTblHdWidths = new float[2];
                        sglTblHdWidths[0] = 20f;
                        sglTblHdWidths[1] = 500f;

                        table.SetWidths(sglTblHdWidths);
                        table.TotalWidth = 570f;
                        table.LockedWidth = true;
                        cell = new PdfPCell(new Phrase("Strategic Actions (Not Related to Company Strategy)", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                        cell.BackgroundColor = new BaseColor(0, 90, 155);
                        cell.Colspan = 2;
                        cell.HorizontalAlignment = 1;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Strategic Actions", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);


                        //List<string> dataValue = new List<string>();
                        FocusAreasController fa = new FocusAreasController();
                        List<FocusArea> fac = await fa.GetByBRIdOnly(BRID);
                        List<List<string>> gridData1 = new List<List<string>>();

                        foreach (var icItem in fac)
                        {
                            string instanceDescription = icItem.StrategicActions;
                            List<string> dataValue01 = new List<string>();
                            dataValue01.Add(instanceDescription);

                            gridData1.Add(dataValue01);
                        }
                        //gridData.Add(dataValue);
                        int count1 = gridData1.Count;
                        if (count1 < 5)
                        {
                            int addEmptyRows = 5 - count1;
                            for (int y = 0; y < addEmptyRows; y++)
                            {
                                //int sno = count + y + 1;
                                List<string> emptyList = new List<string>(3);
                                emptyList.Add(" ");
                                // emptyList.Add(sno.ToString());
                                //for (int z = 0; z < 2; z++)
                                //{
                                //    
                                //}
                                gridData1.Add(emptyList);
                            }
                        }

                        int x1 = 1;
                        foreach (var gridItemList in gridData1)
                        {

                            table.AddCell(x1.ToString());
                            x1++;

                            foreach (var item in gridItemList)
                            {

                                cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                                table.AddCell(cell);

                            }
                        }

                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 60, pdfWriter.DirectContent);


                        table = new PdfPTable(4);
                        table.WidthPercentage = 100;
                        table.DefaultCell.FixedHeight = 35f;

                        sglTblHdWidths = new float[4];
                        sglTblHdWidths[0] = 120f;
                        sglTblHdWidths[1] = 820f;
                        sglTblHdWidths[2] = 520f;
                        sglTblHdWidths[3] = 820f;
                        table.SetWidths(sglTblHdWidths);
                        table.TotalWidth = 570f;
                        table.LockedWidth = true;
                        cell = new PdfPCell(new Phrase("2020 Lease Modifications", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                        cell.BackgroundColor = new BaseColor(0, 90, 155);
                        cell.Colspan = 8;
                        cell.HorizontalAlignment = 1;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Lease Modification Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = new PdfPCell(new Phrase("Current Date Of Expiration", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Plan Of Renewal", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);
                        LeaseModificationController lm = new LeaseModificationController();
                        List<LeaseModification> lmc = await lm.GetByBRIdOnly(BRID);
                        List<List<string>> gridData = new List<List<string>>();

                        foreach (var icItem in lmc)
                        {
                            string instanceDescription = icItem.Description;
                            string instanceDate = icItem.Date.Value.ToShortDateString();
                            string instancePlanOfRenewal = icItem.PlanOfRenewal;
                            List<string> dataValue = new List<string>();
                            dataValue.Add(instanceDescription);
                            dataValue.Add(instanceDate);
                            dataValue.Add(instancePlanOfRenewal);
                            gridData.Add(dataValue);
                        }
                        Console.WriteLine();

                        //gridData.Add(dataValue);
                        int count = gridData.Count;
                        if (count < 5)
                        {
                            int addEmptyRows = 5 - count;
                            for (int y = 0; y < addEmptyRows; y++)
                            {
                                //int sno = count + y + 1;
                                List<string> emptyList = new List<string>(4);
                                //emptyList.Add(sno.ToString());
                                for (int z = 0; z < 3; z++)
                                {
                                    emptyList.Add(" ");
                                }
                                gridData.Add(emptyList);
                            }
                        }

                        int x = 1;
                        foreach (var gridItemList in gridData)
                        {

                            table.AddCell(x.ToString());
                            x++;

                            foreach (var item in gridItemList)
                            {

                                cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));

                                table.AddCell(cell);

                            }
                        }

                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 300, pdfWriter.DirectContent);

                        //capital expenses

                        table = new PdfPTable(3);
                        table.WidthPercentage = 100;
                        table.DefaultCell.FixedHeight = 35f;

                        sglTblHdWidths = new float[3];
                        sglTblHdWidths[0] = 50f;
                        sglTblHdWidths[1] = 420f;
                        sglTblHdWidths[2] = 80f;
                        table.SetWidths(sglTblHdWidths);
                        table.TotalWidth = 570f;
                        table.LockedWidth = true;
                        cell = new PdfPCell(new Phrase("2020 Capital Expenses", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.WHITE)));
                        cell.BackgroundColor = new BaseColor(0, 90, 155);
                        cell.Colspan = 6;
                        cell.HorizontalAlignment = 1;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("#", FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = new PdfPCell(new Phrase("Capital Expense Description", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = new PdfPCell(new Phrase("Amount(in $)", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                        cell.Rowspan = 1;
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);

                        CapitalExpenseController ce = new CapitalExpenseController();
                        List<CapitalExpense> cec = await ce.GetByBRIdOnly(BRID);
                        List<List<string>> gridData3 = new List<List<string>>();

                        foreach (var icItem in cec)
                        {
                            string instanceDescription = icItem.Description;
                            string instanceAmount = icItem.Amount;
                            List<string> dataValue5 = new List<string>();
                            dataValue5.Add(instanceDescription);
                            dataValue5.Add(instanceAmount);
                            gridData3.Add(dataValue5);
                        }
                        Console.WriteLine();

                        //gridData.Add(dataValue);
                        int count3 = gridData3.Count;
                        if (count3 < 5)
                        {
                            int addEmptyRows = 5 - count3;
                            for (int y = 0; y < addEmptyRows; y++)
                            {
                                //int sno = count + y + 1;
                                List<string> emptyList = new List<string>(4);


                                //emptyList.Add(sno.ToString());
                                for (int z = 0; z < 2; z++)
                                {
                                    emptyList.Add(" ");
                                }
                                gridData3.Add(emptyList);
                            }
                        }

                        int x6 = 1;
                        foreach (var gridItemList in gridData3)
                        {

                            table.AddCell(x6.ToString());
                            x6++;

                            foreach (var item in gridItemList)
                            {

                                cell = new PdfPCell(new Phrase(item, FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.BLACK)));

                                table.AddCell(cell);

                            }
                        }
                        table.WriteSelectedRows(0, -1, pdfDoc.Left - 10, pdfDoc.Top - 550, pdfWriter.DirectContent);
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("If you're seeing this, something bad happened sdfghfvjnv" + e);
                throw new ApplicationException(e.ToString());
                //ManageLogs("Error in PDFController converting all to pdf " + e);
            }
        }


        private void drawHeader(Document pdfDoc, out Chunk chunk, out Paragraph line, string year, int BRID)
        {

            Paragraph pr = new Paragraph();
            chunk = new Chunk(" Business Planning Meeting Report For " + year, FontFactory.GetFont("Arial", 21, Font.BOLD, BaseColor.BLACK));

            //Horizontal Line
            line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            Image ok = Image.GetInstance(Path.Combine(_env.ContentRootPath, "~/Content/Images/dpr-logo.jpg"));
            ok.ScaleAbsoluteWidth(40);
            ok.ScaleAbsoluteHeight(20);
            ok.SetAbsolutePosition(pdfDoc.Right - 35, pdfDoc.Top - 5);
            ok.SetDpi(300, 300);
            pdfDoc.Add(ok);

        }



        //[ HttpGet]
        //[ AcceptVerbs("GET", "POST")]
        //[ Route("api/Export2020/CreateMyPDFAsync/{year}")]

        //public async Task CreateMyPDFAsync(string year)
        //{
        //    HttpResponse Response = HttpContext.Response;
        //    Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
        //    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.Body);

        //    //  string pdfFilePath = System.Web.HttpContext.Current.Server.MapPath(".") + "/PDFFiles";


        //    pdfDoc.Open();

        //    BPMReferenceController bpm = new BPMReferenceController();
        //    List<ViewRegion> bpmRef = await bpm.GetByResPerYear(year.ToString());
        //    int i = 0;
        //    foreach (var item in bpmRef)
        //    {
        //        int yearBRID = item.BRID;
        //        await createPDFContentFromBRID(yearBRID, pdfDoc, pdfWriter);
        //        i++;
        //        pdfDoc.NewPage();
        //    }

        //    //await 
        //    pdfWriter.CloseStream = false;
        //    pdfDoc.Close();



        //    //Response.Buffer = true;
        //    //Response.Headers.Add("content-disposition", "attachment;filename=2020 Top 5 by xxx Group.pdf");
        //    // Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.ContentType = "application/pdf";
        //    byte[] pdfBytes = Response.Body.ToArray();
        //    Response.Headers.Append("Content-Length", pdfBytes.Length.ToString());
        //    Response.Body.Write(pdfBytes, 0, (int)pdfBytes.Length);
        //    Response.StatusCode = StatusCodes.Status200OK;
        //}

        #endregion

        #region Excel For 2020
        [HttpGet]
        //[AcceptVerbs("GET", "POST")]
        [Route("api/Export2020/CreateExcelAsyncForYear/{year}")]
        public async Task<HttpResponseMessage> CreateExcelAsyncForYear(string year)
        {

            try
            {

                //string year = "2020";

                HttpResponse Response = HttpContext.Response;


                //var wb = new XLWorkbook();
                //var ws = wb.AddWorksheet("Sheet1");
                //ws.FirstCell().SetValue(year);
                var wob = await CreateWorkbook(year);
                //await CreateNewWorkbook(year);

                //Response.Buffer = true;
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.AddHeader("content-disposition", "attachment;filename=2020Top5.xlsx");
                ////ms.WriteTo(Response.OutputStream);
                //// Response.AppendHeader("Content-Length", excelBytes.Length.ToString());
                //// Response.OutputStream.Write(excelBytes, 0, (int)excelBytes.Length);

                //// ms.WriteTo(Response.OutputStream);

                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Write(HttpContext.Current.Server.MapPath("~/Content/Group1Template.xlsx"));
                //Response.StatusCode = StatusCodes.Status200OK;
                //return ;
                return wob.Deliver("excelfile.xlsx");
            }
            catch (Exception e)
            {
                throw e; ;

            }

        }
        public async Task<XLWorkbook> CreateWorkbook(string year)
        {
            try
            {
                XLWorkbook SourceBook = null;
                var DestinationBook = new XLWorkbook();
                RegionController region = new RegionController();
                List<ViewRegion> RegionList = await region.GetBpmWithRegionByYear(year);
                string startPath, filePath = "";
                int i = 0;
                foreach (var item in RegionList)
                {
                    i++;
                    // int yearBRID = item.BRID;

                    string RegionName = item.RegionName;
                    string GroupName = item.GroupName;
                    //if (RegionName.Contains("\n"))
                    //{
                    //    RegionName = "Integrated" + i;
                    //}
                    if (GroupName == "RBU")
                    {
                        //startPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                        // filePath = System.IO.Path.Combine(startPath, "Group1Template.xlsx");
                        filePath = Path.Combine(_env.ContentRootPath, "~/Content/Group12020.xlsx");
                    }
                    else if (GroupName == "CM" || GroupName == "CSG" || GroupName == "SP" || GroupName == "WVE")
                    {
                        if (GroupName == "CSG" && RegionName.Contains("Integrated"))
                        {
                            //startPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                            //filePath = System.IO.Path.Combine(startPath, "Group3Template.xlsx");
                            filePath = Path.Combine(_env.ContentRootPath, "~/Content/Group32020.xlsx");

                        }
                        else
                        {
                            //startPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                            //filePath = System.IO.Path.Combine(startPath, "Group2Template.xlsx");
                            filePath = Path.Combine(_env.ContentRootPath,"~/Content/Group22020.xlsx");
                        }
                    }
                    SourceBook = new XLWorkbook(filePath);
                    var a = SourceBook.Worksheets.Count;
                    var SourceSheet = SourceBook.Worksheet(3);
                    //DestinationBook.AddWorksheet(RegionName);
                    //var b=DestinationBook.Worksheets.Count;
                    //var DestinationSheet = DestinationBook.Worksheet(i);
                    //DestinationSheet.Name = RegionName;
                    if (GroupName == "CSG" && RegionName.Contains("Integrated"))
                    {
                        RegionName = RegionName.Replace(" ", "");
                    }
                    SourceSheet.CopyTo(DestinationBook, RegionName);
                    var DestinationSheet = DestinationBook.Worksheet(i);
                    DestinationSheet.Cell(5, 3).Value = RegionName;
                    if (item.BRID != 0)
                    {
                        if (GroupName == "RBU")
                        {
                            RecessionPlanningController rpc = new RecessionPlanningController();
                            List<RecessionPlanning> rpList = await rpc.GetByBRId(item.BRID, year);
                            int h = 0;
                            //Recession Planning
                            foreach (var rp in rpList)
                            {
                                DestinationSheet.Cell(11 + h, 2).Value = h + 1;
                                DestinationSheet.Cell(11 + h, 3).Value = rp.Description;
                                DestinationSheet.Cell(11 + h, 4).Value = rp.Category;
                                h++;
                            }

                            //Focus Area
                            FocusAreasController fac = new FocusAreasController();
                            List<FocusArea> faList = await fac.GetByBRId(item.BRID, year);
                            int j = 0;
                            foreach (var fa in faList)
                            {
                                DestinationSheet.Cell(20 + j, 2).Value = j + 1;
                                DestinationSheet.Cell(20 + j, 3).Value = fa.StrategicActions;
                                j++;
                            }

                            //Lease Modification
                            LeaseModificationController lmc = new LeaseModificationController();
                            List<LeaseModification> lmList = await lmc.GetByBRId(item.BRID, year);
                            int k = 0;
                            foreach (var lm in lmList)
                            {
                                DestinationSheet.Cell(29 + k, 2).Value = k + 1;
                                DestinationSheet.Cell(29 + k, 3).Value = lm.Description;
                                DestinationSheet.Cell(29 + k, 4).Value = lm.Date.Value.ToString("MM/dd/yyyy");
                                DestinationSheet.Cell(29 + k, 5).Value = lm.PlanOfRenewal;
                                k++;
                            }
                            //Capital Expense
                            CapitalExpenseController cec = new CapitalExpenseController();
                            List<CapitalExpense> ceList = await cec.GetByBRId(item.BRID, year);
                            k = 0;
                            foreach (var cm in ceList)
                            {
                                DestinationSheet.Cell(38 + k, 2).Value = k + 1;
                                DestinationSheet.Cell(38 + k, 3).Value = cm.Description;
                                DestinationSheet.Cell(38 + k, 4).Value = string.Format("{0:#,###0}", cm.Amount);
                                k++;
                            }
                        }

                        else if (GroupName == "CSG" && RegionName.Contains("Integrated"))
                        {
                            IntegratedTopFiveController itc = new IntegratedTopFiveController();
                            List<IntegratedTopFive> itList = await itc.GetByBRId(item.BRID, year);
                            int h = 0;
                            foreach (var fa in itList)
                            {
                                DestinationSheet.Cell(11 + h, 2).Value = h + 1;
                                DestinationSheet.Cell(11 + h, 3).Value = fa.StrategicActions;
                                h++;
                            }

                        }

                        else
                        {
                            //Focus Area
                            FocusAreasController fac = new FocusAreasController();
                            List<FocusArea> faList = await fac.GetByBRId(item.BRID, year);
                            int j = 0;
                            foreach (var fa in faList)
                            {
                                DestinationSheet.Cell(11 + j, 2).Value = j + 1;
                                DestinationSheet.Cell(11 + j, 3).Value = fa.StrategicActions;
                                j++;
                            }

                            //Lease Modification
                            LeaseModificationController lmc = new LeaseModificationController();
                            List<LeaseModification> lmList = await lmc.GetByBRId(item.BRID, year);
                            int k = 0;
                            foreach (var lm in lmList)
                            {
                                DestinationSheet.Cell(21 + k, 2).Value = k + 1;
                                DestinationSheet.Cell(21 + k, 3).Value = lm.Description;
                                DestinationSheet.Cell(21 + k, 4).Value = lm.Date.Value.ToString("MM/dd/yyyy");
                                DestinationSheet.Cell(21 + k, 5).Value = lm.PlanOfRenewal;
                                k++;
                            }
                            //Capital Expense
                            CapitalExpenseController cec = new CapitalExpenseController();
                            List<CapitalExpense> ceList = await cec.GetByBRId(item.BRID, year);
                            k = 0;
                            foreach (var cm in ceList)
                            {
                                DestinationSheet.Cell(30 + k, 2).Value = k + 1;
                                DestinationSheet.Cell(30 + k, 3).Value = cm.Description;
                                DestinationSheet.Cell(30 + k, 4).Value = string.Format("{0:#,###0}", cm.Amount);
                                k++;
                            }
                        }
                    }





                }
                //DestinationBook.Worksheet().SetTabActive();
                return DestinationBook;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
#endregion
    }
}
